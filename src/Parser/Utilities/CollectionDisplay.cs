using System;
using System.Collections;
using System.Diagnostics;
using System.Linq;

namespace Parser.Utilities
{
    public class CollectionDisplay
    {
        public interface ITarget
        {
            IEnumerable Items { get; }
        }

        private readonly ITarget Target;

        public CollectionDisplay(ITarget target)
        {
            Target = target;
        }

        [DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
        public Object[] Items
            => Target.Items.Cast<Object>().ToArray();
    }
}
