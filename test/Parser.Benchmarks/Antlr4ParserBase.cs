using System;
using System.Collections.Generic;
using Antlr4.Runtime;
using Antlr4.Runtime.Tree;

namespace Parser.Benchmarks
{
    using Valuations;

    public abstract class Antlr4ParserBase<TInner> : IParser<Boolean>
    {
        private readonly TInner Inner;
        private readonly Func<TInner, ITokenStream, ITree> InvokeParse;
        public IValuationSemiring<Boolean> Semiring { get; }
            = new BooleanSemiring();

        protected Antlr4ParserBase(TInner inner, Func<TInner, ITokenStream, ITree> invokeParse)
        {
            Inner = inner;
            InvokeParse = invokeParse;
        }

        public IEnumerable<Statistics> Run(IEnumerable<Char> input, Action<Boolean> withResult)
        {
            var tokens = new List<IToken>();
            var factory = CommonTokenFactory.Default;
            foreach (var terminal in input)
            {
                tokens.Add(factory.Create((Int32)terminal, String.Empty));
                yield return new Statistics();
            }
            var stream = new BufferedTokenStream(new ListTokenSource(tokens));
            try
            {
                InvokeParse(Inner, stream);
                withResult(true);
            }
            catch (Exception)
            {
                withResult(false);
            }
            yield return new Statistics();
        }
    }
}
