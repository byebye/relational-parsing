using System;
using System.Collections.Generic;
using System.Linq;
using No.Comparers;

namespace Parser.Relational
{
    using Algebra;
    using Utilities;
    using static EqualityComparerFactories;

    public interface IRelationStack<TWrap, TStack>
        where TStack : IRelationStack<TWrap, TStack>
    {
        TStack Parent { get; }
        TStack Origin { get; }
        IReadOnlyList<TWrap> Content { get; }
    }

    public abstract class StackedRelationEngine<TValue, TAtom, TWrap, TShape, TInner> : RelationEngine<TValue, TAtom, TWrap, StackedRelationEngine<TValue, TAtom, TWrap, TShape, TInner>.Tip>, IRelationOperant<TValue, TAtom, StackedRelationEngine<TValue, TAtom, TWrap, TShape, TInner>.Stack>
    {
        public abstract IEqualityComparer<TShape> ShapeComparer { get; }
        public abstract (TShape, IReadOnlyList<TWrap>) Decompose(TInner inner);
        public abstract TInner Substitute(TShape shape, IReadOnlyList<TWrap> content, Int32 dependencyDepth);
        public abstract TWrap Epsilon(Stack relation);

        public struct StackData
        {
            public TInner Inner;
            public TShape Shape;
            public IReadOnlyList<TWrap> Content;
        }

        [System.Diagnostics.DebuggerDisplay("[{Id}]")]
        public class Stack : IRelationStack<TWrap, Stack>
        {
            private static Int32 LastId = 0;
            public readonly Int32 Id = ++LastId;

            public Stack Parent { get; }
            public Int32 Depth { get; }
            public StackData Data { get; }
            public TInner Inner
                => Data.Inner;
            public TShape Shape
                => Data.Shape;
            public IReadOnlyList<TWrap> Content
                => Data.Content;
            public Stack Origin { get; }

            public Stack(Stack parent, StackData data, Stack origin)
            {
                Parent = parent;
                Depth = (parent?.Depth ?? 0) + 1;
                Data = data;
                Origin = origin;
            }
        }

        public struct Tip
        {
            public Stack Stack;
            public TInner Inner;
            public Stack Origin;
        }

        protected readonly IRelationEngine<TValue, TAtom, TWrap, TInner> InnerEngine;
        protected readonly IEqualityComparer<Stack> StackComparer;
        public override IEqualityComparer<Tip> Comparer { get; }

        public StackedRelationEngine(IRelationEngine<TValue, TAtom, TWrap, TInner> innerEngine)
        {
            InnerEngine = innerEngine;
            StackComparer = IdentityEC<Stack>();
            Comparer = new ComponentsEqualityComparer<Tip>
            {
                { _ => _.Inner, InnerEngine.Comparer },
                { _ => _.Stack, StackComparer },
            };
        }

        private Stack MakeStack(Stack parent, TInner inner, Stack origin)
        {
            var data = new StackData { Inner = inner };
            (data.Shape, data.Content) = Decompose(data.Inner);
            return new Stack(parent, data, origin);
        }

        protected Stack StackOne
            => MakeStack(null, InnerEngine.One, null);

        protected Stack StackZero
            => MakeStack(null, InnerEngine.Zero, null);

        public override Tip One
            => new Tip { Inner = InnerEngine.One };

        public Boolean IsOne(Stack relation)
            => InnerEngine.IsOne(relation.Inner);

        public override Tip Zero
            => new Tip { Inner = InnerEngine.Zero };

        public override Boolean IsZero(Tip value)
            => InnerEngine.IsZero(value.Inner);

        public override IWrapEngine<TValue, TAtom, TWrap> WrapEngine
            => InnerEngine.WrapEngine;

        Stack IOne<Stack>.One
            => StackOne;

        IEqualityComparer<Stack> ISet<Stack>.Comparer
            => StackComparer;

        public TValue FlatEpsilon(Stack stack)
            => WrapEngine.Flatten(Epsilon(stack));

        protected Tip ToTip(Stack stack, Int32 dependencyDepth = 0)
            => new Tip
            {
                Stack = stack.Parent,
                Inner = Substitute(stack.Shape, stack.Content, dependencyDepth),
                Origin = stack,
            };

        protected Stack FromTip(Tip node)
            => MakeStack(node.Stack, node.Inner, node.Origin);

        private void MergeUp(ref Tip node)
        {
            var parent = ToTip(node.Stack, node.Origin.Depth - (node.Stack?.Depth ?? 0));
            node.Inner = InnerEngine.Concat(node.Inner, parent.Inner);
            node.Stack = parent.Stack;
        }

        private void Fix(ref Tip node)
        {
            if (!WrapEngine.IsZero(InnerEngine.Epsilon(node.Inner)) && node.Stack != null)
                MergeUp(ref node);
        }

        public override IEnumerable<(IState, IReadOnlyCollection<Tip>)> Derivatives(Tip relation, Func<IState, Boolean> filter)
        {
            var results = new List<(IState, IReadOnlyCollection<Tip>)>();
            Fix(ref relation);
            foreach (var (state, inners) in InnerEngine.Derivatives(relation.Inner, filter))
            {
                var nodes = new List<Tip>();
                foreach (var inner in inners)
                {
                    relation.Inner = inner;
                    nodes.Add(relation);
                }
                yield return (state, nodes);
            }
        }

        public override TWrap Epsilon(Tip relation)
            => InnerEngine.Epsilon(relation.Inner);

        public override Tip Multiply(IAtomicRelation<TAtom> left, Tip right)
        {
            right.Inner = InnerEngine.Multiply(left, right.Inner);
            return right;
        }

        public override Tip Multiply(TValue left, Tip right)
        {
            right.Inner = InnerEngine.Multiply(left, right.Inner);
            return right;
        }

        private Stack Lca(Stack one, Stack two)
        {
            var seen = new List<Stack>();
            while (one != null || two != null)
            {
                if (one != null)
                {
                    if (seen.Contains(one))
                        return one;
                    seen.Add(one);
                    one = one.Parent;
                }
                if (two != null)
                {
                    if (seen.Contains(two))
                        return two;
                    seen.Add(two);
                    two = two.Parent;
                }
            }
            return null;
        }

        public override Tip Add(Tip left, Tip right)
        {
            if (InnerEngine.IsZero(left.Inner))
                return right;
            if (InnerEngine.IsZero(right.Inner))
                return left;
            Tip result = new Tip { Stack = Lca(left.Stack, right.Stack) };
            while (left.Stack != result.Stack)
                MergeUp(ref left);
            while (right.Stack != result.Stack)
                MergeUp(ref right);
            result.Inner = InnerEngine.Add(left.Inner, right.Inner);
            System.Diagnostics.Debug.Assert(left.Origin == right.Origin);
            result.Origin = left.Origin;
            return result;
        }

        private sealed class PCache<TKey> : Dictionary<TKey, StackData[]>
        {
            public PCache<(TKey, TShape)> Next;

            public PCache(IEqualityComparer<TKey> keyEC)
                : base(keyEC)
            {
            }
        }

        private readonly PCache<IRelationalOperation<TValue, TAtom>> PerformCache
            = new PCache<IRelationalOperation<TValue, TAtom>>(EquatableEC<IRelationalOperation<TValue, TAtom>>());

        private readonly Counter PerformCalls = new Counter();
        private readonly Counter PerformTotalDependencies = new Counter();
        private readonly Counter PerformLeavesCreated = new Counter();
        private readonly Counter PerformLeavesTotalDepth = new Counter();
        private readonly Counter PerformValuesTotalDepth = new Counter();

        private Boolean Lookup<TKey>(PCache<TKey> cache, TKey key, Stack origin, ref Stack stack)
        {
            StackData[] entry;
            if (!cache.TryGetValue(key, out entry))
                return false;
            if (entry == null)
            {
                if (cache.Next == null)
                    cache.Next = new PCache<(TKey, TShape)>(ValueTupleEC(cache.Comparer, ShapeComparer));
                var shape = stack.Shape;
                stack = stack.Parent;
                return Lookup(cache.Next, (key, shape), origin, ref stack);
            }
            PerformTotalDependencies.Increment((origin?.Depth ?? 0) - (stack?.Depth ?? 0));
            foreach (var data in entry)
                stack = new Stack(stack, data, origin);
            return true;
        }

        private void Insert<TKey>(PCache<TKey> cache, TKey key, Stack stack, ref Stack result)
        {
            if (stack == result.Parent)
            {
                if (result.Origin != null)
                    PerformTotalDependencies.Increment(result.Origin.Depth - result.Depth + 1);
                PerformLeavesCreated.Increment();
                var entry = new List<StackData>();
                foreach (var factor in InnerEngine.Factorize(result.Inner))
                {
                    stack = MakeStack(stack, factor, result.Origin);
                    entry.Add(stack.Data);
                }
                result = stack;
                cache.Add(key, entry.ToArray());
                PerformValuesTotalDepth.Increment(entry.Count);
            }
            else
            {
                PerformLeavesTotalDepth.Increment();
                cache[key] = null;
                if (cache.Next == null)
                    cache.Next = new PCache<(TKey, TShape)>(ValueTupleEC(cache.Comparer, ShapeComparer));
                Insert(cache.Next, (key, stack.Shape), stack.Parent, ref result);
            }
        }

        public void Perform(IRelationalOperation<TValue, TAtom> operation, ref Stack relation)
        {
            PerformCalls.Increment();
            Stack result = relation;
            if (!Lookup(PerformCache, operation, relation, ref result))
            {
                var tip = operation.Perform(this, ToTip(relation));
                Fix(ref tip);
                result = FromTip(tip);
                Insert(PerformCache, operation, relation, ref result);
            }
            relation = result;
        }

        TResult IRelationOperant<TValue>.Accept<TResult>(IRelationOperantVisitor<TValue, TResult> visitor)
            => visitor.Visit<TAtom, Stack>(this);
    }

    public class StackedRelationEngine<TValue, TAtom, TWrap, TInner> : StackedRelationEngine<TValue, TAtom, TWrap, TInner, TInner>
    {
        public StackedRelationEngine(IRelationEngine<TValue, TAtom, TWrap, TInner> innerEngine)
            : base(innerEngine)
        {
        }

        public override IEqualityComparer<TInner> ShapeComparer
            => InnerEngine.Comparer;

        public override (TInner, IReadOnlyList<TWrap>) Decompose(TInner inner)
            => (inner, Array.Empty<TWrap>());

        public override TWrap Epsilon(Stack relation)
            => Epsilon(ToTip(relation));

        public override TInner Substitute(TInner shape, IReadOnlyList<TWrap> content, Int32 dependencyDepth)
            => shape;
    }

    public class StackedRelationEngineFactory : RelationEngineFactory
    {
        private readonly IRelationEngineFactory InnerFactory;

        public StackedRelationEngineFactory(IRelationEngineFactory innerFactory)
            : base(innerFactory.WrapEngineFactory)
        {
            InnerFactory = innerFactory;
        }

        public override IRelationEngine<TValue> Create<TValue>(IValuationSemiring<TValue> semiring, IWrapEngine<TValue> wrapEngine)
            => InnerFactory
                .Create(semiring, wrapEngine)
                .Accept(new CreateRequest<TValue>());

        private class CreateRequest<TValue> : IRelationEngineVisitor<TValue, IRelationEngine<TValue>>
        {
            public IRelationEngine<TValue> Visit<TAtom, TWrap, TShape, TRelation>(IRelationEngine<TValue, TAtom, TWrap, TShape, TRelation> engine) =>
                new StackedRelationEngine<TValue, TAtom, TWrap, TRelation>(engine);
        }

        public override String ToString() =>
            $"{nameof(StackedRelationEngineFactory)}({InnerFactory})";
    }
}
