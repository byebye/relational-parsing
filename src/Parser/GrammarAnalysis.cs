using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Parser
{
    using Utilities;

    public class GrammarAnalysis
    {
        protected TextWriter Debug { get; }
        public IGrammar Grammar { get; }
        public IReadOnlyCollection<IState> AllStates { get; }
        public IReadOnlyDictionary<IState, Boolean> IsNullable { get; }
        public IReadOnlyDictionary<IState, IReadOnlyCollection<Char>> First { get; }

        public IEnumerable<Transition> AllTransitions =>
            AllStates.SelectMany(state => state.Transitions);

        public IEnumerable<Transition.Call> CallTransitions =>
            AllTransitions.OfType<Transition.Call>();

        public IEnumerable<Transition.Reduce> ReduceTransitions =>
            AllTransitions.OfType<Transition.Reduce>();

        public IEnumerable<Transition.Shift> ShiftTransitions =>
            AllTransitions.OfType<Transition.Shift>();

        public GrammarAnalysis(IGrammar grammar, Boolean debug = false)
        {
            Debug = debug ? Console.Error : TextWriter.Null;
            Grammar = grammar;
            AllStates = ComputeAllStates();
            IsNullable = ComputeIsNullable();
            First = ComputeFirst();
        }

        private IReadOnlyCollection<IState> ComputeAllStates()
        {
            var reachable = new HashSet<IState> { Grammar.StopState};
            var queue = new Queue<IState>();
            if (reachable.Add(Grammar.StartState))
                queue.Enqueue(Grammar.StartState);
            while (queue.Count > 0)
            {
                var state = queue.Dequeue();
                foreach (var transition in state.Transitions)
                    switch (transition)
                    {
                        case Transition.Call call:
                            if (reachable.Add(call.Child))
                                queue.Enqueue(call.Child);
                            if (reachable.Add(call.Next))
                                queue.Enqueue(call.Next);
                            break;
                        case Transition.Shift shift:
                            if (reachable.Add(shift.Next))
                                queue.Enqueue(shift.Next);
                            break;
                    }
            }
            Debug.WriteLine($"Reachable grammar states: {reachable.Count}");
            return reachable;
        }

        private IReadOnlyDictionary<IState, Boolean> ComputeIsNullable()
        {
            var zero = new HashSet<IState>();
            var one = new Dictionary<IState, HashSet<IState>>();
            var two = new Dictionary<IState, Dictionary<IState, HashSet<IState>>>();
            var queue = new Queue<IState>();
            void Zero(IState target)
            {
                if (zero.Add(target))
                    queue.Enqueue(target);
            }
            void One(IState source, IState target)
            {
                if (zero.Contains(source))
                    Zero(target);
                else
                    one.GetOrCreate(source).Add(target);
            }
            foreach (var call in CallTransitions)
            {
                two.GetOrCreate(call.Child).GetOrCreate(call.Next).Add(call.Source);
                two.GetOrCreate(call.Next).GetOrCreate(call.Child).Add(call.Source);
            }
            foreach (var reduce in ReduceTransitions)
                Zero(reduce.Source);
            while (queue.Count > 0)
            {
                var state = queue.Dequeue();
                if (one.TryGetValue(state, out var targets1))
                    foreach (var target in targets1)
                        Zero(target);
                if (two.TryGetValue(state, out var pairs))
                    foreach (var (source, targets2) in pairs)
                        foreach (var target in targets2)
                            One(source, target);
            }
            var result = new Dictionary<IState, Boolean>();
            foreach (var state in AllStates)
                result.Add(state, zero.Contains(state));
            Debug.WriteLine($"Nullable states: {zero.Count}");
            return result;
        }

        protected static Int32 CloseUnder<TNeighbors>(HashSet<IState> states, IReadOnlyDictionary<IState, TNeighbors> implied)
            where TNeighbors : IEnumerable<IState>
        {
            var queue = new Queue<IState>(states);
            while (queue.Count > 0)
                if (implied.TryGetValue(queue.Dequeue(), out var neighbors))
                    foreach (var neighbor in neighbors)
                        if (states.Add(neighbor))
                            queue.Enqueue(neighbor);
            return states.Count;
        }

        private IReadOnlyDictionary<IState, IReadOnlyCollection<Char>> ComputeFirst()
        {
            var initial = new Dictionary<Char, HashSet<IState>>();
            var implied = new Dictionary<IState, HashSet<IState>>();
            foreach (var call in CallTransitions)
            {
                implied.GetOrCreate(call.Child).Add(call.Source);
                if (IsNullable[call.Child])
                    implied.GetOrCreate(call.Next).Add(call.Source);
            }
            foreach (var shift in ShiftTransitions)
                initial.GetOrCreate(shift.Terminal).Add(shift.Source);
            var totalCount = initial.Values.Sum(states => CloseUnder(states, implied));
            Debug.WriteLine($"First entries: {totalCount}");
            return
                (from kvp in initial
                 from state in kvp.Value
                 group kvp.Key by state into g
                 select g).
                ToDictionary(g => g.Key, g => new HashSet<Char>(g) as IReadOnlyCollection<Char>);
        }
    }
}
