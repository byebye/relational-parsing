using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace Parser.Tests
{
    using Grammars;
    using Utilities;

    [TestFixture, Explicit]
    public class ExtensiveParsingTests : ParsingTests
    {
        public static SampleGrammar[] Grammars = new SampleGrammar[]
        {
            SampleGrammar.Trivial,
            SampleGrammar.FlatIteration,
            SampleGrammar.LeftRecursion,
            SampleGrammar.RightRecursion,
            SampleGrammar.Parentheses,
            SampleGrammar.HighlyAmbiguous,
        };

        public static readonly List<ParserGenerator> RecognizerGenerators
            = new Instantiator(typeof(ParserGenerator).Assembly) { { typeof(Relational.IWrapEngineFactory), "CommutativeWrapEngineFactory(null)" }}
                .InstantiateAll<ParserGenerator>()
                .OrderBy(g => g.ToString())
                .ToList();

        //[Test]
        public new void TestRecognizer([ValueSource(nameof(RecognizerGenerators))] ParserGenerator generator, [ValueSource(nameof(Grammars))] SampleGrammar grammar)
            => base.TestRecognizer(generator, grammar);

        public static readonly List<ParserGenerator> CounterGenerators
            = new Instantiator(typeof(ParserGenerator).Assembly) { { typeof(Relational.IWrapEngineFactory), "DagWrapEngineFactory" }}
                .InstantiateAll<ParserGenerator>()
                .Where(g => g.Features.HasFlag(ParserFeatures.Counting))
                .OrderBy(g => g.ToString())
                .ToList();

        //[Test]
        public new void TestCounter([ValueSource(nameof(CounterGenerators))] ParserGenerator generator, [ValueSource(nameof(Grammars))] SampleGrammar grammar)
            => base.TestCounter(generator, grammar);

        public static readonly List<ParserGenerator> ShapeParserGenerators
            = new Instantiator(typeof(ParserGenerator).Assembly)
                .InstantiateAll<ParserGenerator>()
                .Where(g => g.Features.HasFlag(ParserFeatures.IdempotentSemiring))
                .OrderBy(g => g.ToString())
                .ToList();

        //[Test]
        public new void TestShapeParser([ValueSource(nameof(ShapeParserGenerators))] ParserGenerator generator, [ValueSource(nameof(Grammars))] SampleGrammar grammar)
            => base.TestShapeParser(generator, grammar);
    }
}
