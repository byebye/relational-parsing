using System;
using System.Collections.Generic;
using System.Linq;
using No.Comparers;

namespace Parser.Relational
{
    using Algebra;
    using Graphs;
    using static EqualityComparerFactories;

    public class DagAtomEngine<TValue, TNode> : AtomEngine<TValue, TNode>
    {
        public override ISemiring<TValue> Semiring { get; }
        private readonly IGraphEngine<(TValue, TValue), TNode> GraphEngine;

        public DagAtomEngine(ISemiring<TValue> semiring, IGraphEngine<(TValue, TValue), TNode> graphEngine)
        {
            Semiring = semiring;
            GraphEngine = graphEngine;
            Root = GraphEngine.Create(((Semiring.Zero, Semiring.Zero), GraphEngine.Zero));
        }

        public override Boolean IsIdempotent
            => Semiring.IsIdempotent;

        private readonly TNode Root;

        public override IEqualityComparer<TNode> Comparer
            => GraphEngine.Comparer;

        public override TNode Add(IEnumerable<TNode> values)
            => GraphEngine.Add(values);

        public override TNode One
            => GraphEngine.Create(((Semiring.One, Semiring.One), Root));

        public override TNode Multiply(TNode left, (TValue, TValue) right)
            => (IsZero(left) || Semiring.IsZero(right.Item1) || Semiring.IsZero(right.Item2)) ? Zero :
                (Semiring.IsOne(right.Item1) && Semiring.IsOne(right.Item2)) ? left :
                GraphEngine.Create((right, left));

        public override TValue Wrap(TValue value, TNode atom)
        {
            var results = new Dictionary<TNode, TValue>(Comparer) { { Root, value } };
            return Dfs(atom);
            TValue Dfs(TNode node)
            {
                TValue result;
                if (!results.TryGetValue(node, out result))
                {
                    result = Semiring.Add(GraphEngine.Neighbors(node).Select(_ => Semiring.Multiply(_.Item1.Item1, Dfs(_.Item2), _.Item1.Item2)));
                    results.Add(node, result);
                }
                return result;
            }
        }
    }

    public class DagAtomEngineFactory : IAtomEngineFactory
    {
        private readonly IGraphEngineFactory GraphEngineFactory;

        public DagAtomEngineFactory(IGraphEngineFactory graphEngineFactory)
        {
            GraphEngineFactory = graphEngineFactory;
        }

        public IAtomEngine<TValue> Create<TValue>(ISemiring<TValue> semiring)
            => GraphEngineFactory
                .Create(
                    new ComponentsEqualityComparer<(TValue, TValue)>
                    {
                        { _ => _.Item1, semiring.Comparer },
                        { _ => _.Item2, semiring.Comparer, false },
                    },
                    (x, y) => (x.Item1, semiring.Add(x.Item2, y.Item2)),
                    semiring.IsIdempotent)
                .Accept(new CreateRequest<TValue> { Semiring = semiring });

        private class CreateRequest<TValue> : IGraphEngineVisitor<(TValue, TValue), IAtomEngine<TValue>>
        {
            public ISemiring<TValue> Semiring { get; set; }

            public IAtomEngine<TValue> Visit<TNode>(IGraphEngine<(TValue, TValue), TNode> engine)
                => new DagAtomEngine<TValue, TNode>(Semiring, engine);
        }

        public override String ToString()
            => $"{nameof(DagAtomEngineFactory)}({GraphEngineFactory})";
    }
}
