using System;
using System.IO;
using System.Reflection;

namespace Parser.Benchmarks
{
    using Grammars;

    public class EmbeddedAntlr4ParserGenerator : ParserGenerator
    {
        public override ParserFeatures Features
            => ParserFeatures.Recognition;

        public override IParser<Boolean> CreateRecognizer(IGrammar grammar)
        {
            if (grammar is Antlr4Grammar antlr4)
            {
                var assembly = Assembly.GetAssembly(typeof(EmbeddedAntlr4ParserGenerator));
                Type parserType = null;
                foreach (var resource in assembly.GetManifestResourceNames())
                    using (var embeddedStream = assembly.GetManifestResourceStream(resource))
                        using (var reader = new StreamReader(embeddedStream))
                            if (reader.ReadToEnd() == antlr4.Source)
                            {
                                var parts = resource.Split('.');
                                parts[parts.Length - 1] = parts[parts.Length - 2] + "Parser";
                                parts[parts.Length - 2] = "Generated";
                                parserType = assembly.GetType(String.Join('.', parts));
                                if (parserType != null)
                                    break;
                            }
                if (parserType == null)
                    throw new NotSupportedException($"No embedded parser found for the given grammar");
                var inner = Activator.CreateInstance(parserType, new Object[] { null });
                return (IParser<Boolean>) Activator.CreateInstance(typeof(Antlr4Parser<>).MakeGenericType(parserType), inner);
            }
            throw new NotSupportedException($"{nameof(Antlr4InterpreterGenerator)} can only generate parsers for {nameof(Antlr4Grammar)}s");
        }
    }
}
