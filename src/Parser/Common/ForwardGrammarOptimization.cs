using System;
using System.Collections.Generic;
using No.Comparers;

namespace Parser.Common
{
    using Algebra;
    using Utilities;
    using static EqualityComparerFactories;

    public class ForwardGrammarOptimization : GrammarOptimization
    {
        public override IGrammar Perform<TValue>(IGrammar grammar, IValuationSemiring<TValue> semiring)
        {
            var classification = new Classifier<TValue>(grammar, semiring).Run();
            if (classification == Classifier<TValue>.NoClassification)
                return grammar;
            var processed = new HashSet<GroupedState>();
            foreach (var (source, newSource) in classification)
                if (processed.Add(newSource))
                    foreach (var transition in source.Transitions)
                        switch (transition)
                        {
                            case Transition.Call call:
                            {
                                var newChild = classification[call.Child];
                                var newNext = classification[call.Next];
                                newSource.Transitions.Add(new Transition.Call(newSource, newChild, newNext));
                                break;
                            }
                            case Transition.Shift shift:
                            {
                                var newNext = classification[shift.Next];
                                newSource.Transitions.Add(new Transition.Shift(newSource, shift.Terminal, newNext));
                                break;
                            }
                            case Transition.Reduce reduce:
                            {
                                newSource.Transitions.Add(new Transition.Reduce(newSource));
                                break;
                            }
                        }
            return new GroupedGrammar { StartState = classification[grammar.StartState], StopState = classification[grammar.StopState] };
        }

        private class Signature<TValue>
        {
            public Dictionary<(GroupedState, GroupedState), TValue> Calls = new Dictionary<(GroupedState, GroupedState), TValue>();
            public Dictionary<(Char, GroupedState), TValue> Shifts = new Dictionary<(Char, GroupedState), TValue>();
            public Dictionary<Int32, TValue> Reduces = new Dictionary<Int32, TValue>();
        }

        private class Classifier<TValue> : Classifier<IState, Signature<TValue>, GroupedState>
        {
            private readonly GrammarAnalysis Analysis;
            private readonly IValuationSemiring<TValue> Semiring;
            protected override IEqualityComparer<Signature<TValue>> SignatureEC { get; }

            public Classifier(IGrammar grammar, IValuationSemiring<TValue> semiring)
            {
                Analysis = new GrammarAnalysis(grammar);
                Semiring = semiring;
                SignatureEC = new ComponentsEqualityComparer<Signature<TValue>>
                {
                    { _ => _.Calls, DictionaryEC(ValueTupleEC(GroupedStateEC, GroupedStateEC), Semiring.Comparer) },
                    { _ => _.Shifts, DictionaryEC(ValueTupleEC(OperatorEC<Char>(), GroupedStateEC), Semiring.Comparer) },
                    { _ => _.Reduces, DictionaryEC(OperatorEC<Int32>(), Semiring.Comparer) },
                };
            }

            protected override IEnumerable<IState> Elements
                => Analysis.AllStates;


            protected override IEnumerable<(IState, Signature<TValue>)> Signatures(IReadOnlyDictionary<IState, GroupedState> classification)
            {
                var result = new Dictionary<IState, Signature<TValue>>();
                foreach (var (source, sourceClass) in classification)
                {
                    var signature = result.GetOrCreate(source);
                    foreach (var transition in source.Transitions)
                    {
                        var value = Semiring.Atomic(transition);
                        switch (transition)
                        {
                            case Transition.Call call:
                            {
                                var key = (classification[call.Child], classification[call.Next]);
                                signature.Calls[key] = signature.Calls.TryGetValue(key, out var current) ? Semiring.Add(current, value) : value;
                                break;
                            }
                            case Transition.Shift shift:
                            {
                                var key = (shift.Terminal, classification[shift.Next]);
                                signature.Shifts[key] = signature.Shifts.TryGetValue(key, out var current) ? Semiring.Add(current, value) : value;
                                break;
                            }
                            case Transition.Reduce reduce:
                            {
                                var key = 0;
                                signature.Reduces[key] = signature.Reduces.TryGetValue(key, out var current) ? Semiring.Add(current, value) : value;
                                break;
                            }
                        }
                    }
                }
                foreach (var (state, signature) in result)
                    yield return (state, signature);
            }

            public override String ToString()
                => $"{nameof(ForwardGrammarOptimization)}";
        }

        public override String ToString()
            => $"{nameof(ForwardGrammarOptimization)}";
    }
}
