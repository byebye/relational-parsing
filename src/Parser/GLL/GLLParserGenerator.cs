using System;

namespace Parser.GLL
{
    public class GLLParserGenerator : ParserGenerator
    {
        public override ParserFeatures Features =>
            ParserFeatures.Recognition;
        public Boolean UseLookahead { get; }

        public GLLParserGenerator(Boolean useLookahead)
        {
            UseLookahead = useLookahead;
        }

        public override IParser<Boolean> CreateRecognizer(IGrammar grammar) =>
            new GLLParser(grammar, UseLookahead);

        public override String ToString()
            => $"{nameof(GLLParserGenerator)}({UseLookahead})";
    }
}
