using System;
using System.Collections.Generic;
using System.Linq;

namespace Parser.GLL
{
    using Common;
    using Valuations;
    using Descriptors = WorkingSet<(IState, Common.GSSNode<IState>)>;

    public partial class GLLParser : IParser<Boolean>
    {
        private readonly IGrammar Grammar;
        private readonly LookaheadAnalysis Analysis;
        public IValuationSemiring<Boolean> Semiring { get; }
            = new BooleanSemiring();

        public GLLParser(IGrammar grammar, Boolean useLookahead)
        {
            Grammar = grammar;
            if (useLookahead)
                Analysis = new LookaheadAnalysis(grammar);
        }

        private Boolean CallerAndReducer(Descriptors descriptors, Char? lookahead, GSSNode<IState> rootNode, Int32 layerIndex, ref Statistics statistics)
        {
            Boolean accept = false;
            var layer = new GSSLayer<IState>(layerIndex);
            var popped = new HashSet<GSSNode<IState>>();
            foreach (var (state, node) in descriptors.Remaining)
            {
                foreach (var transition in state.Transitions)
                    if (Analysis == null || Analysis.IsViable(transition, lookahead))
                        switch (transition)
                        {
                            case Transition.Call call:
                                var parent = layer.AddNode(call.Next, ref statistics);
                                if (parent.AddChild(node, ref statistics))
                                    if (popped.Contains(parent))
                                        if (descriptors.Add((call.Next, node)))
                                            statistics.NewEdges += 1;
                                if (descriptors.Add((call.Child, parent)))
                                    statistics.NewEdges += 1;
                                break;
                            case Transition.Reduce reduce:
                                if (node == rootNode)
                                    accept = true;
                                if (popped.Add(node))
                                    foreach (var child in node.Children)
                                    {
                                        statistics.VisitedEdges += 1;
                                        if (descriptors.Add((node.Label, child)))
                                            statistics.NewEdges += 1;
                                    }
                                break;
                        }
                statistics.VisitedEdges += 1;
            }
            return accept;
        }

        private void Shifter(Descriptors currentDescriptors, Descriptors nextDescriptors, Char terminal, ref Statistics statistics)
        {
            foreach (var (state, node) in currentDescriptors.All)
                foreach (var shift in state.Transitions.OfType<Transition.Shift>())
                    if (shift.Terminal == terminal)
                        if (nextDescriptors.Add((shift.Next, node)))
                            statistics.NewEdges += 1;
        }

        public IEnumerable<Statistics> Run(IEnumerable<Char> input, Action<Boolean> withResult)
        {
            var layerIndex = 0;
            var statistics = new Statistics();
            var currentDescriptors = new Descriptors();
            var specialState = new EpsilonState();
            var root = new GSSNode<IState>(specialState, 0);
            currentDescriptors.Add((Grammar.StartState, root));
            Boolean accept;
            foreach (var terminal in input)
            {
                CallerAndReducer(currentDescriptors, terminal, root, layerIndex, ref statistics);
                statistics.Graph = currentDescriptors;
                yield return statistics;
                layerIndex += 1;
                statistics = new Statistics();
                var nextDescriptors = new Descriptors();
                Shifter(currentDescriptors, nextDescriptors, terminal, ref statistics);
                currentDescriptors = nextDescriptors;
            }
            accept = CallerAndReducer(currentDescriptors, null, root, layerIndex, ref statistics);
            statistics.Graph = currentDescriptors;
            yield return statistics;
            withResult(accept);
        }

        private sealed class EpsilonState : IState
        {
            public IEnumerable<Transition> Transitions =>
                Enumerable.Empty<Transition>();

            public String Name =>
                "$\\epsilon$";

            public Boolean Equals(IState other) =>
                ReferenceEquals(this, other);
        }
    }
}
