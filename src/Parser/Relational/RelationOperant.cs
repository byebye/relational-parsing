using System;

namespace Parser.Relational
{
    using Algebra;

    public interface IRelationalOperation<TValue, TAtom> : IEquatable<IRelationalOperation<TValue, TAtom>>
    {
        TRelation Perform<TWrap, TRelation>(IRelationEngine<TValue, TAtom, TWrap, TRelation> engine, TRelation relation);
    }

    public interface IRelationOperantFactory
    {
        IRelationOperant<TValue> Create<TValue>(IValuationSemiring<TValue> semiring);
    }

    public interface IRelationOperant<TValue>
    {
        TResult Accept<TResult>(IRelationOperantVisitor<TValue, TResult> visitor);
    }

    public interface IRelationOperantVisitor<TValue, out TResult> : IRelationEngineVisitor<TValue, TResult>
    {
        TResult Visit<TAtom, TRelation>(IRelationOperant<TValue, TAtom, TRelation> operant);
    }

    public interface IRelationOperant<TValue, TAtom, TRelation> : IOne<TRelation>, IRelationOperant<TValue>
    {
        IAtomEngine<TValue, TAtom> AtomEngine { get; }
        TValue FlatEpsilon(TRelation relation);
        void Perform(IRelationalOperation<TValue, TAtom> operation, ref TRelation relation);
    }

    public abstract class RelationOperant<TValue, TAtom, TRelation> : Set<TRelation>, IRelationOperant<TValue, TAtom, TRelation>
    {
        public abstract IAtomEngine<TValue, TAtom> AtomEngine { get; }
        public abstract TValue FlatEpsilon(TRelation relation);
        public virtual Boolean IsOne(TRelation relation)
            => Comparer.Equals(relation, One);
        public abstract TRelation One { get; }
        public abstract void Perform(IRelationalOperation<TValue, TAtom> operation, ref TRelation relation);

        TResult IRelationOperant<TValue>.Accept<TResult>(IRelationOperantVisitor<TValue, TResult> visitor)
            => visitor.Visit(this);
    }
}
