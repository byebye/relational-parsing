using System;
using System.Collections.Generic;
using System.Linq;

namespace Parser.Relational
{
    using Algebra;

    public interface IWrapEngineFactory
    {
        IWrapEngine<TValue> Create<TValue>(ISemiring<TValue> semiring);
    }

    public interface IWrapEngine<TValue>
    {
        TResult Accept<TResult>(IWrapEngineVisitor<TValue, TResult> visitor);
    }

    public interface IWrapEngineVisitor<TValue, out TResult>
    {
        TResult Visit<TAtom, TWrap>(IWrapEngine<TValue, TAtom, TWrap> engine);
    }

    public interface IWrapEngine<TValue, TAtom, TWrap> : IMultiplicative<TWrap>, ILeftDistributive<TWrap, TAtom>, IWrapEngine<TValue>
    {
        IAtomEngine<TValue, TAtom> AtomEngine { get; }
        TValue Flatten(TWrap wrap);
        TWrap Fold(TWrap wrap);
        TWrap Multiply(TValue left, TWrap right);
    }

    public abstract class WrapEngine<TValue, TAtom, TWrap> : Semiring<TWrap>, IWrapEngine<TValue, TAtom, TWrap>
    {
        public abstract IAtomEngine<TValue, TAtom> AtomEngine { get; }
        public abstract TValue Flatten(TWrap wrap);
        public virtual TWrap Fold(TWrap wrap)
            => IsZero(wrap) || IsOne(wrap) ? wrap : Multiply(Flatten(wrap), One);
        public virtual TWrap Multiply(TValue left, TWrap right)
            => Multiply(AtomEngine.Embed((AtomEngine.Semiring.One, left)), right);
        public abstract TWrap Multiply(TAtom left, TWrap right);
        public override TWrap Multiply(IEnumerable<TWrap> values)
        {
            var reverse = new Stack<TWrap>(values);
            var result = One;
            foreach (var value in reverse)
                result = Multiply(value, result);
            return result;
        }

        TResult IWrapEngine<TValue>.Accept<TResult>(IWrapEngineVisitor<TValue, TResult> visitor)
            => visitor.Visit(this);
    }
}
