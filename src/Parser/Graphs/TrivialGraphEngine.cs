using System;
using System.Collections.Generic;

namespace Parser.Graphs
{
    public class TrivialGraphEngine<TEdge> : GraphEngine<TEdge, TrivialGraphEngine<TEdge>.Node>
    {
        public class Node : List<(TEdge, Node)>
        {
            public Node(IEnumerable<(TEdge, Node)> items)
                : base(items)
            {
            }
        }

        public override Node Create(IEnumerable<(TEdge, Node)> edges)
            => new Node(edges);

        public override IEnumerable<(TEdge, Node)> Neighbors(Node node)
            => node;
    }

    public class TrivialGraphEngineFactory : IGraphEngineFactory
    {
        public IGraphEngine<TEdge> Create<TEdge>(IEqualityComparer<TEdge> edgeEC, Func<TEdge, TEdge, TEdge> mergeEdges, Boolean mergeIsIdempotent)
            => new TrivialGraphEngine<TEdge>();
    }
}
