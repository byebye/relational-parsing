using System;
using System.Collections.Generic;
using System.Linq;

namespace Parser.Latex
{
    using Common;

    public class GSSLayerDrawing<TLabel> : IDrawable
        where TLabel : IEquatable<TLabel>
    {
        private readonly GSSLayer<TLabel> Target;

        internal GSSLayerDrawing(GSSLayer<TLabel> target)
        {
            Target = target;
        }

        public void Draw(LatexDocument document)
        {
            using (var drawing = new LayeredDrawing<GSSNode<TLabel>>(document))
            {
                var visited = new HashSet<GSSNode<TLabel>>();
                GSSNode<TLabel> Dfs(GSSNode<TLabel> node)
                {
                    if (visited.Add(node))
                    {
                        drawing.AddNode(node, node.Label.ToString(), node.Children.Select(Dfs).ToList(), node.LayerIndex);
                    }
                    return node;
                }
                var union = new GSSNode<TLabel>(default(TLabel), 0);
                drawing.AddNode(union, "", Target.Select(Dfs).ToList());
            }
        }
    }
}
