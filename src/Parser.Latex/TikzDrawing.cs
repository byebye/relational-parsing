using System;
using System.IO;

namespace Parser.Latex
{
    public abstract class TikzDrawing : IDisposable
    {
        protected readonly LatexDocument Document;
        
        public TikzDrawing(LatexDocument document, String style = "")
        {
            Document = document;
            Document.WriteLine($"\\begin{{tikzpicture}}[{style}]");
        }

        public virtual void Dispose()
        {
            Document.WriteLine("\\end{tikzpicture}");
        }
    }
}