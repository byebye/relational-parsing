using System;
using System.Collections.Generic;
using System.Linq;

namespace Parser.Utilities
{
    public static class EnumerableExtensions
    {
        public static IReadOnlyCollection<T> AsReadOnlyCollection<T>(this IEnumerable<T> enumerable) =>
            enumerable as IReadOnlyCollection<T> ?? enumerable.ToArray();

        public static HashSet<T> AsHashSet<T>(this IEnumerable<T> enumerable, IEqualityComparer<T> comparer)
        {
            var set = enumerable as HashSet<T>;
            return (set?.Comparer == comparer) ? set : new HashSet<T>(enumerable, comparer);
        }

        public static IEnumerable<T> SkipLast<T>(this IEnumerable<T> source, Int32 count = 1)
        {
            Queue<T> buffer = new Queue<T>(count + 1);
            foreach (T x in source)
            {
                buffer.Enqueue(x);
                if (buffer.Count == count + 1)
                    yield return buffer.Dequeue();
            }
        }
    }
}
