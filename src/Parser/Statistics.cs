using System;

namespace Parser
{
    public struct Statistics
    {
        public Int32 MemoizedOperations;
        public Int32 NewNodes;
        public Int32 NewEdges;
        public Int32 VisitedEdges;
        public Object Graph;

        public void Append(Statistics other)
        {
            MemoizedOperations += other.MemoizedOperations;
            NewNodes += other.NewNodes;
            NewEdges += other.NewEdges;
            VisitedEdges += other.VisitedEdges;
            Graph = other.Graph ?? Graph;
        }

        public override String ToString() =>
            $"MemoizedOperations: {MemoizedOperations}, NewEdges: {NewEdges}, NewNodes: {NewNodes}, VisitedEdges: {VisitedEdges}";
    }
}
