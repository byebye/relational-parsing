using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using No.Comparers;

namespace Parser.Graphs
{
    using Utilities;
    using static EqualityComparerFactories;

    public class OrderedGraphEngine<TEdge> : GraphEngine<TEdge, OrderedGraphEngine<TEdge>.Node>, IComparer<(TEdge, OrderedGraphEngine<TEdge>.Node)>
    {
        [System.Diagnostics.DebuggerDisplay("{Depth} neighbor(s)")]
        [System.Diagnostics.DebuggerTypeProxy(typeof(CollectionDisplay))]
        public class Node : CollectionDisplay.ITarget
        {
            private static Int32 NextIndex = 0;
            public Int32 Index { get; } = NextIndex++;
            public Int32 Depth { get; }

            protected Node(Int32 depth)
            {
                Depth = depth;
            }

            public Node()
                : this(0)
            {
            }

            public IEnumerable Items
            {
                get
                {
                    for (var node = this; node is NonEmptyNode nonempty; node = nonempty.Parent)
                        yield return (nonempty.Edge, nonempty.Target);
                }
            }

            private Int32 DebugAppend(System.Text.StringBuilder builder, Dictionary<Node, Int32> mapping)
            {
                Int32 index;
                if (!mapping.TryGetValue(this, out index))
                {
                    var description = $"{{{String.Join(", ", Items.Cast<(TEdge, Node)>().Select(_ => $"({_.Item1},{_.Item2.DebugAppend(builder, mapping)})"))}}}";
                    index = mapping.Count;
                    builder.Append($"{index}: {description}, ");
                    mapping.Add(this, index);
                }
                return index;
            }

            public override String ToString()
            {
                var builder = new System.Text.StringBuilder();
                DebugAppend(builder, new Dictionary<Node, Int32>());
                builder.Length -= 2;
                return $"({builder})";
            }
        }

        public sealed class NonEmptyNode : Node
        {
            public readonly Node Parent;
            public readonly TEdge Edge;
            public readonly Node Target;

            public NonEmptyNode(Node parent, TEdge edge, Node target)
                : base(parent.Depth + 1)
            {
                Parent = parent;
                Edge = edge;
                Target = target;
            }
        }

        private readonly IEqualityComparer<TEdge> EdgeEC;
        private readonly Func<TEdge, TEdge, TEdge> MergeEdges;
        private readonly Boolean MergeIsIdempotent;
        public override IEqualityComparer<Node> Comparer { get; }
        private readonly Memoizer<NonEmptyNode> Memoizer;
        public override Node Zero { get; }
        private readonly Counter EdgesCreated = new Counter();
        private readonly Counter EdgesTraversed = new Counter();

        public OrderedGraphEngine(IEqualityComparer<TEdge> edgeEC, Func<TEdge, TEdge, TEdge> mergeEdges, Boolean mergeIsIdempotent)
        {
            EdgeEC = edgeEC;
            MergeEdges = mergeEdges;
            MergeIsIdempotent = mergeIsIdempotent;
            Comparer = new ComponentsEqualityComparer<Node> { { _ => _.Index, OperatorEC<Int32>() } };
            Memoizer = new Memoizer<NonEmptyNode>(new ComponentsEqualityComparer<NonEmptyNode>
            {
                { _ => _.Parent, Comparer },
                { _ => _.Edge, EdgeEC },
                { _ => _.Target, Comparer },
            }, _ => EdgesCreated.Increment());
            Zero = new Node();
        }

        private NonEmptyNode Add(Node node, TEdge edge, Node target)
            => Memoizer.Add(new NonEmptyNode(node, edge, target));

        public override IEnumerable<(TEdge, Node)> Neighbors(Node node)
        {
            while (node is NonEmptyNode nonempty)
            {
                EdgesTraversed.Increment();
                yield return (nonempty.Edge, nonempty.Target);
                node = nonempty.Parent;
            }
        }

        private Node Add(Node node, IEnumerable<(TEdge, Node)> edges)
        {
            foreach (var (edge, target) in edges)
            {
                if (node is NonEmptyNode nonempty && nonempty.Target == target && EdgeEC.GetHashCode(nonempty.Edge) == EdgeEC.GetHashCode(edge))
                    node = Add(nonempty.Parent, MergeEdges(nonempty.Edge, edge), target);
                else
                    node = Add(node, edge, target);
            }
            return node;
        }

        public Int32 Compare((TEdge, Node) one, (TEdge, Node) two)
        {
            var result = one.Item2.Index.CompareTo(two.Item2.Index);
            if (result == 0)
                result = EdgeEC.GetHashCode(one.Item1).CompareTo(EdgeEC.GetHashCode(two.Item1));
            return result;
        }

        public override Node Create(IEnumerable<(TEdge, Node)> edges)
            => Add(Zero, edges.OrderBy(_ => _, this));

        public override Node Map(Node node, Func<TEdge, TEdge> mapping)
            => node is NonEmptyNode nonempty ?
                Add(Map(nonempty.Parent, mapping), mapping(nonempty.Edge), nonempty.Target) :
                node;

        public override Node Add(Node one, Node two)
        {
            var edges = new Stack<(TEdge, Node)>();
            while ((one != two || !MergeIsIdempotent) && one is NonEmptyNode onePlus && two is NonEmptyNode twoPlus)
            {
                var compare = Compare((onePlus.Edge, onePlus.Target), (twoPlus.Edge, twoPlus.Target));
                if (compare >= 0)
                {
                    edges.Push((onePlus.Edge, onePlus.Target));
                    one = onePlus.Parent;
                }
                else
                {
                    edges.Push((twoPlus.Edge, twoPlus.Target));
                    two = twoPlus.Parent;
                }
            }
            return Add(one is NonEmptyNode ? one : two, edges);
        }

        public override Node Add(IEnumerable<Node> nodes)
        {
            var queue = new Queue<Node>(nodes);
            if (queue.Count == 0)
                return Zero;
            while (queue.Count > 1)
                queue.Enqueue(Add(queue.Dequeue(), queue.Dequeue()));
            return queue.Dequeue();
        }
    }

    public class OrderedGraphEngineFactory : IGraphEngineFactory
    {
        public IGraphEngine<TEdge> Create<TEdge>(IEqualityComparer<TEdge> edgeEC, Func<TEdge, TEdge, TEdge> mergeEdges, Boolean mergeIsIdempotent)
            => new OrderedGraphEngine<TEdge>(edgeEC, mergeEdges, mergeIsIdempotent);

        public override String ToString()
            => $"{nameof(OrderedGraphEngineFactory)}";
    }
}
