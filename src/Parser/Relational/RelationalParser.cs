using System;
using System.Collections.Generic;
using System.Linq;

namespace Parser.Relational
{
    using Utilities;

    public sealed class RelationalInitialization<TValue, TAtom> : IRelationalOperation<TValue, TAtom>
    {
        private readonly GrammarAnalysis<TValue> Analysis;
        private readonly IAtomicRelations<TAtom> AtomicRelations;

        public RelationalInitialization(GrammarAnalysis<TValue> analysis, IAtomicRelations<TAtom> atomicRelations)
        {
            Analysis = analysis;
            AtomicRelations = atomicRelations;
        }

        public TRelation Perform<TWrap, TRelation>(IRelationEngine<TValue, TAtom, TWrap, TRelation> engine, TRelation relation)
        {
            relation = engine.Multiply(AtomicRelations.StateRelations[Analysis.Grammar.StopState], relation);
            relation = engine.Multiply(AtomicRelations.StartRelation, relation);
            return relation;
        }

        public Boolean Equals(IRelationalOperation<TValue, TAtom> other) =>
            other is RelationalInitialization<TValue, TAtom>;

        public override Int32 GetHashCode() =>
            -1;
    }

    public sealed class RelationalPhase<TValue, TAtom> : IRelationalOperation<TValue, TAtom>
    {
        private readonly Boolean UseLookahead;
        private readonly GrammarAnalysis<TValue> Analysis;
        private readonly IAtomicRelations<TAtom> AtomicRelations;
        private readonly Char Symbol;

        public RelationalPhase(Boolean useLookahead, GrammarAnalysis<TValue> analysis, IAtomicRelations<TAtom> atomicRelations, Char symbol)
        {
            UseLookahead = useLookahead;
            Analysis = analysis;
            AtomicRelations = atomicRelations;
            Symbol = symbol;
        }

        private static Boolean NoFilter(IState state)
            => true;

        public TRelation Perform<TWrap, TRelation>(IRelationEngine<TValue, TAtom, TWrap, TRelation> engine, TRelation relation)
        {
            var partial = new Dictionary<IState, List<TRelation>>();
            var nulled = new List<TRelation>();
            Func<IState, Boolean> filter = NoFilter;
            if (UseLookahead)
            {
                HashSet<IState> origins;
                if (!Analysis.ShiftOrigins.TryGetValue(Symbol, out origins))
                    return engine.Zero;
                filter = origins.Contains;
            }
            foreach (var (state0, relations0) in engine.Derivatives(relation, filter))
                foreach (var transition in state0.Transitions)
                    if (transition is Transition.Shift shift && shift.Terminal == Symbol)
                        foreach (var relation0 in relations0)
                        {
                            var tail = engine.Multiply(Analysis.Semiring.Atomic(shift), relation0);
                            if (Analysis.ProductiveStates.Contains(shift.Next))
                                partial.GetOrCreate(shift.Next).Add(tail);
                            if (Analysis.NullingTransitions.TryGetValue(shift.Next, out var nulling))
                                nulled.Add(engine.Multiply(nulling, tail));
                        }
            if (nulled.Count > 0)
                foreach (var (state1, relations1) in engine.Derivatives(engine.Add(nulled), NoFilter))
                    partial.GetOrCreate(state1).AddRange(relations1);
            var currents = new List<TRelation>();
            foreach (var (state, relations) in partial)
                currents.Add(engine.Multiply(AtomicRelations.StateRelations[state],engine.Add(relations)));
            return engine.Add(currents);
        }

        public Boolean Equals(IRelationalOperation<TValue, TAtom> other) =>
            other is RelationalPhase<TValue, TAtom> phase && phase.Symbol == Symbol;

        public override Int32 GetHashCode() =>
            Symbol.GetHashCode();
    }

    public sealed class RelationalCompletion<TValue, TAtom> : IRelationalOperation<TValue, TAtom>
    {
        private readonly GrammarAnalysis<TValue> Analysis;

        public RelationalCompletion(GrammarAnalysis<TValue> analysis)
        {
            Analysis = analysis;
        }

        public TRelation Perform<TWrap, TRelation>(IRelationEngine<TValue, TAtom, TWrap, TRelation> engine, TRelation relation)
            => engine.Add(engine.Derivatives(relation, state => state == Analysis.Grammar.StopState).SelectMany(_ => _.Item2));

        public Boolean Equals(IRelationalOperation<TValue, TAtom> other) =>
            other is RelationalCompletion<TValue, TAtom>;

        public override Int32 GetHashCode() =>
            -2;
    }

    public class RelationalParser<TValue, TAtom, TRelation> : IParser<TValue>
    {
        private readonly GrammarAnalysis<TValue> Analysis;
        private readonly IAtomicRelations<TAtom> AtomicRelations;
        private readonly IRelationOperant<TValue, TAtom, TRelation> RelationOperant;
        private readonly Boolean UseLookahead;
        public IValuationSemiring<TValue> Semiring
            => Analysis.Semiring;

        public RelationalParser(GrammarAnalysis<TValue> analysis, IAtomicRelations<TAtom> atomicRelations, IRelationOperant<TValue, TAtom, TRelation> relationOperant, Boolean useLookahead)
        {
            Analysis = analysis;
            AtomicRelations = atomicRelations;
            RelationOperant = relationOperant;
            UseLookahead = useLookahead;
        }

        public IEnumerable<Statistics> Run(IEnumerable<Char> input, Action<TValue> withResult)
        {
            TRelation current = RelationOperant.One;
            RelationOperant.Perform(new RelationalInitialization<TValue, TAtom>(Analysis, AtomicRelations), ref current);
            yield return new Statistics();
            foreach (var symbol in input)
            {
                RelationOperant.Perform(new RelationalPhase<TValue, TAtom>(UseLookahead, Analysis, AtomicRelations, symbol), ref current);
                yield return new Statistics();
            }
            RelationOperant.Perform(new RelationalCompletion<TValue, TAtom>(Analysis), ref current);
            withResult(RelationOperant.FlatEpsilon(current));
        }
    }
}
