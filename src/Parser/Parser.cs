using System;
using System.Collections.Generic;

namespace Parser
{
    public interface IParser<TValue>
    {
        IValuationSemiring<TValue> Semiring { get; }
        IEnumerable<Statistics> Run(IEnumerable<Char> input, Action<TValue> withResult);
    }

    public static class ParserExtensions
    {
        public static TValue Run<TValue>(this IParser<TValue> parser, IEnumerable<Char> input, out Statistics statistics)
        {
            statistics = new Statistics();
            var hasResult = false;
            TValue result = default(TValue);
            foreach (var stat in parser.Run(input, r => { result = r; hasResult = true; }))
                statistics.Append(stat);
            if (!hasResult)
                throw new InvalidOperationException("the parser has not returned a result");
            return result;
        }

        public static TValue Run<TValue>(this IParser<TValue> parser, IEnumerable<Char> input) =>
            Run(parser, input, out var _);
    }
}
