using System;
using System.Collections.Generic;
using System.Linq;
using No.Comparers;
using NUnit.Framework;

namespace Parser.Tests
{
    using Algebra;
    using Valuations;

    public class DebugSemiring<TInner> : ValuationSemiring<DebugSemiring<TInner>.Summary>
    {
        public struct Summary
        {
            public TInner Inner;
            public FreeValue Structure;
            public List<Char> Terminals;
            public List<IState> Consumed;
            public List<IState> Produced;

            public override String ToString() => Structure.ToString();
        }

        private static readonly StructuralSemiring Structural =
            //new StructuralSemiring(System.Console.Error.WriteLine, true);
            new StructuralSemiring();

        public IValuationSemiring<TInner> Inner { get; }
        public override IEqualityComparer<Summary> Comparer { get; }

        public DebugSemiring(IValuationSemiring<TInner> inner)
        {
            Inner = inner;
            Comparer = new ComponentsEqualityComparer<Summary>
            {
                { _ => _.Structure, Structural.Comparer },
            };
        }

        public override Summary Atomic(Transition transition)
        {
            switch (transition)
            {
                case Transition.Call call:
                    return new Summary
                    {
                        Inner = Inner.Atomic(call),
                        Structure = Structural.Atomic(call),
                        Terminals = new List<Char> { },
                        Consumed = new List<IState> { call.Source },
                        Produced = new List<IState> { call.Child, call.Next },
                    };
                case Transition.Shift shift:
                    return new Summary
                    {
                        Inner = Inner.Atomic(shift),
                        Structure = Structural.Atomic(shift),
                        Terminals = new List<Char> { shift.Terminal },
                        Consumed = new List<IState> { shift.Source },
                        Produced = new List<IState> { shift.Next },
                    };
                case Transition.Reduce reduce:
                    return new Summary
                    {
                        Inner = Inner.Atomic(reduce),
                        Structure = Structural.Atomic(reduce),
                        Terminals = new List<Char> { },
                        Consumed = new List<IState> { reduce.Source },
                        Produced = new List<IState> { },
                    };
                default:
                    throw new InvalidOperationException();
            }
        }

        public override Summary Add(IEnumerable<Summary> items)
        {
            List<Char> terminals = null;
            List<IState> consumed = null;
            List<IState> produced = null;
            var inner = new List<TInner>();
            var structures = new List<FreeValue>();
            foreach (var item in items)
            {
                inner.Add(item.Inner);
                structures.Add(item.Structure);
                if (item.Terminals == null)
                    continue;
                if (terminals != null)
                {
                    Assert.True(terminals.SequenceEqual(item.Terminals));
                    var consumedPlus = new List<IState>();
                    var consumedMinus = new List<IState>();
                    var producedPlus = new List<IState>();
                    var producedMinus = new List<IState>();
                    Merge(item.Consumed, consumed, consumedPlus, consumedMinus);
                    Merge(item.Produced, produced, producedPlus, producedMinus);
                    Assert.True(consumedPlus.SequenceEqual(producedPlus));
                    Assert.True(consumedMinus.SequenceEqual(producedMinus));
                    consumed.AddRange(consumedPlus);
                    produced.AddRange(producedPlus);
                }
                else
                {
                    terminals = item.Terminals;
                    consumed = new List<IState>(item.Consumed);
                    produced = new List<IState>(item.Produced);
                }
            }
            return new Summary { Inner = Inner.Add(inner), Structure = Structural.Add(structures), Terminals = terminals, Consumed = consumed, Produced = produced };
        }

        public override Summary Multiply(IEnumerable<Summary> items)
        {
            var inner = new Stack<TInner>();
            var structures = new Stack<FreeValue>();
            var terminals = new List<Char>();
            var consumed = new List<IState>();
            var produced = new List<IState>();
            foreach (var item in items.Reverse())
            {
                if (item.Terminals == null)
                    return item;
                var newProduced = new List<IState>(item.Produced);
                try
                {
                    Merge(item.Consumed, produced, consumed, newProduced);
                }
                catch (Exception e)
                {
                    throw new Exception($"Cannot multiply #{String.Join("*", structures)} ([{String.Join(", ", consumed)}] -> [{String.Join(", ", produced)}]) by #{item.Structure} ([{String.Join(", ", item.Consumed)}] -> [{String.Join(", ", item.Produced)}])", e);
                }
                produced = newProduced;
                inner.Push(item.Inner);
                structures.Push(item.Structure);
                terminals.AddRange(item.Terminals);
            }
            return new Summary { Inner = Inner.Multiply(inner), Structure = Structural.Multiply(structures), Terminals = terminals, Consumed = consumed, Produced = produced };
        }

        private void Merge(IEnumerable<IState> source1, IEnumerable<IState> source2, List<IState> target1, List<IState> target2)
        {
            var move1 = true;
            var move2 = true;
            using (var enumerator1 = source1.GetEnumerator())
            using (var enumerator2 = source2.GetEnumerator())
            while (move1 || move2)
            {
                move1 = move1 && enumerator1.MoveNext();
                move2 = move2 && enumerator2.MoveNext();
                if (move1 && move2)
                {
                    if (enumerator1.Current != enumerator2.Current)
                        throw new Exception($"Cannot unify [{String.Join(", ", source1)}] and [{String.Join(", ", source2)}].");
                }
                else if (move1)
                    target1.Add(enumerator1.Current);
                else if (move2)
                    target2.Add(enumerator2.Current);
            }
        }
    }
}
