﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Parser.Common
{
    using Utilities;

    public class LookaheadAnalysis : GrammarAnalysis
    {
        public IReadOnlyDictionary<IState, (IReadOnlyCollection<Char>, Boolean)> Follow { get; }
        public IReadOnlyDictionary<IState, (IReadOnlyCollection<Char>, Boolean)> FirstPlus { get; }
        public IReadOnlyDictionary<Transition, (IReadOnlyCollection<Char>, Boolean)> ViableLookahead { get; }


        public Boolean IsViable(Transition transition, Char? lookahead) =>
            lookahead.HasValue ? ViableLookahead[transition].Item1.Contains(lookahead.Value) : ViableLookahead[transition].Item2;

        public LookaheadAnalysis(IGrammar grammar, Boolean debug = false)
            : base(grammar, debug)
        {
            Follow = ComputeFollow();
            FirstPlus = ComputeFirstPlus();
            ViableLookahead = ComputeViableLookahead();
        }

        private IReadOnlyDictionary<IState, (IReadOnlyCollection<Char>, Boolean)> ComputeFollow()
        {
            var initial = new Dictionary<Char, HashSet<IState>>();
            var implied = new Dictionary<IState, HashSet<IState>>();
            foreach (var call in CallTransitions)
            {
                implied.GetOrCreate(call.Source).Add(call.Next);
                if (First.TryGetValue(call.Next, out var first))
                    foreach (var terminal in first)
                        initial.GetOrCreate(terminal).Add(call.Child);
                if (IsNullable[call.Next])
                    implied.GetOrCreate(call.Next).Add(call.Child);
            }
            foreach (var shift in ShiftTransitions)
                implied.GetOrCreate(shift.Source).Add(shift.Next);
            var final = new HashSet<IState> { Grammar.StartState };
            var totalCount = initial.Values.Sum(states => CloseUnder(states, implied)) + CloseUnder(final, implied);
            Debug.WriteLine($"Follow entries: {totalCount}");
            var result = new Dictionary<IState, (IReadOnlyCollection<Char>, Boolean)>();
            var nothing = new Char[0];
            foreach (var state in final)
                result.Add(state, (nothing, true));
            foreach (var (terminal, states) in initial)
                foreach (var state in states)
                {
                    var pair = result.GetOrCreate(state);
                    var set = pair.Item1 as HashSet<Char> ?? new HashSet<Char>();
                    set.Add(terminal);
                    pair.Item1 = set;
                    result[state] = pair;
                }
            return result;
        }

        private IReadOnlyDictionary<IState, (IReadOnlyCollection<Char>, Boolean)> ComputeFirstPlus()
        {
            var result = new Dictionary<IState, (IReadOnlyCollection<Char>, Boolean)>();
            foreach (var state in AllStates)
            {
                var combined = new HashSet<Char>();
                var final = false;
                if (First.TryGetValue(state, out var first))
                    combined.UnionWith(first);
                if (IsNullable[state] && Follow.TryGetValue(state, out var follow))
                {
                    combined.UnionWith(follow.Item1);
                    final = follow.Item2;
                }
                result.Add(state, (combined, final));
            }
            return result;
        }

        private IReadOnlyDictionary<Transition, (IReadOnlyCollection<Char>, Boolean)> ComputeViableLookahead()
        {
            var result = new Dictionary<Transition, (IReadOnlyCollection<Char>, Boolean)>();
            foreach (var call in CallTransitions)
            {
                var viable = new HashSet<Char>();
                var final = false;
                if (First.TryGetValue(call.Child, out var first))
                    viable.UnionWith(first);
                if (IsNullable[call.Child])
                {
                    viable.UnionWith(FirstPlus[call.Next].Item1);
                    final = FirstPlus[call.Next].Item2;
                }
                result.Add(call, (viable, final));
            }
            foreach (var shift in ShiftTransitions)
                result.Add(shift, (new [] { shift.Terminal }, false));
            foreach (var reduce in ReduceTransitions)
                result.Add(reduce, Follow.TryGetValue(reduce.Source, out var follow) ? follow : (new Char[0], false));
            return result;
        }
    }
}
