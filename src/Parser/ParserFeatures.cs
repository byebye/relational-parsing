using System;

namespace Parser
{
    [Flags]
    public enum ParserFeatures
    {
        Recognition = 0,
        Counting = 1,
        IdempotentSemiring = 2,
        ArbitrarySemiring = 4 | IdempotentSemiring | Counting,
    }
}
