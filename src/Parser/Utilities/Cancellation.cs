using System;
using System.Diagnostics;

namespace Parser.Utilities
{
    public static class Cancellation
    {
        private static Boolean? Scheduled;

        [Conditional("DEBUG")]
        public static void Check()
        {
            try
            {
                if (Scheduled.Value)
                    throw new OperationCanceledException();
            }
            catch (InvalidOperationException)
            {
                Scheduled = false;
                Console.CancelKeyPress += (s, e) =>
                {
                    e.Cancel = true;
                    Scheduled = true;
                };
            }
        }
    }
}
