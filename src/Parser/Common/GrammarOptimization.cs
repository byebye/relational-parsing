using System;
using System.Collections.Generic;
using No.Comparers;

namespace Parser.Common
{
    using static EqualityComparerFactories;

    public abstract class GrammarOptimization
    {
        public abstract IGrammar Perform<TValue>(IGrammar grammar, IValuationSemiring<TValue> semiring);

        protected sealed class GroupedGrammar : IGrammar
        {
            public IState StartState { get; set; }
            public IState StopState { get; set; }
        }

        protected sealed class GroupedState : List<IState>, IState
        {
            public List<Transition> Transitions
                = new List<Transition>();

            public String Name
                => String.Join("~", this);

            public Boolean Equals(IState other)
                => ReferenceEquals(this, other);

            public override String ToString()
                => Name;

            IEnumerable<Transition> IState.Transitions
                => Transitions;
        }

        protected static IEqualityComparer<GroupedState> GroupedStateEC
            => UniqueHashCodeEC(IdentityEC<GroupedState>());
    }
}
