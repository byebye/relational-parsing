using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Parser.Latex
{
    public class LayeredDrawing<TNode> : TikzDrawing
    {
        private readonly List<Int32> Layers;
        private readonly Dictionary<TNode, (Int32, Int32)> Nodes;

        public LayeredDrawing(LatexDocument document)
            : base(document, "looseness=0.5,every node/.style={draw,circle,inner sep=0pt,minimum size=18pt,font=\\scriptsize}")
        {
            Layers = new List<Int32>();
            Nodes = new Dictionary<TNode, (Int32, Int32)>();
        }

        public LayeredDrawing(LatexDocument document, IEqualityComparer<TNode> nodeEC)
            : base(document)
        {
            Layers = new List<Int32>();
            Nodes = new Dictionary<TNode, (Int32, Int32)>(nodeEC);
        }

        private String Name(TNode node) =>
            (Nodes.TryGetValue(node, out var pair)) ?
                $"L{pair.Item1}I{pair.Item2}" :
                throw new KeyNotFoundException();

        private String Location(TNode node) =>
            (Nodes.TryGetValue(node, out var pair)) ?
                $"({.7*pair.Item2},{-pair.Item1})" :
                throw new KeyNotFoundException();

        public Boolean AddNode(TNode node, String label, Int32 layer)
        {
            if (Nodes.ContainsKey(node))
                return false;
            Layers.AddRange(Enumerable.Repeat(0, Math.Max(0, layer - Layers.Count + 1)));
            Int32 index = Layers[layer];
            Layers[layer] = index + 1;
            Nodes.Add(node, (layer, index));
            Document.WriteLine($"\\node ({Name(node)}) at {Location(node)} {{{label}}};");
            return true;
        }

        public void AddEdge(TNode source, TNode target, String label = null)
        {
            if (label != null)
                label = $" node [midway,text=blue] {{{label}}}";
            Document.WriteLine($"\\draw[gray] ({Name(source)}) to[out=90,in=270] ({Name(target)}){label};");
        }

        public Boolean AddNode(TNode node, String label, IReadOnlyCollection<TNode> neighbors, Int32 layer = 0)
        {
            foreach (var neighbor in neighbors)
                if (Nodes.TryGetValue(neighbor, out var pair))
                    layer = Math.Max(layer, pair.Item1 + 1);
            if (!AddNode(node, label, layer))
                return false;
            foreach (var neighbor in neighbors)
                AddEdge(node, neighbor);
            return true;
        }

        public Boolean AddNode(TNode node, String label, IReadOnlyCollection<(TNode, String)> neighbors, Int32 layer = 0)
        {
            foreach (var (neighbor, _) in neighbors)
                if (Nodes.TryGetValue(neighbor, out var pair))
                    layer = Math.Max(layer, pair.Item1 + 1);
            if (!AddNode(node, label, layer))
                return false;
            foreach (var (neighbor, label2) in neighbors)
                AddEdge(node, neighbor, label2);
            return true;
        }
    }
}
