using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using No.Comparers;

namespace Parser.Relational
{
    using Algebra;
    using Utilities;
    using static EqualityComparerFactories;

    public abstract class SubstitutionWrap<TValue, TAtom, TWrap>
    {
        public abstract override String ToString();
        private IReadOnlyCollection<Substitute> substitutes;

        public IReadOnlyCollection<Substitute> Substitutes
        {
            get
            {
                if (substitutes == null)
                {
                    var result = new List<Substitute>();
                    var seen = new HashSet<SubstitutionWrap<TValue, TAtom, TWrap>>();
                    var queue = new Queue<SubstitutionWrap<TValue, TAtom, TWrap>>();
                    if (seen.Add(this))
                        queue.Enqueue(this);
                    while (queue.Count > 0)
                    {
                        var node = queue.Dequeue();
                        switch (node)
                        {
                            case Add add:
                                foreach (var child in add.Children)
                                    if (seen.Add(child))
                                        queue.Enqueue(child);
                                break;
                            case ByAtom byAtom:
                                if (seen.Add(byAtom.Right))
                                    queue.Enqueue(byAtom.Right);
                                break;
                            case Fold fold:
                                if (seen.Add(fold.Child))
                                    queue.Enqueue(fold.Child);
                                break;
                            case Multiply multiply:
                                foreach (var child in multiply.Children)
                                    if (seen.Add(child))
                                        queue.Enqueue(child);
                                break;
                            case Substitute substitute:
                                result.Add(substitute);
                                break;
                        }
                    }
                    substitutes = result;
                }
                return substitutes;
            }
        }

        public class Add : SubstitutionWrap<TValue, TAtom, TWrap>
        {
            public IReadOnlyCollection<SubstitutionWrap<TValue, TAtom, TWrap>> Children;

            public override String ToString()
                => $"Add({String.Join(", ", Children)})";
        }

        public class ByAtom : SubstitutionWrap<TValue, TAtom, TWrap>
        {
            public TAtom Left;
            public SubstitutionWrap<TValue, TAtom, TWrap> Right;

            public override String ToString()
                => $"ByAtom({Left}, {Right})";
        }

        public class Fold : SubstitutionWrap<TValue, TAtom, TWrap>
        {
            public SubstitutionWrap<TValue, TAtom, TWrap> Child;

            public override String ToString()
                => $"Fold{Child}";
        }

        public class Inner : SubstitutionWrap<TValue, TAtom, TWrap>
        {
            public TWrap Value;

            public override String ToString()
                => Value.ToString();
        }

        public class Multiply : SubstitutionWrap<TValue, TAtom, TWrap>
        {
            public IReadOnlyCollection<SubstitutionWrap<TValue, TAtom, TWrap>> Children;

            public override String ToString()
                => $"Multiply({String.Join(", ", Children)})";
        }

        public class Substitute : SubstitutionWrap<TValue, TAtom, TWrap>
        {
            public Int32 DependencyDepth;
            public Int32 ContentIndex;

            public override String ToString()
                => $"Substitute({DependencyDepth}, {ContentIndex})";
        }
    }

    public class SubstitutionWrapEngine<TValue, TAtom, TWrap> : WrapEngine<TValue, TAtom, SubstitutionWrap<TValue, TAtom, TWrap>>
    {
        private class SubstitutionList : IReadOnlyList<SubstitutionWrap<TValue, TAtom, TWrap>>
        {
            private static readonly Dictionary<(Int32, Int32), SubstitutionWrap<TValue, TAtom, TWrap>> Cache
                = new Dictionary<(Int32, Int32), SubstitutionWrap<TValue, TAtom, TWrap>>();

            private readonly Int32 DependencyDepth;
            public Int32 Count { get; }

            public SubstitutionList(Int32 dependencyDepth, Int32 count)
            {
                DependencyDepth = dependencyDepth;
                Count = count;
            }

            public SubstitutionWrap<TValue, TAtom, TWrap> this[Int32 index]
            {
                get
                {
                    SubstitutionWrap<TValue, TAtom, TWrap> result;
                    if (!Cache.TryGetValue((DependencyDepth, index), out result))
                    {
                        result = new SubstitutionWrap<TValue, TAtom, TWrap>.Substitute { DependencyDepth = DependencyDepth, ContentIndex = index };
                        Cache.Add((DependencyDepth, index), result);
                    }
                    return result;
                }
            }

            public IEnumerator<SubstitutionWrap<TValue, TAtom, TWrap>> GetEnumerator()
            {
                for (var index = 0; index < Count; index++)
                    yield return this[index];
            }

            IEnumerator IEnumerable.GetEnumerator()
                => GetEnumerator();
        }

        public readonly IWrapEngine<TValue, TAtom, TWrap> InnerEngine;
        private readonly Dictionary<IReadOnlyCollection<SubstitutionWrap<TValue, TAtom, TWrap>>, SubstitutionWrap<TValue, TAtom, TWrap>> AddCache;
        private readonly Dictionary<(TAtom, SubstitutionWrap<TValue, TAtom, TWrap>), SubstitutionWrap<TValue, TAtom, TWrap>> ByAtomCache;
        private readonly Dictionary<SubstitutionWrap<TValue, TAtom, TWrap>, SubstitutionWrap<TValue, TAtom, TWrap>> FoldCache;
        private readonly Dictionary<IReadOnlyCollection<SubstitutionWrap<TValue, TAtom, TWrap>>, SubstitutionWrap<TValue, TAtom, TWrap>> MultiplyCache;

        public SubstitutionWrapEngine(IWrapEngine<TValue, TAtom, TWrap> innerEngine)
        {
            InnerEngine = innerEngine;
            AddCache = new Dictionary<IReadOnlyCollection<SubstitutionWrap<TValue, TAtom, TWrap>>, SubstitutionWrap<TValue, TAtom, TWrap>>(IsIdempotent ? MultiSetEC(Comparer) : SequenceEC(Comparer));
            ByAtomCache = new Dictionary<(TAtom, SubstitutionWrap<TValue, TAtom, TWrap>), SubstitutionWrap<TValue, TAtom, TWrap>>(ValueTupleEC(AtomEngine.Comparer, Comparer));
            FoldCache = new Dictionary<SubstitutionWrap<TValue, TAtom, TWrap>, SubstitutionWrap<TValue, TAtom, TWrap>>(Comparer);
            MultiplyCache = new Dictionary<IReadOnlyCollection<SubstitutionWrap<TValue, TAtom, TWrap>>, SubstitutionWrap<TValue, TAtom, TWrap>>(SequenceEC(Comparer));
        }

        public override IAtomEngine<TValue, TAtom> AtomEngine
            => InnerEngine.AtomEngine;

        public override Boolean IsCommutative
            => InnerEngine.IsCommutative;

        public override Boolean IsIdempotent
            => InnerEngine.IsIdempotent;

        public override Boolean IsZero(SubstitutionWrap<TValue, TAtom, TWrap> value)
            => value is SubstitutionWrap<TValue, TAtom, TWrap>.Add add && add.Children.Count == 0;

        public override Boolean IsOne(SubstitutionWrap<TValue, TAtom, TWrap> value)
            => value is SubstitutionWrap<TValue, TAtom, TWrap>.Multiply multiply && multiply.Children.Count == 0;

        public override SubstitutionWrap<TValue, TAtom, TWrap> Add(IEnumerable<SubstitutionWrap<TValue, TAtom, TWrap>> values)
        {
            var children = new List<SubstitutionWrap<TValue, TAtom, TWrap>>();
            foreach (var value in values)
            {
                if (IsZero(value))
                    continue;
                children.Add(value);
            }
            if (children.Count == 1)
                return children[0];
            var key = IsIdempotent ? children.AsHashSet(Comparer) : children.OrderBy(_ => _.GetHashCode()).AsReadOnlyCollection();
            SubstitutionWrap<TValue, TAtom, TWrap> result;
            if (!AddCache.TryGetValue(key, out result))
            {
                result = new SubstitutionWrap<TValue, TAtom, TWrap>.Add { Children = key };
                AddCache.Add(key, result);
            }
            return result;
        }

        public override TValue Flatten(SubstitutionWrap<TValue, TAtom, TWrap> wrap)
            => InnerEngine.Flatten(((SubstitutionWrap<TValue, TAtom, TWrap>.Inner)wrap).Value);

        public override SubstitutionWrap<TValue, TAtom, TWrap> Fold(SubstitutionWrap<TValue, TAtom, TWrap> wrap)
        {
            if (IsZero(wrap) || IsOne(wrap))
                return wrap;
            SubstitutionWrap<TValue, TAtom, TWrap> result;
            if (!FoldCache.TryGetValue(wrap, out result))
            {
                result = new SubstitutionWrap<TValue, TAtom, TWrap>.Fold { Child = wrap };
                FoldCache.Add(wrap, result);
            }
            return result;
        }

        public override SubstitutionWrap<TValue, TAtom, TWrap> Multiply(TAtom left, SubstitutionWrap<TValue, TAtom, TWrap> right)
        {
            SubstitutionWrap<TValue, TAtom, TWrap> result;
            if (!ByAtomCache.TryGetValue((left, right), out result))
            {
                result = (IsZero(right) || AtomEngine.IsOne(left)) ? right : new SubstitutionWrap<TValue, TAtom, TWrap>.ByAtom { Left = left, Right = right };
                ByAtomCache.Add((left, right), result);
            }
            return result;
        }

        public override SubstitutionWrap<TValue, TAtom, TWrap> Multiply(IEnumerable<SubstitutionWrap<TValue, TAtom, TWrap>> values)
        {
            var children = new List<SubstitutionWrap<TValue, TAtom, TWrap>>();
            foreach (var value in values)
            {
                if (IsZero(value))
                    return value;
                if (IsOne(value))
                    continue;
                children.Add(value);
            }
            if (children.Count == 1)
                return children[0];
            var key = IsCommutative ? children.OrderBy(_ => _.GetHashCode()).AsReadOnlyCollection() : children;
            SubstitutionWrap<TValue, TAtom, TWrap> result;
            if (!MultiplyCache.TryGetValue(key, out result))
            {
                result = new SubstitutionWrap<TValue, TAtom, TWrap>.Multiply { Children = key };
                MultiplyCache.Add(key, result);
            }
            return result;
        }

        public IReadOnlyList<SubstitutionWrap<TValue, TAtom, TWrap>> Substitutions(Int32 dependencyDepth, Int32 count)
            => new SubstitutionList(dependencyDepth, count);
    }

    public class SubstitutionWrapEngineFactory : IWrapEngineFactory
    {
        private readonly IWrapEngineFactory Inner;

        public SubstitutionWrapEngineFactory(IWrapEngineFactory inner)
        {
            Inner = inner;
        }

        public IWrapEngine<TValue> Create<TValue>(ISemiring<TValue> semiring)
            => Inner
                .Create(semiring)
                .Accept(new CreateRequest<TValue>());

        private class CreateRequest<TValue> : IWrapEngineVisitor<TValue, IWrapEngine<TValue>>
        {
            public IWrapEngine<TValue> Visit<TAtom, TWrap>(IWrapEngine<TValue, TAtom, TWrap> engine)
                => new SubstitutionWrapEngine<TValue, TAtom, TWrap>(engine);
        }
    }

    public class SubstitutionRelationEngine<TValue, TAtom, TWrap, TShape, TInner> : StackedRelationEngine<TValue, TAtom, SubstitutionWrap<TValue, TAtom, TWrap>, TShape, TInner>
    {
        private new readonly IRelationEngine<TValue, TAtom, SubstitutionWrap<TValue, TAtom, TWrap>, TShape, TInner> InnerEngine;
        private new readonly SubstitutionWrapEngine<TValue, TAtom, TWrap> WrapEngine;
        private readonly Dictionary<TInner, (TShape, IReadOnlyList<SubstitutionWrap<TValue, TAtom, TWrap>>)> DecomposeCache;
        private readonly Dictionary<(TShape, Int32, Int32), TInner> SubstituteCache;
        private readonly IEqualityComparer<(SubstitutionWrap<TValue, TAtom, TWrap>, Stack)> ResolveComparer;
        private readonly IEqualityComparer<DictionaryKey<Stack>> StackKeyComparer;

        public SubstitutionRelationEngine(IRelationEngine<TValue, TAtom, SubstitutionWrap<TValue, TAtom, TWrap>, TShape, TInner> innerEngine)
            : base(innerEngine)
        {
            InnerEngine = innerEngine;
            WrapEngine = (SubstitutionWrapEngine<TValue, TAtom, TWrap>)InnerEngine.WrapEngine;
            DecomposeCache = new Dictionary<TInner, (TShape, IReadOnlyList<SubstitutionWrap<TValue, TAtom, TWrap>>)>(InnerEngine.Comparer);
            SubstituteCache = new Dictionary<(TShape, Int32, Int32), TInner>(ValueTupleEC(ShapeComparer, OperatorEC<Int32>(), OperatorEC<Int32>()));
            ResolveComparer = ValueTupleEC(WrapEngine.Comparer, StackComparer);
            StackKeyComparer = DictionaryKey.Comparer(StackComparer);
        }

        public override IEqualityComparer<TShape> ShapeComparer
            => InnerEngine.ShapeComparer;

        public override (TShape, IReadOnlyList<SubstitutionWrap<TValue, TAtom, TWrap>>) Decompose(TInner inner)
        {
            (TShape, IReadOnlyList<SubstitutionWrap<TValue, TAtom, TWrap>>) result;
            if (!DecomposeCache.TryGetValue(inner, out result))
            {
                result = InnerEngine.Decompose(inner);
                DecomposeCache.Add(inner, result);
            }
            return result;
        }

        private readonly Counter ResolveRoots = new Counter();
        private readonly Counter FindValueCalls = new Counter();
        private readonly Counter FindValueFolds = new Counter();
        private readonly Counter FindValueSubstitutions = new Counter();

        private TWrap Resolve(SubstitutionWrap<TValue, TAtom, TWrap> wrap, Stack context)
        {
            var engine = WrapEngine.InnerEngine;
            var seen = new HashSet<(SubstitutionWrap<TValue, TAtom, TWrap>, Stack)>(ResolveComparer);
            var queue = new Queue<(SubstitutionWrap<TValue, TAtom, TWrap>, Stack)>();
            var needed = new Dictionary<DictionaryKey<Stack>, List<SubstitutionWrap<TValue, TAtom, TWrap>>>(StackKeyComparer);
            if (seen.Add((wrap, context)))
                queue.Enqueue((wrap, context));
            while (queue.Count > 0)
            {
                var (expression, origin) = queue.Dequeue();
                needed.GetOrCreate(origin).Add(expression);
                ResolveRoots.Increment();
                foreach (var substitute in expression.Substitutes)
                {
                    var source = origin;
                    for (var i = 0; i < substitute.DependencyDepth; i++)
                        source = source.Parent;
                    if (seen.Add((source.Content[substitute.ContentIndex], source.Origin)))
                        queue.Enqueue((source.Content[substitute.ContentIndex], source.Origin));
                }
            }
            var values = new Dictionary<(SubstitutionWrap<TValue, TAtom, TWrap>, Stack), TWrap>(ResolveComparer);
            foreach ((Stack origin, var expressions) in needed.OrderBy(_ => ((Stack)_.Key)?.Id ?? 0))
            {
                var locals = new Dictionary<SubstitutionWrap<TValue, TAtom, TWrap>, TWrap>(16, WrapEngine.Comparer);
                TWrap FindValue(SubstitutionWrap<TValue, TAtom, TWrap> expression)
                {
                    FindValueCalls.Increment();
                    TWrap local;
                    if (!locals.TryGetValue(expression, out local))
            {
                switch (expression)
                {
                    case SubstitutionWrap<TValue, TAtom, TWrap>.Add add:
                                local = engine.Add(add.Children.Select(FindValue));
                                break;
                    case SubstitutionWrap<TValue, TAtom, TWrap>.ByAtom byAtom:
                                local = engine.Multiply(byAtom.Left, FindValue(byAtom.Right));
                                break;
                    case SubstitutionWrap<TValue, TAtom, TWrap>.Fold fold:
                                FindValueFolds.Increment();
                                local = engine.Fold(FindValue(fold.Child));
                                break;
                    case SubstitutionWrap<TValue, TAtom, TWrap>.Multiply multiply:
                                local = engine.Multiply(multiply.Children.Select(FindValue));
                                break;
                    case SubstitutionWrap<TValue, TAtom, TWrap>.Substitute substitute:
                                FindValueSubstitutions.Increment();
                        var source = origin;
                        for (var i = 0; i < substitute.DependencyDepth; i++)
                            source = source.Parent;
                                local = values[(source.Content[substitute.ContentIndex], source.Origin)];
                                break;
                    default:
                        throw new InvalidProgramException();
                }
                        locals.Add(expression, local);
                    }
                    return local;
                }
                foreach (var expression in expressions)
                    values[(expression, origin)] = FindValue(expression);
            }
            return values[(wrap, context)];
        }

        public override SubstitutionWrap<TValue, TAtom, TWrap> Epsilon(Stack relation)
            => new SubstitutionWrap<TValue, TAtom, TWrap>.Inner { Value = Resolve(InnerEngine.Epsilon(relation.Inner), relation.Origin) };

        public override TInner Substitute(TShape shape, IReadOnlyList<SubstitutionWrap<TValue, TAtom, TWrap>> content, Int32 dependencyDepth)
        {
            TInner result;
            var key = (shape, dependencyDepth, content.Count);
            if (!SubstituteCache.TryGetValue(key, out result))
            {
                result = InnerEngine.Compose(shape, WrapEngine.Substitutions(dependencyDepth, content.Count));
                SubstituteCache.Add(key, result);
            }
            return result;
        }
    }

    public class SubstitutionRelationEngineFactory : RelationEngineFactory
    {
        private readonly IRelationEngineFactory Inner;

        public SubstitutionRelationEngineFactory(IRelationEngineFactory inner)
            : base(new SubstitutionWrapEngineFactory(inner.WrapEngineFactory))
        {
            Inner = inner;
        }

        public override IRelationEngine<TValue> Create<TValue>(IValuationSemiring<TValue> semiring, IWrapEngine<TValue> wrapEngine)
            => Inner
                .Create(semiring, wrapEngine)
                .Accept(new CreateRequest<TValue>());

        private class CreateRequest<TValue> : IRelationEngineVisitor<TValue, IRelationEngine<TValue>>
        {
            public IRelationEngine<TValue> Visit<TAtom, TWrap, TShape, TRelation>(IRelationEngine<TValue, TAtom, TWrap, TShape, TRelation> engine)
            {
                System.Diagnostics.Debug.Assert(typeof(TWrap).GetGenericTypeDefinition() == typeof(SubstitutionWrap<,,>));
                System.Diagnostics.Debug.Assert(typeof(TWrap).GenericTypeArguments[0] == typeof(TValue));
                System.Diagnostics.Debug.Assert(typeof(TWrap).GenericTypeArguments[1] == typeof(TAtom));
                var @class = typeof(SubstitutionRelationEngine<,,,,>).MakeGenericType(typeof(TValue), typeof(TAtom), typeof(TWrap).GenericTypeArguments[2], typeof(TShape), typeof(TRelation));
                return (IRelationEngine<TValue>)Activator.CreateInstance(@class, engine);
            }
        }
    }
}
