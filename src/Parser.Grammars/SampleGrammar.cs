﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Parser.Grammars
{
    public class SampleGrammar : SimpleGrammar
    {
        public String Name { get; }
        public List<(String, Int64?)> Inputs { get; }

        private SampleGrammar(String name)
        {
            Name = name;
            Inputs = new List<(String, Int64?)>();
        }

        public void Add(String input, Int64? result) =>
            Inputs.Add((input, result));

        public override String ToString() =>
            Name;

        public static SampleGrammar Trivial { get; } =
            new SampleGrammar(nameof(Trivial))
            {
                R(0),
                { "", 1 },
                { "a", 0 },
                { String.Join("", Enumerable.Repeat('a', 1024)), 0 },
            };

        public static SampleGrammar FlatIteration { get; } =
            new SampleGrammar(nameof(FlatIteration))
            {
                R(0), S(0,'a',0),
                { "", 1 },
                { "a", 1 },
                { "aaaaaaaaaa", 1 },
                { String.Join("", Enumerable.Repeat('a', 1024)), 1 },
            };

        public static SampleGrammar LeftRecursion { get; } =
            new SampleGrammar(nameof(LeftRecursion))
            {
                C(0,0,1), R(0), S(1,'a',2), R(2),
                { "", 1 },
                { "a", 1 },
                { "aaaaaaaaaa", 1 },
                { String.Join("", Enumerable.Repeat('a', 1024)), 1 },
            };

        public static SampleGrammar RightRecursion { get; } =
            new SampleGrammar(nameof(RightRecursion))
            {
                S(0,'a',1), R(0), C(1,0,2), R(2),
                { "", 1 },
                { "a", 1 },
                { "aaaaaaaaaa", 1 },
                { String.Join("", Enumerable.Repeat('a', 1024)), 1 },
            };

        public static SampleGrammar Parentheses { get; } =
            new SampleGrammar(nameof(Parentheses))
            {
                R(0), S(0,'(',1), C(1,0,2), S(2,')',3), C(3,0,4), R(4),
                { "", 1 },
                { "()", 1 },
                { "(()(()))()", 1 },
                { "(()()))(()", 0 },
                { ParenthesesInput(1024), 1 },
            };

        private static String ParenthesesInput(Int64 length) =>
            (length < 2) ? "" : "(" + ParenthesesInput(length - (Int64)Math.Sqrt(length)) + ")" + ParenthesesInput((Int64)Math.Sqrt(length) - 2);

        public static SampleGrammar HighlyAmbiguous { get; } =
            new SampleGrammar(nameof(HighlyAmbiguous))
            {
                S(0,'a',1), R(1), C(0,0,2), C(2,0,3), R(3), C(3,0,4), R(4),
                { "", 0 },
                { "a", 1 },
                { "aa", 1 },
                { "aaa", 3 },
                { "aaaa", 10 },
                { String.Join("", Enumerable.Repeat('a', 64)), null },
            };
    }
}
