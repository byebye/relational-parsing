using System.Linq;
using Antlr4.Runtime;
using Antlr4.Runtime.Tree;

namespace Parser.Benchmarks
{
    public class Antlr4Interpreter : Antlr4ParserBase<ParserInterpreter>
    {
        private static ITree InvokeParse(ParserInterpreter parser, ITokenStream stream)
        {
            parser.SetInputStream(stream);
            return parser.Parse(0);
        }

        public Antlr4Interpreter(Antlr4Grammar grammar)
            : base(grammar.CreateParser(null), InvokeParse)
        {
        }
    }
}
