using System;
using System.Collections.Generic;
using No.Comparers;

namespace Parser
{
    using static EqualityComparerFactories;

    public abstract class Transition
    {
        private Transition()
        {
        }

        public sealed class Call : Transition
        {
            public IState Source { get; }
            public IState Child { get; }
            public IState Next { get; }

            public Call(IState source, IState child, IState next)
            {
                Source = source;
                Child = child;
                Next = next;
            }

            public override String ToString() =>
                $"call<{Source}, {Child}, {Next}>";
        }

        public sealed class Shift : Transition
        {
            public IState Source { get; }
            public Char Terminal { get; }
            public IState Next { get; }

            public Shift(IState source, Char terminal, IState next)
            {
                Source = source;
                Terminal = terminal;
                Next = next;
            }

            public override String ToString() =>
                $"shift<{Source}, {Terminal}, {Next}>";
        }

        public sealed class Reduce : Transition
        {
            public IState Source { get; }
            public IState Label { get; }

            public Reduce(IState source)
            {
                Source = source;
            }

            public Reduce(IState source, IState label)
            {
                Source = source;
                Label = label;
            }

            public override String ToString() =>
                $"reduce<{Source}>";
        }

        public static IEqualityComparer<Transition> Comparer { get; }
            = new VariantsEqualityComparer<Transition>(_ => _.GetType())
            {
                new ComponentsEqualityComparer<Call>
                {
                    { _ => _.Source, State.Comparer },
                    { _ => _.Child, State.Comparer },
                    { _ => _.Next, State.Comparer },
                },
                new ComponentsEqualityComparer<Shift>
                {
                    { _ => _.Source, State.Comparer },
                    { _ => _.Terminal, OperatorEC<Char>() },
                    { _ => _.Next, State.Comparer },
                },
                new ComponentsEqualityComparer<Reduce>
                {
                    { _ => _.Source, State.Comparer },
                },
            };
    }
}
