﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Parser.Common
{
    internal class GSSLayer<TLabel> : IEnumerable<GSSNode<TLabel>>
        where TLabel : IEquatable<TLabel>
    {
        private Int32 Index { get; }
        private Dictionary<TLabel, GSSNode<TLabel>> Nodes { get; }

        public GSSLayer(Int32 index)
        {
            Index = index;
            Nodes = new Dictionary<TLabel, GSSNode<TLabel>>();
        }

        public GSSNode<TLabel> AddNode(TLabel label, ref Statistics statistics)
        {
            GSSNode<TLabel> node;
            if (!Nodes.TryGetValue(label, out node))
            {
                statistics.NewNodes += 1;
                node = new GSSNode<TLabel>(label, Index);
                Nodes.Add(label, node);
            }
            return node;
        }

        IEnumerator<GSSNode<TLabel>> IEnumerable<GSSNode<TLabel>>.GetEnumerator() =>
            Nodes.Values.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() =>
            Nodes.Values.GetEnumerator();
    }
}
