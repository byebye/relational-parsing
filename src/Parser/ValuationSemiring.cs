namespace Parser
{
    using Algebra;

    public interface IValuationSemiring<TValue> : ISemiring<TValue>
    {
        TValue Atomic(Transition transition);
    }

    public abstract class ValuationSemiring<TValue> : Semiring<TValue>, IValuationSemiring<TValue>
    {
        public abstract TValue Atomic(Transition transition);
    }
}
