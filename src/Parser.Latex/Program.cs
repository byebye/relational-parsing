﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Parser.Latex
{
    using Common;
    using GLL;
    using GLR;
    using Grammars;
    using Graphs;
    using Relational;
    using Utilities;
    using Valuations;
    using static Grammars.SimpleGrammar;

    public static class Program
    {
        private static void PlotCumulative(PlotDrawing plot, IEnumerable<Int32> increments, String style = "")
        {
            var totals = new List<(Int32, Int32)>();
            var total = 0;
            foreach (var increment in increments)
            {
                total += increment;
                totals.Add((totals.Count, total));
            }
            plot.PlotData(totals, style);
        }

        private static IEnumerable<ParserGenerator> Generators =
            new ParserGenerator[]
            {
                new RelationalParserGenerator(new DagRelationEngineFactory(new CommutativeWrapEngineFactory(new DagWrapEngineFactory(new DagAtomEngineFactory(new DictionaryGraphEngineFactory()), new DictionaryGraphEngineFactory())), new DictionaryGraphEngineFactory()), new AtomicAnalyzer(), true),
                new GLLParserGenerator(false),
                new GLLParserGenerator(true),
                new GLRParserGenerator(false),
            };

        private static IGrammar WideGrammar(Int32 width)
        {
            var grammar = new SimpleGrammar { R(0), C(0,0,1) };
            for (Int32 i = 0; i < width; i++)
                grammar.Add(C(1,3*i+3,3*i+2), R(3*i+2), S(3*i+3,(Char)('a'+i),3*i+4), R(3*i+4));
            return grammar;
        }

        private static IGrammar DeepGrammar(Int32 depth)
        {
            var grammar = new SimpleGrammar { R(0), S(depth,'a',0), R(depth) };
            for (Int32 i = 0; i < depth; i++)
                grammar.Add(C(i,i+1,i+1));
            return grammar;
        }

        private static IGrammar NullGrammar(Int32 depth)
        {
            var grammar = new SimpleGrammar { R(0), C(0,0,1), S(depth,'a',depth+1), R(depth+1), R(depth+2) };
            for (Int32 i = 1; i < depth; i++)
                grammar.Add(C(i,depth+2,i+1));
            return grammar;
        }

        private static IGrammar ExpGrammar(Int32 rank)
        {
            var grammar = new SimpleGrammar { R(0), C(0,6,1), C(1,0,2), R(2), C(1,0,3), R(3), C(0,8,4), C(4,6*rank-2,5), R(5), S(6,'a',7), R(7), S(8,'a',9), R(9) };
            for (Int32 i = 1; i < rank; i++)
            {
                var x = 6*i+4;
                grammar.Add(C(x+0,6,x+1), C(x+1,0,x+2), R(x+2), C(x+1,x+0,x+3), R(x+3), C(x+0,8,x+4), C(x+4,i==1 ? 0 : x-6,x+5), R(x+5));
            }
            return grammar;
        }

        private static IGrammar ArithmeticGrammar =>
            //new SimpleGrammar().C(0,4,1).R(1).S(1,'+',2).C(2,0,3).R(3).C(4,8,5).R(5).S(5,'*',6).C(6,4,7).R(7).S(8,'(',9).C(9,0,10).S(10,')',11).R(11).S(8,'x',12).R(12);
            new SimpleGrammar { C(0,0,1), S(1,'+',2), C(2,5,3), R(3), C(0,5,4), R(4), C(5,5,6), S(6,'*',7), C(7,10,8), R(8), C(5,10,9), R(9), S(10,'(',11), C(11,0,12), S(12,')',13), R(13), S(10,'x',14), R(14) };
        private static IEnumerable<Char> ArithmeticInput(Int32 depth)
        {
            if (depth==0)
            {
                yield return 'x';
                yield break;
            }
            yield return '(';
            foreach (var c in ArithmeticInput(depth - 1))
                yield return c;
            yield return '*';
            foreach (var c in ArithmeticInput(depth - 1))
                yield return c;
            yield return '+';
            foreach (var c in ArithmeticInput(depth - 1))
                yield return c;
            yield return '*';
            foreach (var c in ArithmeticInput(depth - 1))
                yield return c;
            yield return ')';
        }

        private static IEnumerable<(String, IGrammar, IEnumerable<Char>, Int32)> Cases()
        {
            if (true) yield return (
                "flat iteration",
                new SimpleGrammar{ R(0), S(0,'a',0) },
                Enumerable.Repeat('a', 130),
                5
            );

            if (true) yield return (
                "left recursion",
                new SimpleGrammar { C(0,0,1), R(0), S(1,'a',2), R(2) },
                Enumerable.Repeat('a', 130),
                5
            );

            if (true) yield return (
                "right recursion",
                new SimpleGrammar { S(0,'a',1), R(0), C(1,0,2), R(2) },
                Enumerable.Repeat('a', 130),
                5
            );

            if (true) yield return (
                "nondeterministic unambiguous",
                new SimpleGrammar { R(0), S(0,'a',1), S(0,'b',2), C(1,0,3), C(2,0,4), S(3,'a',5), S(4,'b',5), R(5) },
                Enumerable.Repeat('a', 130),
                5
            );

            if (true) yield return (
                "LR-regular but not LR(k) [Ex 1.2]",
                new SimpleGrammar {
                    S(0,'a',1), C(1,11,2), S(2,'b',3), R(3),
                    S(0,'b',4), C(4,11,5), S(5,'a',6), R(6),
                    C(1,17,7), S(7,'a',8), R(8),
                    C(4,17,9), S(9,'b',10), R(10),
                    S(11,'a',12), C(12,11,13), S(13,'a',14), S(14,'a',15), R(15),
                    S(11,'b',16), R(16),
                    S(17,'a',18), C(18,17,19), S(19,'a',20), R(20),
                    S(17,'b',21), R(21),
                },
                Enumerable.Repeat('a', 60).Concat("b").Concat(Enumerable.Repeat('a', 60)),
                5
            );

            if (true) yield return (
                "arithmetic",
                ArithmeticGrammar,
                ArithmeticInput(5),
                //"(x+x)",
                5
            );

            if (true) yield return (
                "ambiguous",
                new SimpleGrammar { S(0,'a',1), R(1), C(0,0,2), C(2,0,3), R(3), C(3,0,4), R(4) },
                Enumerable.Repeat('a', 130),
                6
            );

            if (true) yield return (
                "exp(3)",
                ExpGrammar(3),
                Enumerable.Repeat('a', 130),
                5
            );

            if (true) yield return (
                "wide(10)",
                WideGrammar(10),
                Enumerable.Repeat('a', 1030),
                5
            );

            if (true) yield return (
                "deep(2)",
                DeepGrammar(2),
                Enumerable.Repeat('a', 70),
                5
            );

            if (true) yield return (
                "nullable(4)",
                NullGrammar(4),
                "aaa",
                4
            );
        }

        public static void Main(String[] args)
        {
            Action<Boolean> ignore = _ => {};
            using (var document = new LatexDocument(Console.Out))
            {
                foreach (var (caseName, grammar, input, prefix) in Cases())
                {
                    Console.Error.WriteLine($"Case: {caseName}");
                    document.WriteLine($"\\section{{{caseName}}}");
                    var allStatistics = new List<(String, List<Statistics>)>();
                    foreach (var generator in Generators)
                    {
                        document.WriteLine($"\\subsection*{{{generator}}}");
                        document.WriteLine("\\begin{enumerate}[start=0]");
                        var statistics = new List<Statistics>();
                        Console.Error.WriteLine($"Running {generator}...");
                        var semiring = new ReportingStructuralSemiring(s => document.WriteLine("term " + s + "\\\\"));
                        var run = false && generator.Features.HasFlag(ParserFeatures.ArbitrarySemiring) ?
                            generator.CreateParser(grammar, semiring).Run(input, p => Debug.Assert(!semiring.IsZero(p))) :
                            generator.CreateRecognizer(grammar).Run(input, p => Debug.Assert(p));
                        foreach (var stat in run)
                        {
                            statistics.Add(stat);
                            if (statistics.Count > prefix)
                                continue;
                            document.WriteLine("\\item\\ \\\\");
                            if (stat.Graph != null)
                                stat.Graph.ToDrawable().Draw(document);
                        }
                        document.WriteLine("\\end{enumerate}");
                        allStatistics.Add((generator.ToString(), statistics));
                    }

                    document.WriteLine("\\subsection*{GSS statistics}");
                    var legend = $"legend pos=north west, legend entries={{{String.Join(',', from p in allStatistics select p.Item1)}}},";
                    using (var plot = new PlotDrawing(document, "loglog", "title={nodes created},"+legend))
                        foreach (var (_, statistics) in allStatistics)
                            PlotCumulative(plot, from s in statistics select s.NewNodes, "no markers");
                    using (var plot = new PlotDrawing(document, "loglog", "title={edges created},"+legend))
                        foreach (var (_, statistics) in allStatistics)
                            PlotCumulative(plot, from s in statistics select s.NewEdges, "no markers");
                    using (var plot = new PlotDrawing(document, "loglog", "title={edges traversed},"+legend))
                        foreach (var (_, statistics) in allStatistics)
                            PlotCumulative(plot, from s in statistics select s.VisitedEdges, "no markers");
                }
                if (false)
                {
                    var allStatistics = new Dictionary<String, List<(Int32, Int32)>>();
                    for (Int32 rank = 2; rank <= 12; rank += 2)
                    {
                        Console.Error.WriteLine($"rank = {rank}");
                        var grammar = ExpGrammar(rank);
                        foreach (var generator in Generators)
                        {
                            var total = generator.CreateRecognizer(grammar).Run(Enumerable.Repeat('a', 30), ignore).Sum(s => s.NewNodes);
                            allStatistics.GetOrCreate(generator.ToString()).Add((rank, total));
                        }
                    }
                    document.WriteLine("\\section{Exp (growth factor)}");
                    var legend = $"legend pos=north west, legend entries={{{String.Join(',', from p in allStatistics select p.Key)}}},";
                    using (var plot = new PlotDrawing(document, "loglog", "title={nodes created},"+legend))
                        foreach (var (_, statistics) in allStatistics)
                            plot.PlotData(statistics, "no markers");
                    using (var plot = new PlotDrawing(document, "semilogy", "title={nodes created},"+legend))
                        foreach (var (_, statistics) in allStatistics)
                            plot.PlotData(statistics, "no markers");
                }
                if (false)
                {
                    var nodeStatistics = new Dictionary<String, List<(Int32, Int32)>>();
                    var edgeStatistics = new Dictionary<String, List<(Int32, Int32)>>();
                    var timeStatistics = new Dictionary<String, List<(Int32, Int32)>>();
                    for (Int32 width = 2; width <= 64; width *= 2)
                    {
                        Console.Error.WriteLine($"width = {width}");
                        var grammar = WideGrammar(width);
                        foreach (var generator in Generators)
                        {
                            var stats = generator.CreateRecognizer(grammar).Run("aaa", ignore).ToList();
                            nodeStatistics.GetOrCreate(generator.ToString()).Add((width, stats.Sum(s => s.NewNodes)));
                            edgeStatistics.GetOrCreate(generator.ToString()).Add((width, stats.Sum(s => s.NewEdges)));
                            timeStatistics.GetOrCreate(generator.ToString()).Add((width, stats.Sum(s => s.VisitedEdges)));
                        }
                    }
                    document.WriteLine("\\section{Wide (growth factor)}");
                    var legend = $"legend pos=north west, legend entries={{{String.Join(',', from p in edgeStatistics select p.Key)}}},";
                    using (var plot = new PlotDrawing(document, "loglog", "title={nodes created},"+legend))
                        foreach (var (_, statistics) in nodeStatistics)
                            plot.PlotData(statistics, "no markers");
                    using (var plot = new PlotDrawing(document, "loglog", "title={edges created},"+legend))
                        foreach (var (_, statistics) in edgeStatistics)
                            plot.PlotData(statistics, "no markers");
                    using (var plot = new PlotDrawing(document, "loglog", "title={edges traversed},"+legend))
                        foreach (var (_, statistics) in timeStatistics)
                            plot.PlotData(statistics, "no markers");
                }
                if (false)
                {
                    var nodeStatistics = new Dictionary<String, List<(Int32, Int32)>>();
                    var timeStatistics = new Dictionary<String, List<(Int32, Int32)>>();
                    for (Int32 depth = 2; depth <= 8; depth *= 2)
                    {
                        Console.Error.WriteLine($"depth = {depth}");
                        var grammar = DeepGrammar(depth);
                        foreach (var generator in Generators)
                        {
                            var stats = generator.CreateRecognizer(grammar).Run("aaa", ignore).ToList();
                            nodeStatistics.GetOrCreate(generator.ToString()).Add((depth, stats.Sum(s => s.NewNodes)));
                            timeStatistics.GetOrCreate(generator.ToString()).Add((depth, stats.Sum(s => s.VisitedEdges)));
                        }
                    }
                    document.WriteLine("\\section{Deep (growth factor)}");
                    var legend = $"legend pos=north west, legend entries={{{String.Join(',', from p in nodeStatistics select p.Key)}}},";
                    using (var plot = new PlotDrawing(document, "loglog", "title={nodes created},"+legend))
                        foreach (var (_, statistics) in nodeStatistics)
                            plot.PlotData(statistics, "no markers");
                    using (var plot = new PlotDrawing(document, "loglog", "title={edges traversed},"+legend))
                        foreach (var (_, statistics) in timeStatistics)
                            plot.PlotData(statistics, "no markers");
                }
                if (false)
                {
                    var nodeStatistics = new Dictionary<String, List<(Int32, Int32)>>();
                    var timeStatistics = new Dictionary<String, List<(Int32, Int32)>>();
                    for (Int32 depth = 2; depth <= 128; depth *= 2)
                    {
                        Console.Error.WriteLine($"depth = {depth}");
                        var grammar = NullGrammar(depth);
                        foreach (var generator in Generators)
                        {
                            var stats = generator.CreateRecognizer(grammar).Run("aaa", ignore).ToList();
                            nodeStatistics.GetOrCreate(generator.ToString()).Add((depth, stats.Sum(s => s.NewNodes)));
                            timeStatistics.GetOrCreate(generator.ToString()).Add((depth, stats.Sum(s => s.VisitedEdges)));
                        }
                    }
                    document.WriteLine("\\section{Nullable (growth factor)}");
                    var legend = $"legend pos=north west, legend entries={{{String.Join(',', from p in nodeStatistics select p.Key)}}},";
                    using (var plot = new PlotDrawing(document, "loglog", "title={nodes created},"+legend))
                        foreach (var (_, statistics) in nodeStatistics)
                            plot.PlotData(statistics, "no markers");
                    using (var plot = new PlotDrawing(document, "loglog", "title={edges traversed},"+legend))
                        foreach (var (_, statistics) in timeStatistics)
                            plot.PlotData(statistics, "no markers");
                }
                if (false)
                {
                    var nodeStatistics = new Dictionary<String, List<(Int32, Int32)>>();
                    var edgeStatistics = new Dictionary<String, List<(Int32, Int32)>>();
                    var timeStatistics = new Dictionary<String, List<(Int32, Int32)>>();
                    for (Int32 rank = 0; rank <= 5; rank += 1)
                    {
                        Console.Error.WriteLine($"rank = {rank}");
                        foreach (var generator in Generators)
                        {
                            var stats = generator.CreateRecognizer(ArithmeticGrammar).Run(ArithmeticInput(rank), ignore).ToList();
                            var length = ArithmeticInput(rank).Count();
                            nodeStatistics.GetOrCreate(generator.ToString()).Add((length, stats.Sum(s => s.NewNodes)));
                            edgeStatistics.GetOrCreate(generator.ToString()).Add((length, stats.Sum(s => s.NewEdges)));
                            timeStatistics.GetOrCreate(generator.ToString()).Add((length, stats.Sum(s => s.VisitedEdges)));
                        }
                    }
                    document.WriteLine("\\section{Arithmetic}");
                    var legend = $"legend pos=north west, legend entries={{{String.Join(',', from p in edgeStatistics select p.Key)}}},";
                    using (var plot = new PlotDrawing(document, "loglog", "title={nodes created},"+legend))
                        foreach (var (_, statistics) in nodeStatistics)
                            plot.PlotData(statistics, "no markers");
                    using (var plot = new PlotDrawing(document, "loglog", "title={edges created},"+legend))
                        foreach (var (_, statistics) in edgeStatistics)
                            plot.PlotData(statistics, "no markers");
                    using (var plot = new PlotDrawing(document, "loglog", "title={edges traversed},"+legend))
                        foreach (var (_, statistics) in timeStatistics)
                            plot.PlotData(statistics, "no markers");
                }
            }
        }
    }
}
