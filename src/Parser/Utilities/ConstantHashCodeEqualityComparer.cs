﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using No.Comparers;

namespace Parser.Utilities
{
    public class ConstantHashCodeEqualityComparer<T> : InliningEqualityComparer<T>, IEqualityComparer<T>
    {
        private readonly IEqualityComparer<T> Inner;
        private readonly Int32 HashCode;

        public ConstantHashCodeEqualityComparer(IEqualityComparer<T> inner, Int32 hashCode = 0)
        {
            Inner = inner;
            HashCode = hashCode;
        }

        Boolean IEqualityComparer<T>.Equals(T x, T y)
            => Inner.Equals(x, y);

        Int32 IEqualityComparer<T>.GetHashCode(T obj)
            => HashCode;

        public override Expression EqualsExpression(Expression x, Expression y)
            => Inner.EqualsExpression(x, y);

        public override Expression GetHashCodeExpression(Expression obj)
            => Expression.Constant(HashCode);
    }
}
