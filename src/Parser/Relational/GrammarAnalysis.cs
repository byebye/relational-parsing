using System;
using System.Collections.Generic;
using System.Linq;

namespace Parser.Relational
{
    using Algebra;
    using Utilities;

    public sealed class GrammarAnalysis<TValue> : GrammarAnalysis
    {
        public class EdgeRelation
        {
            public Boolean HasBase { get; }
            public TValue Base { get; }
            public List<(TValue Prefix, EdgeRelation Inner, TValue Suffix)> Recursive { get; }
                = new List<(TValue Prefix, EdgeRelation Inner, TValue Suffix)>();

            public EdgeRelation()
            {
            }

            public EdgeRelation(TValue @base)
            {
                HasBase = true;
                Base = @base;
            }
        }

        public IValuationSemiring<TValue> Semiring { get; }
        public IReadOnlyDictionary<Char, HashSet<IState>> ShiftOrigins { get; }
        public IReadOnlyCollection<IState> ProductiveStates { get; }
        public IReadOnlyDictionary<IState, TValue> NullingTransitions { get; }
        private IReadOnlyDictionary<IState, IReadOnlyCollection<(IState, TValue)>> FlatTransitions { get; }
        private IReadOnlyDictionary<IState, List<(IState, IState, TValue)>> MoveTransitions { get; }
        public IReadOnlyDictionary<(IState, IState), Dictionary<IState, EdgeRelation>> EdgeRelations { get; }
        public IReadOnlyDictionary<IState, Dictionary<IState, EdgeRelation>> NodeRelations { get; }

        public Boolean OptimizeProductiveStates { get; set; } = true;

        public GrammarAnalysis(IGrammar grammar, IValuationSemiring<TValue> semiring, Boolean debug = false)
            : base(grammar, debug)
        {
            Semiring = semiring;
            ProductiveStates = OptimizeProductiveStates ? ComputeProductiveStates() : AllStates;
            ShiftOrigins = ComputeShiftOrigins();
            NullingTransitions = ComputeNullingTransitions();
            FlatTransitions = ComputeFlatTransitions();
            MoveTransitions = ComputeMoveTransitions();
            EdgeRelations = ComputeEdgeRelations();
            NodeRelations = ComputeNodeRelations();
        }

        private IReadOnlyCollection<IState> ComputeProductiveStates()
        {
            var result = new HashSet<IState> { Grammar.StopState };
            var implied = new Dictionary<IState, HashSet<IState>>();
            foreach (var shift in ShiftTransitions)
                result.Add(shift.Source);
            foreach (var call in CallTransitions)
            {
                implied.GetOrCreate(call.Child).Add(call.Source);
                implied.GetOrCreate(call.Next).Add(call.Source);
            }
            Debug.WriteLine($"Productive states: {CloseUnder(result, implied)}, {{{String.Join(", ", result)}}}");
            return result;
        }

        private IReadOnlyDictionary<Char, HashSet<IState>> ComputeShiftOrigins()
        {
            var origins = new Dictionary<Char, HashSet<IState>>();
            foreach (var state in AllStates)
                foreach (var transition in state.Transitions)
                    if (transition is Transition.Shift shift)
                        origins.GetOrCreate(shift.Terminal).Add(state);
            Debug.WriteLine($"Shift origins: {origins.Values.Sum(_ => _.Count)} in {origins.Count} groups");
            return origins;
        }

        private IReadOnlyDictionary<IState, TValue> ComputeNullingTransitions()
        {
            var nullingTransitions = new Dictionary<IState, TValue>();
            var inProgress = new HashSet<IState>();
            TValue Dfs(IState state)
            {
                TValue result;
                if (!nullingTransitions.TryGetValue(state, out result))
                {
                    if (!inProgress.Add(state))
                        throw new NotImplementedException("Support for infinitely ambiguous grammars has not yet been implemented");
                    var direct = Semiring.Add(
                        from reduce in state.Transitions.OfType<Transition.Reduce>()
                        select Semiring.Atomic(reduce));
                    var indirect = Semiring.Add(
                        from call in state.Transitions.OfType<Transition.Call>()
                        where IsNullable[call.Child] && IsNullable[call.Next]
                        select Semiring.Multiply(Dfs(call.Next), Dfs(call.Child), Semiring.Atomic(call)));
                    result = Semiring.Add(direct, indirect);
                    nullingTransitions.Add(state, result);
                }
                return result;
            }
            foreach (var state in AllStates)
                if (IsNullable[state])
                    Dfs(state);
            return nullingTransitions;
        }

        private IReadOnlyDictionary<IState, IReadOnlyCollection<(IState, TValue)>> ComputeFlatTransitions()
        {
            var flatTransitions = new Dictionary<IState, IReadOnlyCollection<(IState, TValue)>>();
            var totalCount = 0;
            IReadOnlyCollection<(IState, TValue)> Dfs(IState state)
            {
                if (flatTransitions.TryGetValue(state, out var done))
                    return done;
                var byTarget = new Dictionary<IState, List<TValue>> { { state, new List<TValue> { Semiring.One } } };
                foreach (var transition in state.Transitions)
                    if (transition is Transition.Call call && NullingTransitions.TryGetValue(call.Child, out var nulling))
                        foreach (var (target, value) in Dfs(call.Next))
                            byTarget.GetOrCreate(target).Add(Semiring.Multiply(value, nulling, Semiring.Atomic(call)));
                var result = byTarget.Select(kvp => (kvp.Key, Semiring.Add(kvp.Value))).ToList();
                flatTransitions.Add(state, result);
                totalCount += result.Count;
                return result;
            }
            foreach (var state in AllStates)
                Dfs(state);
            Debug.WriteLine($"Flat transitions: {totalCount}");
            return flatTransitions;
        }

        private IReadOnlyDictionary<IState, List<(IState, IState, TValue)>> ComputeMoveTransitions()
        {
            var byTriple = new Dictionary<(IState, IState, IState), List<TValue>>();
            foreach (var state in AllStates)
                foreach (var transition in state.Transitions)
                    if (transition is Transition.Call call)
                        foreach (var (target, value) in FlatTransitions[call.Child])
                            byTriple.GetOrCreate((call.Next, target, state)).Add(Semiring.Multiply(value, Semiring.Atomic(call)));
            var moveTransitions = new Dictionary<IState, List<(IState, IState, TValue)>>();
            foreach (var ((s, u, t), values) in byTriple)
                moveTransitions.GetOrCreate(u).Add((s, t, Semiring.Add(values)));
            Debug.WriteLine($"Move transitions: {byTriple.Count} in {moveTransitions.Count} groups");
            return moveTransitions;
        }

        private IReadOnlyDictionary<(IState, IState), Dictionary<IState, EdgeRelation>> ComputeEdgeRelations()
        {
            var result = new Dictionary<(IState, IState), Dictionary<IState, EdgeRelation>>();
            var queue = new Queue<(IState, IState, IState, EdgeRelation)>();
            foreach (var (u, bases) in MoveTransitions)
                foreach (var (s, t, @base) in bases)
                {
                    var final = new EdgeRelation(@base);
                    result.GetOrCreate((s, t)).Add(u, final);
                    queue.Enqueue((s, u, t, final));
                }
            while (queue.Count > 0)
            {
                var (s, u, t, current) = queue.Dequeue();
                if (NullingTransitions.TryGetValue(s, out var prefix) && MoveTransitions.TryGetValue(t, out var suffixes))
                    foreach (var (s1, t1, suffix) in suffixes)
                    {
                        var sub = result.GetOrCreate((s1, t1));
                        EdgeRelation next;
                        if (!sub.TryGetValue(u, out next))
                        {
                            next = new EdgeRelation();
                            sub.Add(u, next);
                            queue.Enqueue((s1, u, t1, next));
                        }
                        next.Recursive.Add((prefix, current, suffix));
                    }
            }
            Debug.WriteLine($"Edge relations: {result.Values.Sum(_ => _.Count)} in {result.Count} groups");
            return result;
        }

        private IReadOnlyDictionary<IState, Dictionary<IState, EdgeRelation>> ComputeNodeRelations()
        {
            var result = new Dictionary<IState, Dictionary<IState, EdgeRelation>>();
            var totalCount = 0;
            foreach (var s0 in AllStates)
            {
                var group = new Dictionary<IState, EdgeRelation>();
                result.Add(s0, group);
                foreach (var (t, @base) in FlatTransitions[s0])
                    group.Add(t, new EdgeRelation(@base));
                foreach (var (t, suffix) in FlatTransitions[s0])
                    foreach (var (s, prefix) in NullingTransitions)
                        if (EdgeRelations.TryGetValue((s, t), out var edges))
                            foreach (var (u, edge) in edges)
                            {
                                EdgeRelation next;
                                if (!group.TryGetValue(u, out next))
                                {
                                    next = new EdgeRelation();
                                    group.Add(u, next);
                                }
                                next.Recursive.Add((prefix, edge, suffix));
                            }
                totalCount += group.Count;
            }
            Debug.WriteLine($"Node relations: {totalCount}");
            return result;
        }
    }
}
