namespace Parser.Latex
{
    using System.Linq;
    using GLL;
    using Common;

    public class GLLDescriptorDrawing : IDrawable
    {
        private readonly WorkingSet<(IState, GSSNode<IState>)> Target;

        internal GLLDescriptorDrawing(WorkingSet<(IState, GSSNode<IState>)> target)
        {
            Target = target;
        }

        public void Draw(LatexDocument document)
        {
            var statistics = new Statistics();
            var index = Target.All.Max(p => p.Item2.LayerIndex) + 1;
            var layer = new GSSLayer<IState>(index);
            foreach (var (state, node) in Target.All)
                layer.AddNode(state, ref statistics).AddChild(node, ref statistics);
            new GSSLayerDrawing<IState>(layer).Draw(document);
        }

    }
}
