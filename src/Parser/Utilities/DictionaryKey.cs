using System.Collections.Generic;
using No.Comparers;

namespace Parser.Utilities
{
    public struct DictionaryKey<TKey>
        where TKey : class
    {
        public readonly TKey Key;

        public DictionaryKey(TKey key)
        {
            Key = key;
        }

        public static implicit operator DictionaryKey<TKey>(TKey key)
            => new DictionaryKey<TKey>(key);

        public static implicit operator TKey(DictionaryKey<TKey> key)
            => key.Key;
    }

    public static class DictionaryKey
    {
        public static IEqualityComparer<DictionaryKey<TKey>> Comparer<TKey>(IEqualityComparer<TKey> keyEC)
            where TKey : class
            => new ComponentsEqualityComparer<DictionaryKey<TKey>>
            {
                { _ => _.Key, keyEC },
            };
    }
}
