using System.Collections.Generic;

namespace Parser.Algebra
{
    public interface ISet<TValue>
    {
        IEqualityComparer<TValue> Comparer { get; }
    }

    public abstract class Set<TValue> : ISet<TValue>
    {
        public virtual IEqualityComparer<TValue> Comparer
            => EqualityComparer<TValue>.Default;
    }
}
