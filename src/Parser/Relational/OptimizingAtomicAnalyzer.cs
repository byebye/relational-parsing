using System;
using System.Collections.Generic;

namespace Parser.Relational
{
    public class OptimizingAtomicAnalyzer : AtomicAnalyzer
    {
        private readonly IReadOnlyCollection<AtomicOptimization> Optimizations;

        public OptimizingAtomicAnalyzer(params AtomicOptimization[] optimizations)
        {
            if (optimizations.Length == 0)
                throw new ArgumentException("Please specify at least one optimization", nameof(optimizations));
            Optimizations = optimizations;
        }

        public override IAtomicRelations<TAtom> ComputeRelations<TValue, TAtom>(GrammarAnalysis<TValue> analysis, IAtomEngine<TValue, TAtom> atomEngine)
        {
            var relations = base.ComputeRelations(analysis, atomEngine);
            IAtomicRelations<TAtom> previous = null;
            while (relations != previous)
            {
                previous = relations;
                foreach (var optimization in Optimizations)
                    relations = optimization.Perform(relations, atomEngine);
            }
            return relations;
        }

        public override String ToString()
            => $"{nameof(OptimizingAtomicAnalyzer)}({String.Join(",", Optimizations)})";
    }
}
