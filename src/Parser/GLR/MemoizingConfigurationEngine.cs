using System;
using System.Collections.Generic;

namespace Parser.GLR
{
    internal static class MemoizingConfigurationEngine
    {
        private class CreateVisitor : IConfigurationEngineVisitor<IConfigurationEngine>
        {
            public IConfigurationEngine Visit<TConfiguration>(IConfigurationEngine<TConfiguration> engine)
                where TConfiguration : IEquatable<TConfiguration> =>
                new MemoizingConfigurationEngine<TConfiguration>(engine);
        }

        public static IConfigurationEngine Create(IConfigurationEngine inner) =>
            inner.Accept(new CreateVisitor());
    }

    internal class MemoizingConfigurationEngine<TConfiguration> : IConfigurationEngine<TConfiguration>
        where TConfiguration : IEquatable<TConfiguration>
    {
        private readonly IConfigurationEngine<TConfiguration> Inner;
        private readonly Dictionary<(TConfiguration, IState), TConfiguration> CallMemo;
        private readonly Dictionary<(TConfiguration, Char?), HashSet<IState>> ReduceMemo;
        private readonly Dictionary<(TConfiguration, Char), TConfiguration> ShiftMemo;

        public MemoizingConfigurationEngine(IConfigurationEngine<TConfiguration> inner)
        {
            Inner = inner;
            CallMemo = new Dictionary<(TConfiguration, IState), TConfiguration>();
            ReduceMemo = new Dictionary<(TConfiguration, Char?), HashSet<IState>>();
            ShiftMemo = new Dictionary<(TConfiguration, Char), TConfiguration>();
        }

        public GrammarAnalysis Analysis => Inner.Analysis;

        public Boolean IsAlive(TConfiguration configuration) =>
            Inner.IsAlive(configuration);

        public TConfiguration Atomic(IState start) =>
            Inner.Atomic(start);

        public TConfiguration Call(TConfiguration configuration, IState child)
        {
            TConfiguration target;
            if (!CallMemo.TryGetValue((configuration, child), out target))
            {
                target = Inner.Call(configuration, child);
                CallMemo.Add((configuration, child), target);
            }
            return target;
        }

        public IEnumerable<IState> Reduce(TConfiguration configuration, Char? lookahead)
        {
            HashSet<IState> result;
            if (!ReduceMemo.TryGetValue((configuration, lookahead), out result))
            {
                result = new HashSet<IState>(Inner.Reduce(configuration, lookahead));
                ReduceMemo.Add((configuration, lookahead), result);
            }
            return result;
        }

        public TConfiguration Shift(TConfiguration configuration, char terminal)
        {
            TConfiguration target;
            if (!ShiftMemo.TryGetValue((configuration, terminal), out target))
            {
                target = Inner.Shift(configuration, terminal);
                ShiftMemo.Add((configuration, terminal), target);
            }
            return target;
        }

        public IEnumerable<TConfiguration> Successors(TConfiguration configuration) =>
            Inner.Successors(configuration);

        TResult IConfigurationEngine.Accept<TResult>(IConfigurationEngineVisitor<TResult> visitor) =>
            visitor.Visit(this);
    }
}
