using System;
using System.Collections.Generic;

namespace Parser.Latex
{
    public interface IDrawable
    {
         void Draw(LatexDocument document);
    }

    public class CompoundDrawable : IDrawable
    {
        private readonly IReadOnlyCollection<IDrawable> Components;

        public CompoundDrawable(params IDrawable[] components)
        {
            Components = components;
        }

        public void Draw(LatexDocument document)
        {
            document.Write("(");
            var comma = false;
            foreach (var component in Components)
            {
                if (comma)
                    document.Write(", ");
                component.Draw(document);
                comma = true;
            }
            document.Write(")");
        }
    }

    public class StringDrawable : IDrawable
    {
        private readonly String Value;

        public StringDrawable(String value)
        {
            Value = value;
        }

        public void Draw(LatexDocument document)
        {
            document.Write(Value);
        }
    }

    public static class Drawable
    {
        private static IDrawable ToDrawableAux(GLL.WorkingSet<(IState, Common.GSSNode<IState>)> thing) =>
            new GLLDescriptorDrawing(thing);

        private static IDrawable ToDrawableAux<T>(Common.GSSLayer<T> thing) where T : IEquatable<T> =>
            new GSSLayerDrawing<T>(thing);

        private static IDrawable ToDrawableAux<T1,T2>(Tuple<T1,T2> thing) =>
            new CompoundDrawable(ToDrawable(thing.Item1), ToDrawable(thing.Item2));

        private static IDrawable ToDrawableAux(Object thing) =>
            new StringDrawable(thing.ToString());
            //throw new ArgumentException($"I don't know how to draw an instance of {thing.GetType().Name}", nameof(thing));

        public static IDrawable ToDrawable<T>(this T thing) =>
            ToDrawableAux((dynamic) thing);
    }
}
