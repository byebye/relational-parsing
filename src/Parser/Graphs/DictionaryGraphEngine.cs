using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using No.Comparers;

namespace Parser.Graphs
{
    using Utilities;
    using static EqualityComparerFactories;

    public class DictionaryGraphEngine<TEdge> : GraphEngine<TEdge, DictionaryGraphEngine<TEdge>.Node>
    {
        [System.Diagnostics.DebuggerDisplay("{Count} neighbor(s)")]
        [System.Diagnostics.DebuggerTypeProxy(typeof(CollectionDisplay))]
        public class Node : Dictionary<(Node, Int32), TEdge>, CollectionDisplay.ITarget
        {
            public static new IEqualityComparer<(Node, Int32)> Comparer { get; }
                = ValueTupleEC(IdentityEC<Node>(), OperatorEC<Int32>());

            public Node()
                : base(Comparer)
            {
            }

            IEnumerable CollectionDisplay.ITarget.Items
                => this.Select(_ => (_.Value, _.Key.Item1));
        }

        public override IEqualityComparer<Node> Comparer { get; }
        private readonly IEqualityComparer<TEdge> EdgeEC;
        private readonly Func<TEdge, TEdge, TEdge> MergeEdges;
        private readonly Memoizer<Node> Memoizer;
        private readonly Counter NodesCreated = new Counter();
        private readonly Counter EdgesCreated = new Counter();
        private readonly Counter EdgesTraversed = new Counter();

        public DictionaryGraphEngine(IEqualityComparer<TEdge> edgeEC, Func<TEdge, TEdge, TEdge> mergeEdges)
        {
            var comparer = UniqueHashCodeEC(IdentityEC<Object>());
            Comparer = comparer;
            EdgeEC = edgeEC;
            MergeEdges = mergeEdges;
            Memoizer = new Memoizer<Node>(DictionaryEC(Node.Comparer, EdgeEC), _ => { NodesCreated.Increment(); EdgesCreated.Increment(_.Count); });
        }

        public override IEnumerable<(TEdge, Node)> Neighbors(Node node)
        {
            EdgesTraversed.Increment(node.Count);
            return from kvp in node select (kvp.Value, kvp.Key.Item1);
        }

        public override Node Create(IEnumerable<(TEdge, Node)> edges)
        {
            var result = new Node();
            foreach (var (edge, target) in edges)
            {
                var hash = EdgeEC.GetHashCode(edge);
                result[(target, hash)] = result.TryGetValue((target, hash), out var previous) ? MergeEdges(previous, edge) : edge;
            }
            return Memoizer.Add(result);
        }
    }

    public class DictionaryGraphEngineFactory : IGraphEngineFactory
    {
        public IGraphEngine<TEdge> Create<TEdge>(IEqualityComparer<TEdge> edgeEC, Func<TEdge, TEdge, TEdge> mergeEdges, Boolean mergeIsIdempotent)
            => new DictionaryGraphEngine<TEdge>(edgeEC, mergeEdges);

        public override String ToString()
            => $"{nameof(DictionaryGraphEngineFactory)}";
    }
}
