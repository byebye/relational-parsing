using System;
using System.IO;

namespace Parser.Latex
{
    public class LatexDocument : IDisposable
    {
        private readonly TextWriter Writer;

        public void Write(String text)
        {
            Writer.Write(text);
        }

        public void WriteLine(String line)
        {
            Writer.WriteLine(line);
        }

        public LatexDocument(TextWriter writer)
        {
            Writer = writer;
            WriteLine("\\documentclass[a4paper]{article}");
            WriteLine("\\usepackage{enumitem}");
            WriteLine("\\usepackage{pgfplots}");
            WriteLine("\\usepackage{tikz}");
            WriteLine("\\pgfplotsset{compat = 1.14}");
            WriteLine("\\begin{document}");
        }

        void IDisposable.Dispose()
        {
            WriteLine("\\end{document}");
        }
    }
}
