﻿using NUnit.Framework;

namespace Parser.Tests
{
    using Grammars;

    public class GrammarAnalysisTests
    {
        [Test]
        public void TestTrivial()
        {
            var grammar = SampleGrammar.Trivial;
            var analysis = new GrammarAnalysis(grammar);
            Assert.True(analysis.IsNullable[grammar[0]]);
        }
    }
}
