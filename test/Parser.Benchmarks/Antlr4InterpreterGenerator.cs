using System;

namespace Parser.Benchmarks
{
    public class Antlr4InterpreterGenerator : ParserGenerator
    {
        public override ParserFeatures Features
            => ParserFeatures.Recognition;

        public override IParser<Boolean> CreateRecognizer(IGrammar grammar)
        {
            if (grammar is Antlr4Grammar antlr)
                return new Antlr4Interpreter(antlr);
            throw new NotSupportedException($"{nameof(Antlr4InterpreterGenerator)} can only generate parsers for {nameof(Antlr4Grammar)}s");
        }
    }
}
