using System;
using System.Collections.Generic;
using System.Linq;

namespace Parser.Algebra
{
    public interface IZero<TValue> : ISet<TValue>
    {
        TValue Zero { get; }
        Boolean IsZero(TValue value);
    }

    public interface IAdditive<TValue> : IZero<TValue>
    {
        Boolean IsIdempotent { get; }
        TValue Add(TValue left, TValue right);
        TValue Add(IEnumerable<TValue> values);
    }

    public abstract class Additive<TValue> : Set<TValue>, IAdditive<TValue>
    {
        public virtual Boolean IsIdempotent
            => false;

        public virtual TValue Zero
            => Add(Enumerable.Empty<TValue>());

        public virtual Boolean IsZero(TValue value)
            => Comparer.Equals(value, Zero);

        public virtual TValue Add(TValue left, TValue right)
            => Add(new [] { left, right });

        public virtual TValue Add(IEnumerable<TValue> values)
            => values.Aggregate(Zero, Add);
    }

    public static class AdditiveExtensions
    {
        public static TValue Add<TValue>(this IAdditive<TValue> additive, params TValue[] values) =>
            additive.Add(values as IEnumerable<TValue>);
    }
}
