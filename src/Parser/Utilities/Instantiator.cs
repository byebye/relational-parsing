using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Parser.Utilities
{
    public class Instantiator : IEnumerable
    {
        private static readonly Dictionary<Type, Func<IEnumerable<Object>, Object>>  NewArrayCache
            = new Dictionary<Type, Func<IEnumerable<Object>, Object>>();

        private static Func<IEnumerable<Object>, Object> NewArray(Type elementType)
        {
            Func<IEnumerable<Object>, Object> result;
            if (!NewArrayCache.TryGetValue(elementType, out result))
            {
                var elements = Expression.Parameter(typeof(IEnumerable<Object>), "elements");
                var cast = Expression.Call(null, typeof(Enumerable).GetMethod(nameof(Enumerable.Cast)).MakeGenericMethod(elementType), elements);
                var call = Expression.Call(null, typeof(Enumerable).GetMethod(nameof(Enumerable.ToArray)).MakeGenericMethod(elementType), cast);
                var lambda = Expression.Lambda<Func<IEnumerable<Object>, Object>>(call, elements);
                result = lambda.Compile();
                NewArrayCache.Add(elementType, result);
            }
            return result;
        }

        private static readonly Dictionary<ConstructorInfo, Func<IEnumerable<Object>, Object>> NewObjectCache
            = new Dictionary<ConstructorInfo, Func<IEnumerable<Object>, Object>>();

        private static Func<IEnumerable<Object>, Object> NewObject(ConstructorInfo constructor)
        {
            Func<IEnumerable<Object>, Object> result;
            if (!NewObjectCache.TryGetValue(constructor, out result))
            {
                var arguments = Expression.Parameter(typeof(IEnumerable<Object>), "arguments");
                var enumerator = Expression.Variable(typeof(IEnumerator<Object>), "enumerator");
                var getEnumerator = Expression.Assign(enumerator, Expression.Call(arguments, typeof(IEnumerable<Object>).GetMethod(nameof(IEnumerable<Object>.GetEnumerator))));
                var moveNext = Expression.IfThen(Expression.Not(Expression.Call(enumerator, typeof(IEnumerator).GetMethod(nameof(IEnumerator.MoveNext)))), Expression.Throw(Expression.New(typeof(InvalidOperationException))));
                var current = Expression.PropertyOrField(enumerator, nameof(IEnumerator<Object>.Current));
                var instructions = new List<Expression>();
                var variables = new List<ParameterExpression>();
                foreach (var parameter in constructor.GetParameters())
                {
                    instructions.Add(moveNext);
                    var argument = Expression.Variable(parameter.ParameterType, parameter.Name);
                    variables.Add(argument);
                    instructions.Add(Expression.Assign(argument, Expression.Convert(current, parameter.ParameterType)));
                }
                instructions.Add(Expression.New(constructor, variables));
                var block = Expression.Block(constructor.DeclaringType, variables, instructions);
                var dispose = Expression.Call(enumerator, typeof(IDisposable).GetMethod(nameof(IDisposable.Dispose)));
                var complete = Expression.Block(typeof(Object), new [] { enumerator }, getEnumerator, Expression.TryFinally(block, dispose));
                var lambda = Expression.Lambda<Func<IEnumerable<Object>, Object>>(complete, arguments);
                result = lambda.Compile();
                NewObjectCache.Add(constructor, result);
            }
            return result;
        }

        private readonly Dictionary<Type, HashSet<Type>> ImplementingTypes;
        private readonly Dictionary<Type, Tree> Recipes;

        public Instantiator(Assembly root = null)
        {
            ImplementingTypes = new Dictionary<Type, HashSet<Type>>();
            Recipes = new Dictionary<Type, Tree>();
            var assemblies = new HashSet<Assembly>();
            Process(root ?? Assembly.GetEntryAssembly());
            void Process(Assembly assembly)
            {
                if (assemblies.Add(assembly))
                {
                    foreach (var name in assembly.GetReferencedAssemblies())
                        Process(Assembly.Load(name));
                    foreach (var type in assembly.GetTypes())
                        if (type.IsClass && !type.IsAbstract)
                            for (var ancestor = type; ancestor != typeof(Object); ancestor = ancestor.BaseType)
                            {
                                ImplementingTypes.GetOrCreate(ancestor).Add(type);
                                foreach (var @interface in ancestor.GetInterfaces())
                                    ImplementingTypes.GetOrCreate(@interface).Add(type);
                            }
                }
            }
        }

        private class Tree
        {
            public readonly List<Char> Label = new List<Char>();
            public readonly List<Tree> Children = new List<Tree>();

            public override String ToString()
                => String.Join("", Label);
        }

        private Tree Parse(String recipe)
        {
            if (recipe == null)
                return null;
            using (var enumerator = recipe.GetEnumerator())
                return Parse(enumerator);
            Tree Parse(IEnumerator<Char> chars)
            {
                var result = new Tree();
                var delimiters = "(,)";
                while (true)
                {
                    if (!chars.MoveNext())
                        return result;
                    if (delimiters.Contains(chars.Current))
                        break;
                    result.Label.Add(chars.Current);
                }
                if (chars.Current == '(')
                {
                    while (chars.Current != ')')
                        result.Children.Add(Parse(chars));
                    chars.MoveNext();
                }
                return result;
            }
        }

        public void Add(Type target, String recipe)
            => Recipes.Add(target, Parse(recipe));

        public IEnumerable<T> InstantiateAll<T>(String description = null, Action<Exception> report = null)
        {
            var inProgress = new HashSet<Type>();
            return Instances(typeof(T), Parse(description)).Cast<T>();
            IEnumerable<Object> Instances(Type target, Tree recipe)
            {
                if (recipe == null && !Recipes.TryGetValue(target, out recipe))
                    recipe = Parse("*");
                var rootLabel = recipe.ToString();
                if (rootLabel == "null")
                    return new Object[] { null };
                if (target == typeof(Boolean))
                {
                    if (rootLabel != "*")
                        return new Object[] { Boolean.Parse(rootLabel) };
                    else
                        return new Object[] { false, true };
                }
                var options = new List<IEnumerable<Object>>();
                foreach (var type in ImplementingTypes.GetOrCreate(target))
                    if ((rootLabel == "*" || rootLabel == type.Name) && inProgress.Add(type))
                    {
                        foreach (var constructor in type.GetConstructors())
                        {
                            var parameters = constructor.GetParameters();
                            var arguments = new List<IEnumerable<Object>>();
                            using (var enumerator = recipe.Children.GetEnumerator())
                            {
                                foreach (var parameter in parameters)
                                {
                                    if (parameter.Position == parameters.Length - 1 && parameter.ParameterType.IsArray)
                                    {
                                        var element = parameter.ParameterType.GetElementType();
                                        var children = new List<Tree>();
                                        while (enumerator.MoveNext())
                                            children.Add(enumerator.Current);
                                        if (children.Count == 0 && rootLabel == "*")
                                        {
                                            children.Add(new Tree());
                                            children[0].Label.AddRange("**");
                                        }
                                        arguments.Add(
                                            ((children.Count == 1 && children[0].ToString() == "**") ?
                                                Instances(element, children[0].Children.FirstOrDefault()).Subsets() :
                                                children.Select(t => Instances(element, t)).Product())
                                                .Select(NewArray(element)));
                                        break;
                                    }
                                    arguments.Add(Instances(parameter.ParameterType, enumerator.MoveNext() ? enumerator.Current : null));
                                }
                                if (enumerator.MoveNext())
                                    continue;
                            }
                            options.Add(arguments.Product().Select(NewObject(constructor)));
                        }
                        inProgress.Remove(type);
                    }
                if (options.Count == 0)
                    Console.Error.WriteLine($"warning: failed to satisfy {target.Name}");
                return options.SelectMany(Catch);
            }
            IEnumerable<Object> Catch(IEnumerable<Object> enumerable)
            {
                using (var enumerator = enumerable.GetEnumerator())
                    while (true)
                    {
                        Object value = null;
                        try
                        {
                            if (!enumerator.MoveNext())
                                break;
                            value = enumerator.Current;
                        }
                        catch (Exception error)
                        {
                            if (report != null)
                                report(error);
                        }
                        if (value != null)
                            yield return value;
                    }
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
            => throw new NotSupportedException();
    }
}
