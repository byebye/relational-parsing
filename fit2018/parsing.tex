\documentclass{beamer}

\usepackage[light,math]{anttor}
\usepackage[T1]{fontenc}

\usepackage{polski}
\usepackage[polish]{babel}
\usepackage[utf8]{inputenc}

\usepackage{tikz}
\usetikzlibrary{decorations.pathmorphing}

\definecolor{themecolor}{rgb}{0.6, 0.4, 0.2}

\usetheme{Frankfurt}
\useoutertheme[subsection=false]{miniframes}
\setbeamertemplate{mini frames}{}
\setbeamersize{mini frame size=0pt}
\usefonttheme{structuresmallcapsserif}
\setbeamerfont{frametitle}{size=\normalsize}
\setbeamerfont{block title}{size=\small}
\usecolortheme[named=themecolor]{structure}
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{blocks}[rounded][shadow=false]
%\addtobeamertemplate{block begin}{\setlength\abovedisplayskip{0pt}}

\defbeamertemplate{headline}{title}{}
\defbeamertemplate{headline}{body}{%
    \begin{beamercolorbox}[colsep=1.5pt]{upper separation line head}
    \end{beamercolorbox}
    \begin{beamercolorbox}[ht=2.5ex,dp=1.25ex,leftskip=2ex,rightskip=2ex]{section in head/foot}
        \usebeamerfont{section in head/foot}
        \insertshorttitle~$\to$~\insertsection
        \hfill
        \insertframenumber/\inserttotalframenumber
    \end{beamercolorbox}
    \begin{beamercolorbox}[colsep=1.5pt]{lower separation line head}
    \end{beamercolorbox}
}

\renewcommand{\baselinestretch}{1.1}

\newcommand{\con}{{\cdot}}
\newcommand{\move}[1][D_\epsilon]{%
    \mathrel{%
        \begin{tikzpicture}[baseline={(current bounding box.south)}]
        \node[inner sep=.44ex,align=center] (tmp) {$\scriptstyle #1$};
        \path[draw,<-,decorate,decoration={zigzag,amplitude=0.2mm,segment length=0.7mm,pre length=0.7mm}] (tmp.south east) -- (tmp.south west);
        \end{tikzpicture}
        }
    }

%\usetikzlibrary{automata,fit,positioning}
%\tikzstyle{fitbox}=[draw,dotted,transform shape=false]

\title{relational parsing}
\subtitle{a clean generalization of gll, glc and glr}
\author{Grzegorz Herman \url{<gherman@tcs.uj.edu.pl>}}
\date{Forum Informatyki Teoretycznej,\\May 11, 2018}

\begin{document}
\setbeamertemplate{headline}[title]
\maketitle
\setbeamertemplate{headline}[body]

\section{introduction}

\begin{frame}
    \begin{block}{}
    Parsing context-free languages:\\
    a problem solved but still being solved!
    \end{block}
    \pause
    \begin{block}{}
    Existing parsers are:
    \begin{itemize}
        \item general (handle arbitrary CFGs),
        \item efficient,
        \item easy to understand/implement/modify,
    \end{itemize}
    \pause
    but one can pick at most two of these three :)
    \end{block}
    \pause
    \begin{block}{}
    This work proposes a new algorithm:\\
    general, mathematically clean, and (possibly) efficient.
    \end{block}
\end{frame}

\section{grammar model}

\begin{frame}
    \begin{block}{}
    A \textbf{recursive transition network} (RTN)\\
    for an input alphabet $A$ consists of:
    \begin{itemize}
        \item a finite set $S$ of \textbf{states},
        \item a distinguished starting state $s_\texttt{start} \in S$,
        \item a finite set $D$ of \textbf{transitions}, each being one of:
            \begin{itemize}
                \item $\texttt{shift}(s,a,t)$ for $a \in A, s,t \in S$,
                \item $\texttt{call}(s,u,t)$ for $s,u,t \in S$,
                \item $\texttt{reduce}(s)$ for $s \in S$.
            \end{itemize}
    \end{itemize}
    \end{block}
\end{frame}

\begin{frame}
    \begin{block}{}
    A \textbf{configuration} of the RTN is a stack of states.\\
    We view it as a member of $S^*$, read top-to-bottom.
    \end{block}
    \pause
    \begin{block}{}
    Transitions induce legal configuration changes:
    \begin{align*}
        &s \con \sigma \move[\texttt{shift}(s,a,t)] t \con \sigma \\
        &s \con \sigma \move[\texttt{call}(s,u,t)] u \con t \con \sigma \\
        &s \con \sigma \move[\texttt{reduce}(s)] \sigma
    \end{align*}
    \end{block}
    \pause
    \begin{block}{}
    The network begins operation in configuration $s_\texttt{start}$,\\
    chooses valid transitions nondeterministically,\\
    must shift consecutive input symbols,\\
    and accepts by reaching the empty configuration $\epsilon$.
    \end{block}
\end{frame}

\section{recognition}

\begin{frame}
    \begin{block}{}
    Let $\Sigma_\alpha \subseteq S^*$
    be the language of configurations\\
    reachable from $s_\texttt{start}$ while shifting $\alpha \in A^*$.
    \end{block}
    \begin{block}{}
    Such languages can be computed inductively:
    $$
    \Sigma_\epsilon = \mathcal{C}(s_\texttt{start}) \qquad \text{and} \qquad
    \Sigma_{\alpha \con a} = \mathcal{C}(\mathcal{S}_a(\Sigma_\alpha)),
    $$
    where $\mathcal{C}(\cdot)$ closes a language by arbitrary sequences of \texttt{call} or \texttt{reduce} transitions,
    and $\mathcal{S}_a(\cdot)$ closes a language under a single $\texttt{shift}(\_,a,\_)$.
    \end{block}
    \pause
    \begin{block}{}
    Key observation:
    the languages $\Sigma_\alpha$ and operations $\mathcal{C}(\cdot)$ and $\mathcal{S}_a(\cdot)$ are \emph{regular},
    and thus can be finitely (and efficiently) represented.
    \end{block}
\end{frame}

\begin{frame}
    \begin{block}{}
    A single configuration from a language $\Sigma$
    participates in $\mathcal{C}(\mathcal{S}_a(\Sigma))$ as follows:
    $$
    s \con \sigma \move[\texttt{shift}(s,a,t)] t \con \sigma \move[D_\epsilon^*] \mathcal{C}(t) \con \sigma
    $$
    \end{block}
    \pause
    \begin{block}{}
    The set of possible top states $s$ is finite.
    The (possibly infinite) set of possible $\sigma$ can be dealt with simultaneously
    by taking the Brzozowski derivative of $\Sigma$:
    $$
    \mathcal{C}(\mathcal{S}_a(\Sigma)) \supseteq \bigcup_{\texttt{shift}(s,a,t) \in D} \mathcal{C}(t) \con \Sigma^{(s)}.
    $$
    \end{block}
\end{frame}

\begin{frame}
    \begin{block}{}
    What if $\epsilon \in \mathcal{C}(t)$?\\
    Then the call-reduce closure might continue past $\mathcal{C}(t)$,\\
    looking at symbols further down the stack.\\
    We could fix it by taking $\Theta(n)$ nested derivatives...
    \end{block}
    \pause
    \begin{block}{}
    instead we make the configuration languages \textbf{nulled}:
    $$
        \sigma \con s \con \sigma' \in \Sigma, \quad \epsilon \in \mathcal{C}(s)
        \qquad \Rightarrow \qquad
        \sigma \con \sigma' \in \Sigma.
    $$
    When $\Sigma$ is nulled, computing (nulled) $\mathcal{C}(\mathcal{S}_a(\Sigma))$
    requires only a single nesting of derivatives.
    \end{block}
\end{frame}

\begin{frame}
    \begin{block}{}
    A single inductive step of our algorithm
    can now be computed in $O(1)$ of the following operations on regular languages:
    \begin{itemize}
        \item checking whether $\Sigma = \varnothing$,
        \item checking whether $\epsilon \in \Sigma$,
        \item Brzozowski derivative,
        \item union,
        \item prepending (nulled) $\mathcal{C}(s)$.
    \end{itemize}
    \end{block}
    \begin{block}{}
    All these operations are purely mathematical,\\
    and thus amenable to immutable-style implementations.
    \end{block}
\end{frame}

\section{parsing}

\begin{frame}
    \begin{block}{}
    How do we move from recognition to parsing?\\
    By \emph{relating} the reachable configurations\\
    to (the sequences of) transitions by which they can be reached.
    \end{block}
    \pause
    \begin{block}{}
    We replace regular languages with regular relations over $S^* \times D^*$.
    Proper handling of nullables is a little tricky:\\
    the final relations are over $(S \cup D_\epsilon)^* \times D^*$.
    \end{block}
    \pause
    \begin{block}{}
    The algorithm uses the following API of regular relations:
    \begin{itemize}
        \item checking for emptyness,
        \item projecting to the empty configuration,
        \item Brzozowski derivative (by state),
        \item union,
        \item prepending (nulled) $\mathcal{C}(s)$ (now a relation),
        \item prepending a transition.
    \end{itemize}
    \end{block}
\end{frame}

\section{realizing the apis}

\begin{frame}
    \begin{block}{}
    Transition languages can be represented using:
    \begin{itemize}
        \item \texttt{bool}: parsing degenerates to recognition,
        \item a free algebra over union and concatenation:\\the result encodes all parse trees,
        \item an arbitrary other semiring (with idempotent addition).
    \end{itemize}
    \end{block}
    \pause
    \begin{block}{}
    The nulled relations $\mathcal{C}(s)$ for $s \in S$
    are expressed as simple NFAs derived from the grammar.
    Of course, any equivalent automaton will do the job, and maybe do it faster!
    \end{block}
\end{frame}

\begin{frame}
    \begin{block}{}
    Proposed data structure providing all required operations:\\
    a (persistent) DAG with vertices and edges labeled by
    (derivatives of) $\mathcal{C}(s)$ and by transition languages, respectively.
    \end{block}
    \pause
    \begin{block}{}
    Very similar to the graph-structured stack employed by common generalized parsers,
    realizing a sequence of $n$ operations in $O(n^3)$ time.
    Does \emph{not} require mutation for proper handling of nullables!
    \end{block}
    \pause
    \begin{block}{}
    Variations of this data structure make the whole algorithm behave like GLL, GLR, etc.
    Thanks to clean semantics, better separation of concerns, and immutability,
    it is easier to analyze and optimize:
    we hope to be able to make it more efficient than state-of-the-art.
    \end{block}
\end{frame}

\section{thank you}

\begin{frame}
    \begin{block}{}
    \begin{center}
    Any (context-free) questions?
    \end{center}
    \end{block}
\end{frame}

\end{document}
