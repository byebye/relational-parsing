using System;
using System.Collections.Generic;
using System.Linq;

namespace Parser.GLR
{
    internal class LR1ConfigurationEngine : IConfigurationEngine<ClosedSet<(IState, Char?)>>
    {
        public GrammarAnalysis Analysis { get; }

        public LR1ConfigurationEngine(GrammarAnalysis analysis)
        {
            Analysis = analysis;
        }

        public Boolean IsAlive(ClosedSet<(IState, Char?)> configuration) =>
            configuration.Count > 0;

        public ClosedSet<(IState, Char?)> Atomic(IState start) =>
            new ClosedSet<(IState, Char?)>(new (IState, Char?)[] { (start, null) }, Closure);

        public ClosedSet<(IState, Char?)> Call(ClosedSet<(IState, Char?)> configuration, IState child) =>
            new ClosedSet<(IState, Char?)>(
                from pair in configuration
                from call in pair.Item1.Transitions.OfType<Transition.Call>()
                where call.Child == child
                select (call.Next, pair.Item2),
                Closure);

        public IEnumerable<IState> Reduce(ClosedSet<(IState, Char?)> configuration, Char? lookahead) =>
            new HashSet<IState>(from pair in configuration where Analysis.IsNullable[pair.Item1] && pair.Item2 == lookahead select pair.Item1);

        public ClosedSet<(IState, Char?)> Shift(ClosedSet<(IState, Char?)> configuration, Char terminal) =>
            new ClosedSet<(IState, Char?)>(
                from pair in configuration
                from shift in pair.Item1.Transitions.OfType<Transition.Shift>()
                where shift.Terminal == terminal
                select (shift.Next, pair.Item2),
                Closure);

        public IEnumerable<ClosedSet<(IState, Char?)>> Successors(ClosedSet<(IState, Char?)> configuration)
        {
            foreach (var (state, lookahead) in configuration)
                foreach (var transition in state.Transitions)
                    switch (transition)
                    {
                        case Transition.Shift shift:
                            yield return Shift(configuration, shift.Terminal);
                            break;
                        case Transition.Call call:
                            yield return Call(configuration, call.Child);
                            break;
                    }
        }

        private IEnumerable<(IState, Char?)> Closure((IState, Char?) pair)
        {
            foreach (var transition in pair.Item1.Transitions)
                if (transition is Transition.Call call)
                {
                    if (Analysis.First.TryGetValue(call.Next, out var first))
                        foreach (var terminal in first)
                            yield return (call.Child, terminal);
                    if (Analysis.IsNullable[call.Next])
                        yield return (call.Child, pair.Item2);
                }
        }

        TResult IConfigurationEngine.Accept<TResult>(IConfigurationEngineVisitor<TResult> visitor) =>
            visitor.Visit(this);
    }
}
