/*
 * A simplified grammar for ANTLR v4 grammars.
 * Covers only what our implementation can currently handle.
 */

grammar Antlr4;

grammarSpec
    : GRAMMAR identifier SEMI ruleSpec* EOF
    ;

identifier
    : RULE_REF
    | TOKEN_REF
    ;

ruleSpec
    : lexerRuleSpec
    | parserRuleSpec
    ;

lexerRuleSpec
    : FRAGMENT? TOKEN_REF COLON lexerAltList (ARROW identifier)? SEMI
    ;

lexerAltList
    : lexerAlt (OR lexerAlt)*
    ;

lexerAlt
    : lexerElement*
    ;

lexerElement
    : lexerAtom (ebnfSuffix QUESTION?)?
    ;

lexerAtom
    : lexerItem
    | NOT lexerItem
    | NOT LPAREN lexerItem (OR lexerItem)* RPAREN
    | DOT
    | LPAREN lexerAltList RPAREN
    ;

lexerItem
    : TOKEN_REF
    | STRING_LITERAL
    | STRING_LITERAL RANGE STRING_LITERAL
    | CHARSET
    ;

parserRuleSpec
    : RULE_REF COLON parserAltList SEMI
    ;

parserAltList
    : parserAlt (OR parserAlt)*
    ;

parserAlt
    : parserElement*
    ;

parserElement
    : parserAtom ebnfSuffix?
    ;

parserAtom
    : RULE_REF
    | TOKEN_REF
    | LPAREN parserAltList RPAREN
    ;

ebnfSuffix
    : QUESTION
    | STAR
    | PLUS
    ;



ARROW : '->' ;
CHARSET : '[' (Escaped | ~ [\]\\])* ']' ;
COLON : ':' ;
DOT : '.' ;
FRAGMENT : 'fragment' ;
GRAMMAR : 'grammar' ;
LPAREN : '(' ;
NOT : '~' ;
OR : '|' ;
PLUS : '+' ;
QUESTION : '?' ;
RANGE : '..' ;
RPAREN : ')' ;
RULE_REF : Lower Letter* ;
SEMI : ';' ;
STAR : '*' ;
STRING_LITERAL : '\'' (Escaped | ~ ['\r\n\\])* '\'' ;
TOKEN_REF : Upper Letter* ;

BlockComment : '/*' .*? '*/' -> skip ;
LineComment : '//' ~[\r\n]* -> skip ;
Whitespace : [ \t\r\n\f] -> skip ;

fragment Escaped : '\\' . ;
fragment Letter : [0-9A-Za-z_] ;
fragment Lower : [a-z] ;
fragment Upper : [A-Z] ;

