using System;

namespace Parser.Lang
{
    public class LangParserGenerator : ParserGenerator
    {
        public override ParserFeatures Features =>
            ParserFeatures.Recognition;
        public Boolean UseLookahead { get; }

        public LangParserGenerator(Boolean useLookahead)
        {
            UseLookahead = useLookahead;
        }

        public override IParser<Boolean> CreateRecognizer(IGrammar grammar) =>
            new LangParser(grammar, UseLookahead);

        public override String ToString() =>
            $"{nameof(LangParserGenerator)}({UseLookahead})";
    }
}
