﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Parser.Lang
{
    using Common;
    using Utilities;
    using Valuations;

    public partial class LangParser : IParser<Boolean>
    {
        private readonly IGrammar Grammar;
        private readonly LookaheadAnalysis Analysis;
        public IValuationSemiring<Boolean> Semiring { get; }
            = new BooleanSemiring();

        public LangParser(IGrammar grammar, Boolean useLookahead)
        {
            Grammar = grammar;
            if (useLookahead)
                Analysis = new LookaheadAnalysis(grammar);
        }

        private Boolean CallerAndReducer(GSSLayer<(IState, IState)> layer, Char? lookahead, GSSNode<(IState, IState)> rootNode, Int32 layerIndex, ref Statistics statistics)
        {
            var accept = false;
            var queue = new Queue<(GSSNode<(IState, IState)>, GSSNode<(IState, IState)>)>();
            var reduced = new Dictionary<GSSNode<(IState, IState)>, List<GSSNode<(IState, IState)>>>();
            foreach (var node in layer)
                foreach (var child in node.Children)
                    queue.Enqueue((node, child));
            while (queue.Count > 0)
            {
                var (node, child) = queue.Dequeue();
                if (reduced.TryGetValue(node, out var parents))
                    foreach (var parent in parents)
                        if (parent.AddChild(child, ref statistics))
                            queue.Enqueue((parent, child));
                if (node.Label.Item1 == Grammar.StopState && child == rootNode)
                    accept = true;
                foreach (var transition in node.Label.Item1.Transitions)
                    if (Analysis == null || Analysis.IsViable(transition, lookahead))
                        switch (transition)
                        {
                            case Transition.Call call:
                                var newNode = layer.AddNode((call.Child, call.Next), ref statistics);
                                if (newNode.AddChild(node, ref statistics))
                                    queue.Enqueue((newNode, node));
                                break;
                            case Transition.Reduce reduce:
                                var newNode2 = layer.AddNode((node.Label.Item2, child.Label.Item2), ref statistics);
                                reduced.GetOrCreate(child).Add(newNode2);
                                foreach (var grandchild in child.Children)
                                    if (newNode2.AddChild(grandchild, ref statistics))
                                        queue.Enqueue((newNode2, grandchild));
                                break;
                        }
            }
            return accept;
        }

        private void Shifter(GSSLayer<(IState, IState)> currentLayer, GSSLayer<(IState, IState)> nextLayer, Char terminal, ref Statistics statistics)
        {
            foreach (var node in currentLayer)
                foreach (var shift in node.Label.Item1.Transitions.OfType<Transition.Shift>())
                    if (shift.Terminal == terminal)
                        foreach (var child in node.Children)
                            nextLayer.AddNode((shift.Next, node.Label.Item2), ref statistics).AddChild(child, ref statistics);
        }

        public IEnumerable<Statistics> Run(IEnumerable<Char> input, Action<Boolean> withResult)
        {
            var layerIndex = 0;
            var statistics = new Statistics();
            var currentLayer = new GSSLayer<(IState, IState)>(layerIndex);
            var root = currentLayer.AddNode((Grammar.StartState, Grammar.StopState), ref statistics);
            root.AddChild(root, ref statistics);
            Boolean accept;
            foreach (var terminal in input)
            {
                CallerAndReducer(currentLayer, terminal, root, layerIndex, ref statistics);
                yield return statistics;
                layerIndex += 1;
                statistics = new Statistics();
                var nextLayer = new GSSLayer<(IState, IState)>(layerIndex);
                Shifter(currentLayer, nextLayer, terminal, ref statistics);
                currentLayer = nextLayer;
            }
            accept = CallerAndReducer(currentLayer, null, root, layerIndex, ref statistics);
            yield return statistics;
            withResult(accept);
        }
    }
}
