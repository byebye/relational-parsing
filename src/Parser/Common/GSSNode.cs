﻿using System;
using System.Collections.Generic;

namespace Parser.Common
{
    internal class GSSNode<TLabel>
        where TLabel : IEquatable<TLabel>
    {
        public TLabel Label { get; }
        public HashSet<GSSNode<TLabel>> Children { get; }
        public Int32 LayerIndex { get; }

        public GSSNode(TLabel label, Int32 layerIndex)
        {
            Label = label;
            LayerIndex = layerIndex;
            Children = new HashSet<GSSNode<TLabel>>();
        }

        public Boolean AddChild(GSSNode<TLabel> child, ref Statistics statistics)
        {
            if (Children.Add(child))
            {
                statistics.NewEdges += 1;
                return true;
            }
            return false;
        }
    }
}
