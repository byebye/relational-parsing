using System;
using System.Collections.Generic;
using System.Linq;

namespace Parser.Algebra
{
    public interface ISemiring<TValue> : IMultiplicative<TValue>, ILeftDistributive<TValue, TValue>, IRightDistributive<TValue, TValue>
    {
        new TValue Multiply(TValue left, TValue right);
    }

    public abstract class Semiring<TValue> : Additive<TValue>, ISemiring<TValue>
    {
        public virtual Boolean IsCommutative
            => false;

        public virtual TValue One
            => Multiply(Enumerable.Empty<TValue>());

        public virtual Boolean IsOne(TValue value)
            => Comparer.Equals(value, One);

        public virtual TValue Multiply(IEnumerable<TValue> values)
            => values.Aggregate(One, Multiply);

        public virtual TValue Multiply(TValue left, TValue right)
            => Multiply(new [] { left, right });
    }
}
