using System;

namespace Parser.Relational
{
    public class RelationalParserGenerator : ParserGenerator
    {
        public override ParserFeatures Features =>
            ParserFeatures.ArbitrarySemiring;

        private readonly IRelationOperantFactory EngineFactory;
        private readonly IAtomicAnalyzer AtomicAnalyzer;
        private readonly Boolean UseLookahead;

        public RelationalParserGenerator(IRelationOperantFactory operantFactory, IAtomicAnalyzer atomicAnalyzer, Boolean useLookahead)
        {
            EngineFactory = operantFactory;
            AtomicAnalyzer = atomicAnalyzer;
            UseLookahead = useLookahead;
        }

        public override IParser<TValue> CreateParser<TValue>(IGrammar grammar, IValuationSemiring<TValue> semiring)
        {
            var engine = EngineFactory.Create(semiring);
            return engine.Accept(new CreateRequest<TValue> { Grammar = grammar, Semiring = semiring, AtomicAnalyzer = AtomicAnalyzer, UseLookahead = UseLookahead });
        }

        private class CreateRequest<TValue> : IRelationOperantVisitor<TValue, IParser<TValue>>
        {
            public IGrammar Grammar { get; set; }
            public IValuationSemiring<TValue> Semiring { get; set; }
            public IAtomicAnalyzer AtomicAnalyzer { get; set; }
            public Boolean UseLookahead { get; set; }

            public IParser<TValue> Visit<TAtom, TRelation>(IRelationOperant<TValue, TAtom, TRelation> operant)
            {
                var analysis = new GrammarAnalysis<TValue>(Grammar, Semiring);
                var atomicRelations = AtomicAnalyzer.ComputeRelations(analysis, operant.AtomEngine);
                return new RelationalParser<TValue, TAtom, TRelation>(analysis, atomicRelations, operant, UseLookahead);
            }

            public IParser<TValue> Visit<TAtom, TWrap, TShape, TRelation>(IRelationEngine<TValue, TAtom, TWrap, TShape, TRelation> engine)
                => Visit(engine as IRelationOperant<TValue, TAtom, TRelation>);
        }

        public override String ToString() =>
            $"{nameof(RelationalParserGenerator)}({EngineFactory},{AtomicAnalyzer},{UseLookahead})";
    }
}
