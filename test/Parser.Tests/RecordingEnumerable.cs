using System;
using System.Collections;
using System.Collections.Generic;

namespace Parser
{
    public sealed class RecordingEnumerable<T> : IEnumerable<T>
    {
        public IEnumerable<T> Inner { get; }
        public List<T> Target { get; set; }

        public RecordingEnumerable(IEnumerable<T> inner)
        {
            Inner = inner;
        }

        public IEnumerator<T> GetEnumerator()
        {
            if (Target == null)
                throw new InvalidOperationException("Target not set or already used");
            var enumerator = new Enumerator(Inner.GetEnumerator(), Target);
            Target = null;
            return enumerator;
        }

        IEnumerator IEnumerable.GetEnumerator() =>
            GetEnumerator();

        private class Enumerator : IEnumerator<T>
        {
            public IEnumerator<T> Inner { get; }
            public List<T> Target { get; }

            public Enumerator(IEnumerator<T> inner, List<T> target)
            {
                Inner = inner;
                Target = target;
            }

            public Boolean MoveNext()
            {
                if (!Inner.MoveNext())
                    return false;
                Target.Add(Current);
                return true;
            }

            public T Current =>
                Inner.Current;

            public void Reset() =>
                throw new NotSupportedException();

            public void Dispose() =>
                Inner.Dispose();

            Object IEnumerator.Current =>
                Current;
        }
    }
}
