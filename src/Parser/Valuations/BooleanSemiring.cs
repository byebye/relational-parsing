using System;
using System.Collections.Generic;
using System.Linq;
using No.Comparers;

namespace Parser.Valuations
{
    using static EqualityComparerFactories;

    public sealed class BooleanSemiring : ValuationSemiring<Boolean>
    {
        public override IEqualityComparer<Boolean> Comparer { get; } = OperatorEC<Boolean>();
        public override Boolean IsCommutative => true;
        public override Boolean IsIdempotent => true;
        public override Boolean Zero => false;
        public override Boolean IsZero(Boolean value) => !value;
        public override Boolean Add(Boolean left, Boolean right) => left | right;
        public override Boolean Add(IEnumerable<Boolean> values) => values.Any(b => b);
        public override Boolean One => true;
        public override Boolean IsOne(Boolean value) => value;
        public override Boolean Multiply(Boolean left, Boolean right) => left & right;
        public override Boolean Multiply(IEnumerable<Boolean> values) => values.All(b => b);
        public override Boolean Atomic(Transition transition) => true;
    }
}
