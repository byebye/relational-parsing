﻿using System;
using System.Collections.Generic;
using System.Linq;
using No.Comparers;

namespace Parser.GLR
{
    using static EqualityComparerFactories;

    public class ClosedSet<T> : HashSet<T>, IEquatable<ClosedSet<T>>
    {
        private static IEqualityComparer<ClosedSet<T>> SetComparer { get; }
            = MultiSetEC(DefaultEqualityComparer<T>.Instance);

        public ClosedSet(IEnumerable<T> states, Func<T, IEnumerable<T>> closure)
            : base(states)
        {
            var queue = new Queue<T>(this);
            while (queue.Count > 0)
                foreach (var next in closure(queue.Dequeue()))
                    if (Add(next))
                        queue.Enqueue(next);
        }

        public Boolean Equals(ClosedSet<T> other) =>
            SetComparer.Equals(this, other);

        public override Boolean Equals(Object obj) =>
            (obj is ClosedSet<T> other && other.Equals(this));

        public override Int32 GetHashCode() =>
            SetComparer.GetHashCode(this);

        public override String ToString() =>
            String.Join(".", this);
    }
}
