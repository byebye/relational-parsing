using System;
using System.Collections.Generic;
using System.Linq;

namespace Parser.Common
{
    using Utilities;

    public abstract class Classifier<TElement, TSignature, TClass>
        where TClass : ICollection<TElement>, new()
    {
        public static readonly IReadOnlyDictionary<TElement, TClass> NoClassification
            = null;

        protected abstract IEnumerable<TElement> Elements { get; }
        protected virtual IEqualityComparer<TElement> ElementEC
            => EqualityComparer<TElement>.Default;
        protected virtual IEqualityComparer<TSignature> SignatureEC
            => EqualityComparer<TSignature>.Default;
        protected abstract IEnumerable<(TElement, TSignature)> Signatures(IReadOnlyDictionary<TElement, TClass> classification);

        public IReadOnlyDictionary<TElement, TClass> Run()
        {
            var everything = new TClass();
            var classification = new Dictionary<TElement, TClass>(ElementEC);
            foreach (var element in Elements)
            {
                everything.Add(element);
                classification.Add(element, everything);
            }
            var previousCount = 0;
            var currentCount = 1;
            while (currentCount > previousCount)
            {
                previousCount = currentCount;
                var classes = new Dictionary<TSignature, TClass>(SignatureEC);
                foreach (var (element, signature) in Signatures(classification))
                    classes.GetOrCreate(signature).Add(element);
                currentCount = classes.Count;
                classification = new Dictionary<TElement, TClass>(ElementEC);
                foreach (var @class in classes.Values)
                    foreach (var element in @class)
                        classification.Add(element, @class);
            }
            if (currentCount == everything.Count)
                return NoClassification;
            Console.Error.Write($"[{this}: {everything.Count} -> {currentCount}] ");
            return classification;
        }
    }
}
