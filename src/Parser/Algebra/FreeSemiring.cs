using System;
using System.Collections.Generic;
using System.Linq;
using No.Comparers;

namespace Parser.Algebra
{
    using Utilities;
    using static EqualityComparerFactories;

    public class FreeSemiring : Semiring<FreeValue>
    {
        public sealed override IEqualityComparer<FreeValue> Comparer
            => FreeValue.Comparer;

        public class AddValue : FreeValue
        {
            public IReadOnlyCollection<FreeValue> Values;

            public override String Description
                => $"Add({String.Join(", ", Values)})";
        }

        private readonly Counter AddUniqueValues = new Counter();

        protected virtual FreeValue MakeAddValue(IReadOnlyCollection<FreeValue> values)
        {
            AddUniqueValues.Increment();
            return new AddValue { Values = values };
        }

        private readonly Dictionary<IReadOnlyCollection<FreeValue>, FreeValue> AddCache
            = new Dictionary<IReadOnlyCollection<FreeValue>, FreeValue>(MultiSetEC(FreeValue.Comparer));

        private readonly Counter AddCalls = new Counter();

        public override FreeValue Add(IEnumerable<FreeValue> values)
        {
            Cancellation.Check();
            AddCalls.Increment();
            var filtered = values.Where(v => !IsZero(v));
            var combined = IsIdempotent ? filtered.AsHashSet(FreeValue.Comparer) : filtered.AsReadOnlyCollection();
            if (combined.Count == 1)
                return combined.Single();
            return AddCache.GetOrCreate(combined, MakeAddValue);
        }

        public override FreeValue Zero { get; }

        public class MultiplyValue : FreeValue
        {
            public IReadOnlyCollection<FreeValue> Values;

            public override String Description
                => $"Multiply({String.Join(", ", Values)})";
        }

        private readonly Counter MultiplyUniqueValues = new Counter();

        protected virtual FreeValue MakeMultiplyValue(IReadOnlyCollection<FreeValue> values)
        {
            MultiplyUniqueValues.Increment();
            return new MultiplyValue { Values = values };
        }

        private readonly Dictionary<IReadOnlyCollection<FreeValue>, FreeValue> MultiplyCache
            = new Dictionary<IReadOnlyCollection<FreeValue>, FreeValue>(SequenceEC(FreeValue.Comparer));

        private readonly Counter MultiplyCalls = new Counter();

        public override FreeValue Multiply(IEnumerable<FreeValue> values)
        {
            Cancellation.Check();
            MultiplyCalls.Increment();
            var filtered = values.Where(v => !IsOne(v));
            var combined = IsCommutative ? filtered.OrderBy(v => v.Id).AsReadOnlyCollection() : filtered.AsReadOnlyCollection();
            if (combined.Count == 1)
                return combined.Single();
            if (combined.Any(IsZero))
                return Zero;
            return MultiplyCache.GetOrCreate(combined, MakeMultiplyValue);
        }

        public override FreeValue One { get; }

        public FreeSemiring()
        {
            Zero = base.Zero;
            One = base.One;
        }
    }
}
