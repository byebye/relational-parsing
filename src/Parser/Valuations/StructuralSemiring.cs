using System;
using System.Collections.Generic;
using System.Linq;
using No.Comparers;

namespace Parser.Valuations
{
    using Algebra;
    using Utilities;
    using static EqualityComparerFactories;

    public class StructuralSemiring : FreeSemiring, IValuationSemiring<FreeValue>
    {
        public override Boolean IsIdempotent
            => true;

        public class AtomicValue : FreeValue
        {
            public Transition Transition;

            public override String Description
                => Transition.ToString();
        }

        protected virtual FreeValue MakeAtomicValue(Transition transition)
            => new AtomicValue { Transition = transition };

        private readonly Dictionary<Transition, FreeValue> AtomicCache
            = new Dictionary<Transition, FreeValue>(Transition.Comparer);

        public FreeValue Atomic(Transition transition)
            => AtomicCache.GetOrCreate(transition, MakeAtomicValue);
    }

    public class ReportingStructuralSemiring : StructuralSemiring
    {
        private readonly Action<String> Report;

        public ReportingStructuralSemiring(Action<String> report)
        {
            Report = report;
        }

        private TValue GetAndReport<TValue>(TValue value)
            where TValue : FreeValue
        {
            Report($"{value} = {value.Description}");
            return value;
        }

        protected override FreeValue MakeAddValue(IReadOnlyCollection<FreeValue> values)
            => GetAndReport(base.MakeAddValue(values));

        protected override FreeValue MakeAtomicValue(Transition transition)
            => GetAndReport(base.MakeAtomicValue(transition));

        protected override FreeValue MakeMultiplyValue(IReadOnlyCollection<FreeValue> values)
            => GetAndReport(base.MakeMultiplyValue(values));
    }
}
