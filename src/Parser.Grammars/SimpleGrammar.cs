using System;
using System.Collections;
using System.Collections.Generic;

namespace Parser.Grammars
{
    public class SimpleGrammar : IGrammar, IEnumerable
    {
        private List<SimpleState> States { get; }
        public IState StartState =>
            States[0];
        public IState StopState { get; }

        public SimpleGrammar()

        {
            States = new List<SimpleState>();
            StopState = new SimpleState("$\\bot$");
        }

        public void Add(params Action<SimpleGrammar>[] actions)
        {
            foreach (var action in actions)
                action(this);
        }

        public IState this[Int32 index] =>
            Get(index);

        public static Action<SimpleGrammar> C(Int32 source, Int32 child, Int32 next) =>
            _ => _.Get(source).Add(new Transition.Call(_.Get(source), _.Get(child), _.Get(next)));

        public static Action<SimpleGrammar> R(Int32 source) =>
            _ => _.Get(source).Add(new Transition.Reduce(_.Get(source)));

        public static Action<SimpleGrammar> S(Int32 source, Char terminal, Int32 next) =>
            _ => _.Get(source).Add(new Transition.Shift(_.Get(source), terminal, _.Get(next)));

        private Int32 Make(Int32 index)
        {
            while (States.Count <= index)
                States.Add(new SimpleState(States.Count.ToString()));
            return index;
        }

        private SimpleState Get(Int32 index) =>
            States[Make(index)];

        public Int32 NewState() =>
            (States.Count, this[States.Count]).Item1;

        IEnumerator IEnumerable.GetEnumerator() =>
            throw new NotSupportedException("IEnumerable faked to make initialization syntax work");

        private sealed class SimpleState : IState
        {
            public SimpleState(String name)
            {
                Name = name;
            }

            public String Name { get; }

            private List<Transition> transitions =
                new List<Transition>();

            public IEnumerable<Transition> Transitions =>
                transitions;

            public void Add(Transition transition) =>
                transitions.Add(transition);

            public Boolean Equals(IState other) =>
                ReferenceEquals(this, other);

            public override String ToString() =>
                Name;
        }
    }
}
