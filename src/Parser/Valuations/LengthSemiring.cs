using System;
using System.Collections.Generic;
using No.Comparers;

namespace Parser.Valuations
{
    using static EqualityComparerFactories;

    public sealed class LengthSemiring : ValuationSemiring<Int64?>
    {
        public override IEqualityComparer<Int64?> Comparer { get; } = OperatorEC<Int64?>();
        public override Int64? Atomic(Transition transition) => 1L;
        public override Int64? Zero => null;
        public override Boolean IsZero(Int64? value) => value == null;
        public override Int64? Add(Int64? left, Int64? right) => left == null ? right : right == null ? left : Math.Max(left.Value, right.Value);
        public override Int64? One => 0L;
        public override Boolean IsOne(Int64? value) => value == 0L;
        public override Int64? Multiply(Int64? left, Int64? right) => left + right;
        public override Boolean IsCommutative => true;
        public override Boolean IsIdempotent => true;
    }
}
