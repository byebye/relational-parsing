using System;
using System.Collections.Generic;
using System.Linq;

namespace Parser.Relational
{
    using Algebra;
    using Utilities;

    public interface IAtomicAnalyzer
    {
        IAtomicRelations<TAtom> ComputeRelations<TValue, TAtom>(GrammarAnalysis<TValue> analysis, IAtomEngine<TValue, TAtom> atomEngine);
    }

    public class AtomicAnalyzer : IAtomicAnalyzer
    {
        public virtual IAtomicRelations<TAtom> ComputeRelations<TValue, TAtom>(GrammarAnalysis<TValue> analysis, IAtomEngine<TValue, TAtom> atomEngine)
        {
            var relations = new AtomicRelations<TAtom>();
            var values = new Dictionary<GrammarAnalysis<TValue>.EdgeRelation, TAtom>();
            var inProgress = new HashSet<GrammarAnalysis<TValue>.EdgeRelation>();
            TAtom Evaluate(GrammarAnalysis<TValue>.EdgeRelation relation)
            {
                TAtom result;
                if (!values.TryGetValue(relation, out result))
                {
                    if (!inProgress.Add(relation))
                        throw new NotImplementedException("Support for infinitely ambiguous grammars has not yet been implemented");
                    var items = relation.Recursive.Select(r => atomEngine.Multiply(Evaluate(r.Inner), (r.Prefix, r.Suffix))).ToList();
                    if (relation.HasBase)
                        items.Add(atomEngine.Embed((atomEngine.Semiring.One, relation.Base)));
                    result = atomEngine.Add(items);
                    values.Add(relation, result);
                }
                return result;
            }
            var noEdge = new GrammarAnalysis<TValue>.EdgeRelation();
            var startEpsilon = analysis.NullingTransitions.TryGetValue(analysis.Grammar.StartState, out var nulling) ? new GrammarAnalysis<TValue>.EdgeRelation(nulling) : noEdge;
            var startRelation = new AtomicRelation<TAtom>("(init)", Evaluate(startEpsilon));
            relations.StartRelation = startRelation;
            foreach (var state in analysis.ProductiveStates)
            {
                var byTarget = new Dictionary<IState, AtomicRelation<TAtom>>();
                var queue = new Queue<(IState, IAtomicRelation<TAtom>)>();
                foreach (var (target, edge) in analysis.NodeRelations[state])
                {
                    var final = new AtomicRelation<TAtom>($"({state}:{target})", Evaluate(edge));
                    byTarget.Add(target, final);
                    queue.Enqueue((target, final));
                }
                while (queue.Count > 0)
                {
                    var (target, current) = queue.Dequeue();
                    foreach (var move in analysis.ProductiveStates)
                        if (analysis.EdgeRelations.TryGetValue((move, target), out var edges))
                            foreach (var (source, edge) in edges)
                            {
                                AtomicRelation<TAtom> next;
                                if (!byTarget.TryGetValue(source, out next))
                                {
                                    next = new AtomicRelation<TAtom>($"({state}.{source})", Evaluate(noEdge));
                                    byTarget.Add(source, next);
                                    queue.Enqueue((source, next));
                                }
                                next.Derivatives.Add((move, current), Evaluate(edge));
                            }
                }
                var noTarget = new AtomicRelation<TAtom>($"({state}.$\\epsilon$)", Evaluate(noEdge));
                foreach (var (target, relation) in byTarget)
                    if (analysis.ProductiveStates.Contains(target))
                        noTarget.Derivatives.Add((target, relation), atomEngine.One);
                relations.StateRelations.Add(state, noTarget);
                if (state == analysis.Grammar.StartState)
                    foreach (var kvp in noTarget.Derivatives)
                        startRelation.Derivatives.Add(kvp.Key, kvp.Value);
            }
            return relations;
        }

        public override String ToString()
            => $"{nameof(AtomicAnalyzer)}";
    }
}
