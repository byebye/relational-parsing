using System;
using System.Collections.Generic;
using System.Linq;
using No.Comparers;

namespace Parser.Relational
{
    using Algebra;
    using Graphs;
    using Utilities;
    using static EqualityComparerFactories;

    public struct DagWrapEdge<TValue, TAtom>
    {
        public readonly Boolean IsValue;
        private readonly Object ValueOrAtom;

        public DagWrapEdge(TValue value)
        {
            IsValue = true;
            ValueOrAtom = value;
        }

        public DagWrapEdge(TAtom atom)
        {
            IsValue = false;
            ValueOrAtom = atom;
        }

        public TValue Value
            => (TValue) ValueOrAtom;

        public TAtom Atom
            => (TAtom) ValueOrAtom;

        public static IEqualityComparer<DagWrapEdge<TValue, TAtom>> EdgeEC(IAtomEngine<TValue, TAtom> engine)
            => new VariantsEqualityComparer<DagWrapEdge<TValue, TAtom>, Boolean>(_ => _.IsValue, OperatorEC<Boolean>())
            {
                { true, _ => _.Value, engine.Semiring.Comparer },
                { false, _ => _.Atom, engine.Comparer },
            };
    }

    public class DagWrapEngine<TValue, TAtom, TNode> : WrapEngine<TValue, TAtom, TNode>
    {
        public override IAtomEngine<TValue, TAtom> AtomEngine { get; }
        private readonly IGraphEngine<DagWrapEdge<TValue, TAtom>, TNode> GraphEngine;

        public DagWrapEngine(IAtomEngine<TValue, TAtom> atomEngine, IGraphEngine<DagWrapEdge<TValue, TAtom>, TNode> graphEngine)
        {
            AtomEngine = atomEngine;
            GraphEngine = graphEngine;
            Root = GraphEngine.Create((new DagWrapEdge<TValue, TAtom>(AtomEngine.Semiring.Zero), GraphEngine.Zero));
            Zero = GraphEngine.Zero;
            One = GraphEngine.Create((new DagWrapEdge<TValue, TAtom>(AtomEngine.Semiring.One), Root));
        }

        private ISemiring<TValue> Semiring
            => AtomEngine.Semiring;

        public override Boolean IsIdempotent
            => AtomEngine.IsIdempotent;

        private readonly TNode Root;

        public override IEqualityComparer<TNode> Comparer
            => GraphEngine.Comparer;

        public override TNode Add(IEnumerable<TNode> wraps)
            => GraphEngine.Add(wraps);

        public override TNode Zero { get; }
        public override TNode One { get; }

        public override TNode Multiply(TValue value, TNode wrap)
            => IsZero(wrap) || Semiring.IsOne(value) ? wrap : GraphEngine.Create((new DagWrapEdge<TValue, TAtom>(value), wrap));

        public override TNode Multiply(TAtom atom, TNode wrap)
            => IsZero(wrap) || AtomEngine.IsOne(atom) ? wrap : GraphEngine.Create((new DagWrapEdge<TValue, TAtom>(atom), wrap));

        private readonly Counter MultiplyDfsCalls = new Counter();

        public override TNode Multiply(TNode inner, TNode outer)
        {
            if (IsZero(inner) || IsOne(outer))
                return inner;
            if (IsOne(inner))
                return outer;
            var results = new Dictionary<TNode, TNode>(Comparer) { { Root, outer } };
            TNode Dfs(TNode node)
            {
                MultiplyDfsCalls.Increment();
                TNode result;
                if (!results.TryGetValue(node, out result))
                {
                    result = GraphEngine.Create(GraphEngine.Neighbors(node).Select(_ => (_.Item1, Dfs(_.Item2))));
                    results.Add(node, result);
                }
                return result;
            }
            return Dfs(inner);
        }

        private readonly Counter FlattenDfsCalls = new Counter();

        public override TValue Flatten(TNode wrap)
        {
            var visited = new HashSet<TNode>(Comparer);
            var topological = new List<TNode>();
            void Dfs(TNode node)
            {
                FlattenDfsCalls.Increment();
                if (visited.Add(node))
                {
                    foreach (var (_, neighbor) in GraphEngine.Neighbors(node))
                        Dfs(neighbor);
                    topological.Add(node);
                }
            }
            Dfs(wrap);
            if (!visited.Contains(Root))
            {
                return Semiring.Zero;
            }
            topological.Reverse();
            var values = new Dictionary<TNode, List<TValue>>(Comparer);
            values.GetOrCreate(wrap).Add(Semiring.One);
            foreach (var node in topological)
            {
                var union = Semiring.Add(values[node]);
                if (Comparer.Equals(node, Root))
                    return union;
                foreach (var (edge, neighbor) in GraphEngine.Neighbors(node))
                    values.GetOrCreate(neighbor).Add(edge.IsValue ? Semiring.Multiply(union, edge.Value) : AtomEngine.Wrap(union, edge.Atom));
            }
            throw new InvalidProgramException();
        }
    }

    public class DagWrapEngineFactory : IWrapEngineFactory
    {
        private readonly IAtomEngineFactory AtomEngineFactory;
        private readonly IGraphEngineFactory GraphEngineFactory;

        public DagWrapEngineFactory(IAtomEngineFactory atomEngineFactory, IGraphEngineFactory graphEngineFactory)
        {
            AtomEngineFactory = atomEngineFactory;
            GraphEngineFactory = graphEngineFactory;
        }

        public IWrapEngine<TValue> Create<TValue>(ISemiring<TValue> semiring)
            => AtomEngineFactory
                .Create(semiring)
                .Accept(new CreateRequest<TValue> { GraphEngineFactory = GraphEngineFactory });

        private class CreateRequest<TValue> : IAtomEngineVisitor<TValue, IWrapEngine<TValue>>
        {
            public IGraphEngineFactory GraphEngineFactory { get; set; }

            public IWrapEngine<TValue> Visit<TAtom>(IAtomEngine<TValue, TAtom> engine)
                => GraphEngineFactory
                    .CreateNoParallel(DagWrapEdge<TValue, TAtom>.EdgeEC(engine), null, engine.IsIdempotent)
                    .Accept(new CreateRequest<TValue, TAtom> { AtomEngine = engine });
        }

        private class CreateRequest<TValue, TAtom> : IGraphEngineVisitor<DagWrapEdge<TValue, TAtom>, IWrapEngine<TValue>>
        {
            public IAtomEngine<TValue, TAtom> AtomEngine { get; set; }

            public IWrapEngine<TValue> Visit<TNode>(IGraphEngine<DagWrapEdge<TValue, TAtom>, TNode> engine)
                => new DagWrapEngine<TValue, TAtom, TNode>(AtomEngine, engine);
        }

        public override String ToString()
            => $"{nameof(DagWrapEngineFactory)}({AtomEngineFactory},{GraphEngineFactory})";
    }

    public class MemoizingDagWrapEngine<TValue, TAtom, TNode> : DagWrapEngine<TValue, TAtom, TNode>
    {
        private readonly Dictionary<TNode, TValue> FlattenCache;
        private readonly Dictionary<TNode, TNode> FoldCache;

        public MemoizingDagWrapEngine(IAtomEngine<TValue, TAtom> atomEngine, IGraphEngine<DagWrapEdge<TValue, TAtom>, TNode> graphEngine)
            : base(atomEngine, graphEngine)
        {
            FlattenCache = new Dictionary<TNode, TValue>(Comparer);
            FoldCache = new Dictionary<TNode, TNode>(Comparer);
        }

        public override TValue Flatten(TNode wrap)
        {
            TValue result;
            if (!FlattenCache.TryGetValue(wrap, out result))
            {
                result = base.Flatten(wrap);
                FlattenCache.Add(wrap, result);
            }
            return result;
        }

        public override TNode Fold(TNode wrap)
        {
            TNode result;
            if (!FoldCache.TryGetValue(wrap, out result))
            {
                result = base.Fold(wrap);
                FoldCache.Add(wrap, result);
            }
            return result;
        }
    }

    public class MemoizingDagWrapEngineFactory : IWrapEngineFactory
    {
        private readonly IAtomEngineFactory AtomEngineFactory;
        private readonly IGraphEngineFactory GraphEngineFactory;

        public MemoizingDagWrapEngineFactory(IAtomEngineFactory atomEngineFactory, IGraphEngineFactory graphEngineFactory)
        {
            AtomEngineFactory = atomEngineFactory;
            GraphEngineFactory = graphEngineFactory;
        }

        public IWrapEngine<TValue> Create<TValue>(ISemiring<TValue> semiring)
            => AtomEngineFactory
                .Create(semiring)
                .Accept(new CreateRequest<TValue> { GraphEngineFactory = GraphEngineFactory });

        private class CreateRequest<TValue> : IAtomEngineVisitor<TValue, IWrapEngine<TValue>>
        {
            public IGraphEngineFactory GraphEngineFactory { get; set; }

            public IWrapEngine<TValue> Visit<TAtom>(IAtomEngine<TValue, TAtom> engine)
                => GraphEngineFactory
                    .CreateNoParallel(DagWrapEdge<TValue, TAtom>.EdgeEC(engine), null, engine.IsIdempotent)
                    .Accept(new CreateRequest<TValue, TAtom> { AtomEngine = engine });
        }

        private class CreateRequest<TValue, TAtom> : IGraphEngineVisitor<DagWrapEdge<TValue, TAtom>, IWrapEngine<TValue>>
        {
            public IAtomEngine<TValue, TAtom> AtomEngine { get; set; }

            public IWrapEngine<TValue> Visit<TNode>(IGraphEngine<DagWrapEdge<TValue, TAtom>, TNode> engine)
                => new MemoizingDagWrapEngine<TValue, TAtom, TNode>(AtomEngine, engine);
        }

        public override String ToString()
            => $"{nameof(MemoizingDagWrapEngineFactory)}({AtomEngineFactory},{GraphEngineFactory})";
    }
}
