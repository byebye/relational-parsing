using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace Parser.Tests
{
    public class DebugParserGenerator : ParserGenerator
    {
        public ParserGenerator Inner { get; }

        public override ParserFeatures Features =>
            Inner.Features;

        public DebugParserGenerator(ParserGenerator inner)
        {
            Inner = inner;
        }

        public override IParser<TValue> CreateParser<TValue>(IGrammar grammar, IValuationSemiring<TValue> semiring) =>
            new DebugParser<TValue>(grammar, Inner.CreateParser(grammar, new DebugSemiring<TValue>(semiring)));
    }

    public class DebugParser<TValue> : IParser<TValue>
    {
        private readonly IGrammar Grammar;
        private readonly IParser<DebugSemiring<TValue>.Summary> Inner;
        public IValuationSemiring<TValue> Semiring
            => (Inner.Semiring as DebugSemiring<TValue>).Inner;

        public DebugParser(IGrammar grammar, IParser<DebugSemiring<TValue>.Summary> inner)
        {
            Grammar = grammar;
            Inner = inner;
        }

        public IEnumerable<Statistics> Run(IEnumerable<Char> input, Action<TValue> withResult)
        {
            var terminals = new List<Char>();
            return Inner.Run(
                new RecordingEnumerable<Char>(input) { Target = terminals },
                result =>
                {
                    if (result.Terminals != null)
                    {
                        Assert.True(result.Consumed.SequenceEqual(new[] { Grammar.StartState }));
                        Assert.True(result.Terminals.SequenceEqual(terminals));
                        Assert.True(result.Produced.Count == 0);
                    }
                    withResult(result.Inner);
                });
        }
    }
}
