# Relational parsing

This is the repository of the **relational parsing** research project -
an experimental generalized parsing algorithm developed at Jagiellonian University.

You can find here both a proof-of-concept implementation of the algorithm
(without any documentation yet, sorry)
and a research paper with the theory behind it.

The paper has been uploaded to [arXiv](https://arxiv.org/abs/1902.06591),
but it is still a manuscript, with missing parts and many rough edges.
To avoid flooding arXiv with too many versions,
we are only going to publish an update when we judge the changes to be substantial enough.
If you want the "bleeding edge" version of the paper,
build automatically from the master branch of the repository,
you can get it [here](https://gitlab.com/alt-lang/parser-paper/-/jobs/artifacts/master/raw/paper/full.pdf?job=paper).

If you have any comments,
regarding either the algorithm itself, its implementation, or the paper,
please drop us an email!
