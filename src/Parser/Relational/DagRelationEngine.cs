﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using No.Comparers;

namespace Parser.Relational
{
    using Algebra;
    using Graphs;
    using Utilities;
    using static EqualityComparerFactories;

    public static class DagRelationEngine<TValue, TAtom, TWrap>
    {
        public struct Suffix
        {
            public IAtomicRelation<TAtom> Atomic;
            public TWrap Epsilon;
        }

        public struct Edge
        {
            public readonly TWrap Prefix;
            public readonly Suffix Suffix;

            public Edge(TWrap prefix, Suffix suffix)
            {
                Prefix = prefix;
                Suffix = suffix;
            }

            public Edge(TWrap prefix, IAtomicRelation<TAtom> atomic, TWrap epsilon)
                : this(prefix, new Suffix { Atomic = atomic, Epsilon = epsilon })
            {
            }

            public override String ToString()
                => $"{Prefix},{Suffix.Atomic},{Suffix.Epsilon}";
        }

        public static IEqualityComparer<Edge> EdgeEC(IWrapEngine<TValue, TAtom, TWrap> wrapEngine, IEqualityComparer<IAtomicRelation<TAtom>> atomicEC)
            => new ComponentsEqualityComparer<Edge>
            {
                { _ => _.Prefix, wrapEngine.Comparer, false },
                { _ => _.Suffix, UniqueHashCodeEC(new ComponentsEqualityComparer<Suffix>
                {
                    { _ => _.Atomic, atomicEC },
                    { _ => _.Epsilon, wrapEngine.Comparer },
                }) },
            };

        public static Func<Edge, Edge, Edge> MergeEdges(IWrapEngine<TValue, TAtom, TWrap> wrapEngine, IEqualityComparer<IAtomicRelation<TAtom>> atomicEC)
            => (one, two) => new Edge(wrapEngine.Add(one.Prefix, two.Prefix), one.Suffix);
    }

    public class DagRelationEngine<TValue, TAtom, TWrap, TNode> : RelationEngine<TValue, TAtom, TWrap, (Int32, DagRelationEngine<TValue, TAtom, TWrap, TNode>.Shape), (TWrap, TNode)>
    {
        public override IWrapEngine<TValue, TAtom, TWrap> WrapEngine { get; }
        protected readonly IGraphEngine<DagRelationEngine<TValue, TAtom, TWrap>.Edge, TNode> GraphEngine;
        public override IEqualityComparer<(TWrap, TNode)> Comparer { get; }
        protected IEqualityComparer<TNode> NeighborsEC
            => GraphEngine.Comparer;
        public override IEqualityComparer<(Int32, Shape)> ShapeComparer { get; }

        public DagRelationEngine(IWrapEngine<TValue, TAtom, TWrap> wrapEngine, IGraphEngine<DagRelationEngine<TValue, TAtom, TWrap>.Edge, TNode> graphEngine, IEqualityComparer<IAtomicRelation<TAtom>> atomicEC)
        {
            WrapEngine = wrapEngine;
            GraphEngine = graphEngine;
            Comparer = ValueTupleEC(WrapEngine.Comparer, GraphEngine.Comparer);
            var intEC = OperatorEC<Int32>();
            ShapeComparer = ValueTupleEC(intEC, SequenceEC(ArrayEC(ValueTupleEC(intEC, atomicEC, intEC, intEC))) as IEqualityComparer<Shape>);
            One = (WrapEngine.One, GraphEngine.Zero);
            Zero = base.Zero;
        }

        protected DagRelationEngine<TValue, TAtom, TWrap>.Edge MakeEdge(TWrap prefix, IAtomicRelation<TAtom> atomic, TWrap epsilon)
            => new DagRelationEngine<TValue, TAtom, TWrap>.Edge(prefix, atomic, epsilon);

        protected DagRelationEngine<TValue, TAtom, TWrap>.Edge Prepend(TValue value, DagRelationEngine<TValue, TAtom, TWrap>.Edge edge)
            => new DagRelationEngine<TValue, TAtom, TWrap>.Edge(WrapEngine.Multiply(value, edge.Prefix), edge.Suffix);

        protected DagRelationEngine<TValue, TAtom, TWrap>.Edge Prepend(TAtom atom, DagRelationEngine<TValue, TAtom, TWrap>.Edge edge)
            => new DagRelationEngine<TValue, TAtom, TWrap>.Edge(WrapEngine.Multiply(atom, edge.Prefix), edge.Suffix);

        protected DagRelationEngine<TValue, TAtom, TWrap>.Edge Prepend(TWrap wrap, DagRelationEngine<TValue, TAtom, TWrap>.Edge edge)
            => new DagRelationEngine<TValue, TAtom, TWrap>.Edge(WrapEngine.Multiply(wrap, edge.Prefix), edge.Suffix);

        public override (TWrap, TNode) One { get; }

        public override Boolean IsOne((TWrap, TNode) value)
            => WrapEngine.IsOne(value.Item1) && GraphEngine.IsZero(value.Item2);

        public override (TWrap, TNode) Zero { get; }

        public override Boolean IsZero((TWrap, TNode) value)
            => WrapEngine.IsZero(value.Item1) && GraphEngine.IsZero(value.Item2);

        public override TWrap Epsilon((TWrap, TNode) relation)
            => relation.Item1;

        protected virtual TNode Prepend(TValue value, TNode node)
            => GraphEngine.Map(node, _ => Prepend(value, _));

        protected virtual TNode Prepend(TAtom atom, TNode node)
            => GraphEngine.Map(node, _ => Prepend(atom, _));

        protected virtual TNode Prepend(TWrap wrap, TNode node)
            => GraphEngine.Map(node, _ => Prepend(wrap, _));

        public override IEnumerable<(IState, IReadOnlyCollection<(TWrap, TNode)>)> Derivatives((TWrap, TNode) relation, Func<IState, Boolean> filter)
        {
            var epsilons = new Dictionary<IState, List<TWrap>>();
            var neighbors = new Dictionary<IState, List<TNode>>();
            var finals = new Dictionary<IState, List<(DagRelationEngine<TValue, TAtom, TWrap>.Edge, TNode)>>();
            foreach (var (edge, target) in GraphEngine.Neighbors(relation.Item2))
                foreach (var (state, prefix, atomic) in edge.Suffix.Atomic.Derivatives)
                    if (filter(state))
                    {
                        var leading = WrapEngine.Fold(WrapEngine.Multiply(WrapEngine.Fold(edge.Prefix), WrapEngine.Embed(prefix)));
                        if (!WrapEngine.AtomEngine.IsZero(atomic.Epsilon))
                        {
                            var epsilon = WrapEngine.Multiply(leading, WrapEngine.Embed(atomic.Epsilon));
                            epsilons.GetOrCreate(state).Add(WrapEngine.Multiply(epsilon, edge.Suffix.Epsilon));
                            neighbors.GetOrCreate(state).Add(Prepend(epsilon, target));
                        }
                        if (atomic.Derivatives.Any())
                        {
                            finals.GetOrCreate(state).Add((MakeEdge(leading, atomic, edge.Suffix.Epsilon), target));
                        }
                    }
            foreach (var (state, final) in finals)
                neighbors.GetOrCreate(state).Add(GraphEngine.Create(final));
            var result = new List<(IState, IReadOnlyCollection<(TWrap, TNode)>)>();
            foreach (var (state, targets) in neighbors)
                result.Add((state, new [] {(WrapEngine.Add(epsilons.GetOrCreate(state)), GraphEngine.Add(targets))}));
            return result;
        }

        public override (TWrap, TNode) Multiply(TValue left, (TWrap, TNode) right)
            => (WrapEngine.Multiply(left, right.Item1), Prepend(left, right.Item2));

        public override (TWrap, TNode) Multiply(IAtomicRelation<TAtom> left, (TWrap, TNode) right)
        {
            var node = left.Derivatives.Any() ?
                GraphEngine.Create((MakeEdge(WrapEngine.One, left, right.Item1), right.Item2)) :
                GraphEngine.Zero;
            return (WrapEngine.AtomEngine.IsZero(left.Epsilon)) ?
                (WrapEngine.Zero, node) :
                (WrapEngine.Multiply(left.Epsilon, right.Item1), GraphEngine.Add(node, Prepend(left.Epsilon, right.Item2)));
        }

        protected virtual TNode Concat(TNode left, (TWrap, TNode) right)
        {
            var nodes = new List<TNode>();
            var final = new List<(DagRelationEngine<TValue, TAtom, TWrap>.Edge, TNode)>();
            foreach (var (edges, neighbor) in GraphEngine.Neighbors(left))
            {
                var (epsilon, target) = Concat((edges.Suffix.Epsilon, neighbor), right);
                final.Add((MakeEdge(edges.Prefix, edges.Suffix.Atomic, epsilon), target));
            }
            if (final.Count > 0)
                nodes.Add(GraphEngine.Create(final));
            return GraphEngine.Add(nodes);
        }

        public override (TWrap, TNode) Concat((TWrap, TNode) left, (TWrap, TNode) right)
        {
            var node = Concat(left.Item2, right);
            if (!WrapEngine.IsZero(left.Item1))
                node = GraphEngine.Add(node, GraphEngine.Map(right.Item2, _ => Prepend(left.Item1, _)));
            return (WrapEngine.Multiply(left.Item1, right.Item1), node);
        }

        protected virtual Int32 Rank(TNode neighbors)
            => GraphEngine.Neighbors(neighbors).Select(_ => Rank(_.Item2)).DefaultIfEmpty(-1).Max() + 1;

        protected virtual Boolean Dominator(ref TNode neighbors)
        {
            if (GraphEngine.Neighbors(neighbors).Any(_ => !WrapEngine.IsZero(_.Item1.Suffix.Epsilon)))
                return false;
            var candidates = new HashSet<TNode>(GraphEngine.Neighbors(neighbors).Select(_ => _.Item2));
            if (candidates.Count == 0)
                return false;
            while (candidates.Count > 1)
            {
                var two = candidates.Take(2).ToArray();
                candidates.Remove(two[0]);
                candidates.Remove(two[1]);
                if (Rank(two[0]) < Rank(two[1]))
                    Array.Reverse(two);
                if (!Dominator(ref two[0]))
                    return false;
                candidates.Add(two[0]);
                candidates.Add(two[1]);
            }
            neighbors = candidates.Single();
            return true;
        }

        protected virtual (TWrap, TNode) FactorOut(TNode concat, TNode right)
        {
            if (NeighborsEC.Equals(concat, right))
                return One;
            var finals = new List<(DagRelationEngine<TValue, TAtom, TWrap>.Edge, TNode)>();
            foreach (var (edge, neighbor) in GraphEngine.Neighbors(concat))
            {
                if (!WrapEngine.IsZero(edge.Suffix.Epsilon))
                    throw new InvalidOperationException();
                var (epsilon, target) = FactorOut(neighbor, right);
                finals.Add((MakeEdge(edge.Prefix, edge.Suffix.Atomic, epsilon), target));
            }
            return (WrapEngine.Zero, GraphEngine.Create(finals));
        }

        public override IReadOnlyCollection<(TWrap, TNode)> Factorize((TWrap, TNode) relation)
        {
            var factors = new Stack<(TWrap, TNode)>();
            while (WrapEngine.IsZero(relation.Item1))
            {
                var previous = relation.Item2;
                if (!Dominator(ref relation.Item2))
                    break;
                factors.Push(FactorOut(previous, relation.Item2));
            }
            factors.Push(relation);
            return factors;
        }

        public override (TWrap, TNode) Add(IEnumerable<(TWrap, TNode)> relations)
        {
            var epsilons = new List<TWrap>();
            var edges = new List<TNode>();
            foreach (var (epsilon, neighbors) in relations)
            {
                epsilons.Add(epsilon);
                edges.Add(neighbors);
            }
            return (WrapEngine.Add(epsilons), GraphEngine.Add(edges));
        }

        public class Shape : List<(Int32, IAtomicRelation<TAtom>, Int32, Int32)[]>
        {
            public override String ToString()
                => String.Join("; ", this.Select((entries, index) => $"{index}: {{{String.Join(", ", entries.Select(entry => $"(?,{entry.Item2},{(entry.Item3 >= 0 ? '?' : '0')},{entry.Item4})"))}}}"));
        }

        public override (TWrap, TNode) Compose((Int32, Shape) shape, IReadOnlyList<TWrap> content)
        {
            TWrap Content(Int32 index) => (index >= 0) ? content[index] : WrapEngine.Zero;
            var epsilon = Content(shape.Item1);
                var nodes = new List<TNode>();
                foreach (var entries in shape.Item2)
                nodes.Add(GraphEngine.Create(entries.Select(e => (MakeEdge(Content(e.Item1), e.Item2, Content(e.Item3)), nodes[e.Item4]))));
            return (epsilon, nodes[nodes.Count - 1]);
        }

        public override ((Int32, Shape), IReadOnlyList<TWrap>) Decompose((TWrap, TNode) relation)
        {
            var content = new List<TWrap>();
            var locations = new Dictionary<TWrap, Int32>(WrapEngine.Comparer);
            Int32 Location(TWrap wrap)
                {
                if (WrapEngine.IsZero(wrap))
                    return -1;
                Int32 location;
                if (!locations.TryGetValue(wrap, out location))
                    {
                    location = content.Count;
                    content.Add(wrap);
                    locations.Add(wrap, location);
            }
                return location;
        }
            var epsilon = Location(relation.Item1);
            var shape = new Shape();
            var indices = new Dictionary<TNode, Int32>(GraphEngine.Comparer);
            Int32 Index(TNode node)
            {
                Int32 index;
                if (!indices.TryGetValue(node, out index))
                {
                    var entries = GraphEngine.Neighbors(node).Select(_ => (Location(_.Item1.Prefix), _.Item1.Suffix.Atomic, Location(_.Item1.Suffix.Epsilon), Index(_.Item2))).ToArray();
                    index = shape.Count;
                    shape.Add(entries);
                    indices.Add(node, index);
                }
                return index;
            }
            Index(relation.Item2);
            return ((epsilon, shape), content);
        }
    }

    public class DagRelationEngineFactory : RelationEngineFactory
    {
        private readonly IGraphEngineFactory GraphEngineFactory;

        public DagRelationEngineFactory(IWrapEngineFactory wrapEngineFactory, IGraphEngineFactory graphEngineFactory)
            : base(wrapEngineFactory)
        {
            GraphEngineFactory = graphEngineFactory;
        }

        public override IRelationEngine<TValue> Create<TValue>(IValuationSemiring<TValue> semiring, IWrapEngine<TValue> wrapEngine)
            => wrapEngine
                .Accept(new CreateRequest<TValue> { GraphEngineFactory = GraphEngineFactory });

        private class CreateRequest<TValue> : IWrapEngineVisitor<TValue, IRelationEngine<TValue>>
        {
            public IGraphEngineFactory GraphEngineFactory { get; set; }

            public Relational.IRelationEngine<TValue> Visit<TAtom, TWrap>(IWrapEngine<TValue, TAtom, TWrap> engine)
            {
                var atomicEC = IdentityEC<IAtomicRelation<TAtom>>();
                return GraphEngineFactory
                    .Create(
                        DagRelationEngine<TValue, TAtom, TWrap>.EdgeEC(engine, atomicEC),
                        DagRelationEngine<TValue, TAtom, TWrap>.MergeEdges(engine, atomicEC),
                        engine.IsIdempotent)
                    .Accept(new CreateRequest<TValue, TAtom, TWrap> { WrapEngine = engine, AtomicEC = atomicEC });
        }
        }

        private class CreateRequest<TValue, TAtom, TWrap> : IGraphEngineVisitor<DagRelationEngine<TValue, TAtom, TWrap>.Edge, IRelationEngine<TValue>>
        {
            public IWrapEngine<TValue, TAtom, TWrap> WrapEngine { get; set; }
            public IEqualityComparer<IAtomicRelation<TAtom>> AtomicEC { get; set; }

            public IRelationEngine<TValue> Visit<TNode>(IGraphEngine<DagRelationEngine<TValue, TAtom, TWrap>.Edge, TNode> engine)
                => new DagRelationEngine<TValue, TAtom, TWrap, TNode>(WrapEngine, engine, AtomicEC);
        }

        public override String ToString() =>
            $"{nameof(DagRelationEngineFactory)}({WrapEngineFactory},{GraphEngineFactory})";
    }

    public class MemoizingDagRelationEngine<TValue, TAtom, TWrap, TNode> : DagRelationEngine<TValue, TAtom, TWrap, TNode>
    {
        private readonly Dictionary<(TAtom, TNode), TNode> PrependAtomCache;
        private readonly Dictionary<(TWrap, TNode), TNode> PrependWrapCache;
        private readonly Dictionary<(TNode, Func<IState, Boolean>), IEnumerable<(IState, IReadOnlyCollection<(TWrap, TNode)>)>> DerivativesCache;
        private readonly Dictionary<(TNode, (TWrap, TNode)), TNode> ConcatCache;
        private readonly Dictionary<TNode, Int32> RankCache;
        private readonly Dictionary<TNode, (Boolean, TNode)> DominatorCache;
        private readonly Dictionary<(TNode, TNode), (TWrap, TNode)> FactorOutCache;
        private readonly Dictionary<(IRelationalOperation<TValue, TAtom>, (TWrap, TNode)), (TWrap, TNode)> PerformCache;

        public MemoizingDagRelationEngine(IWrapEngine<TValue, TAtom, TWrap> wrapEngine, IGraphEngine<DagRelationEngine<TValue, TAtom, TWrap>.Edge, TNode> graphEngine, IEqualityComparer<IAtomicRelation<TAtom>> atomicEC)
            : base(wrapEngine, graphEngine, atomicEC)
        {
            var filterEC = UniqueHashCodeEC(IdentityEC<Func<IState, Boolean>>());
            PrependAtomCache = new Dictionary<(TAtom, TNode), TNode>(ValueTupleEC(WrapEngine.AtomEngine.Comparer, GraphEngine.Comparer));
            PrependWrapCache = new Dictionary<(TWrap, TNode), TNode>(ValueTupleEC(WrapEngine.Comparer, GraphEngine.Comparer));
            DerivativesCache = new Dictionary<(TNode, Func<IState, Boolean>), IEnumerable<(IState, IReadOnlyCollection<(TWrap, TNode)>)>>(ValueTupleEC(GraphEngine.Comparer, filterEC));
            ConcatCache = new Dictionary<(TNode, (TWrap, TNode)), TNode>(ValueTupleEC(GraphEngine.Comparer, Comparer));
            RankCache = new Dictionary<TNode, Int32>(NeighborsEC);
            DominatorCache = new Dictionary<TNode, (Boolean, TNode)>(NeighborsEC);
            FactorOutCache = new Dictionary<(TNode, TNode), (TWrap, TNode)>(ValueTupleEC(NeighborsEC, NeighborsEC));
            PerformCache = new Dictionary<(IRelationalOperation<TValue, TAtom>, (TWrap, TNode)), (TWrap, TNode)>();
        }

        protected override TNode Prepend(TAtom atomic, TNode node)
        {
            TNode result;
            if (!PrependAtomCache.TryGetValue((atomic, node), out result))
            {
                result = base.Prepend(atomic, node);
                PrependAtomCache.Add((atomic, node), result);
            }
            return result;
        }

        protected override TNode Prepend(TWrap wrap, TNode node)
        {
            TNode result;
            if (!PrependWrapCache.TryGetValue((wrap, node), out result))
            {
                result = base.Prepend(wrap, node);
                PrependWrapCache.Add((wrap, node), result);
            }
            return result;
        }

        public override IEnumerable<(IState, IReadOnlyCollection<(TWrap, TNode)>)> Derivatives((TWrap, TNode) relation, Func<IState, Boolean> filter)
        {
            IEnumerable<(IState, IReadOnlyCollection<(TWrap, TNode)>)> result;
            if (!DerivativesCache.TryGetValue((relation.Item2, filter), out result))
            {
                result = base.Derivatives(relation, filter);
                DerivativesCache.Add((relation.Item2, filter), result);
            }
            return result;
        }

        private readonly Counter ConcatCalls = new Counter();
        private readonly Counter ConcatCacheSize = new Counter();

        protected override TNode Concat(TNode left, (TWrap, TNode) right)
        {
            ConcatCalls.Increment();
            TNode result;
            if (!ConcatCache.TryGetValue((left, right), out result))
            {
                result = base.Concat(left, right);
                ConcatCache.Add((left, right), result);
                ConcatCacheSize.Increment();
            }
            return result;
        }

        protected override Int32 Rank(TNode neighbors)
        {
            Int32 result;
            if (!RankCache.TryGetValue(neighbors, out result))
            {
                result = base.Rank(neighbors);
                RankCache.Add(neighbors, result);
            }
            return result;
        }

        protected override Boolean Dominator(ref TNode neighbors)
        {
            (Boolean, TNode) result;
            if (!DominatorCache.TryGetValue(neighbors, out result))
            {
                result.Item2 = neighbors;
                result.Item1 = base.Dominator(ref result.Item2);
                DominatorCache.Add(neighbors, result);
            }
            neighbors = result.Item2;
            return result.Item1;
        }

        protected override (TWrap, TNode) FactorOut(TNode concat, TNode right)
        {
            (TWrap, TNode) result;
            if (!FactorOutCache.TryGetValue((concat, right), out result))
            {
                result = base.FactorOut(concat, right);
                FactorOutCache.Add((concat, right), result);
            }
            return result;
        }

        public override void Perform(IRelationalOperation<TValue, TAtom> operation, ref (TWrap, TNode) relation)
        {
            var key = (operation, relation);
            if (PerformCache.TryGetValue(key, out var result))
            {
                relation = result;
            }
            else
            {
                base.Perform(operation, ref relation);
                PerformCache.Add(key, relation);
            }
        }
    }

    public class MemoizingDagRelationEngineFactory : RelationEngineFactory
    {
        private readonly IGraphEngineFactory GraphEngineFactory;

        public MemoizingDagRelationEngineFactory(IWrapEngineFactory wrapEngineFactory, IGraphEngineFactory graphEngineFactory)
            : base(wrapEngineFactory)
        {
            GraphEngineFactory = graphEngineFactory;
        }

        public override IRelationEngine<TValue> Create<TValue>(IValuationSemiring<TValue> semiring, IWrapEngine<TValue> wrapEngine)
            => wrapEngine
                .Accept(new CreateRequest<TValue> { GraphEngineFactory = GraphEngineFactory });

        private class CreateRequest<TValue> : IWrapEngineVisitor<TValue, IRelationEngine<TValue>>
        {
            public IGraphEngineFactory GraphEngineFactory { get; set; }

            public IRelationEngine<TValue> Visit<TAtom, TWrap>(IWrapEngine<TValue, TAtom, TWrap> engine)
            {
                var atomicEC = IdentityEC<IAtomicRelation<TAtom>>();
                return GraphEngineFactory
                    .Create(
                        DagRelationEngine<TValue, TAtom, TWrap>.EdgeEC(engine, atomicEC),
                        DagRelationEngine<TValue, TAtom, TWrap>.MergeEdges(engine, atomicEC),
                        engine.IsIdempotent)
                    .Accept(new CreateRequest<TValue, TAtom, TWrap> { WrapEngine = engine, AtomicEC = atomicEC });
        }
        }

        private class CreateRequest<TValue, TAtom, TWrap> : IGraphEngineVisitor<DagRelationEngine<TValue, TAtom, TWrap>.Edge, IRelationEngine<TValue>>
        {
            public IWrapEngine<TValue, TAtom, TWrap> WrapEngine { get; set; }
            public IEqualityComparer<IAtomicRelation<TAtom>> AtomicEC { get; set; }

            public IRelationEngine<TValue> Visit<TNode>(IGraphEngine<DagRelationEngine<TValue, TAtom, TWrap>.Edge, TNode> engine)
                => new MemoizingDagRelationEngine<TValue, TAtom, TWrap, TNode>(WrapEngine, engine, AtomicEC);
        }

        public override String ToString() =>
            $"{nameof(MemoizingDagRelationEngineFactory)}({WrapEngineFactory},{GraphEngineFactory})";
    }
}
