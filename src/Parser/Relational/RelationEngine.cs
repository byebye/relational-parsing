﻿using System;
using System.Collections.Generic;

namespace Parser.Relational
{
    using Algebra;

    public interface IRelationEngineFactory : IRelationOperantFactory
    {
        IWrapEngineFactory WrapEngineFactory { get; }
        new IRelationEngine<TValue> Create<TValue>(IValuationSemiring<TValue> semiring);
        IRelationEngine<TValue> Create<TValue>(IValuationSemiring<TValue> semiring, IWrapEngine<TValue> wrapEngine);
    }

    public abstract class RelationEngineFactory : IRelationEngineFactory
    {
        public IWrapEngineFactory WrapEngineFactory { get; }

        protected RelationEngineFactory(IWrapEngineFactory wrapEngineFactory)
        {
            WrapEngineFactory = wrapEngineFactory;
        }

        public virtual IRelationEngine<TValue> Create<TValue>(IValuationSemiring<TValue> semiring)
            => Create(semiring, WrapEngineFactory.Create(semiring));

        public abstract IRelationEngine<TValue> Create<TValue>(IValuationSemiring<TValue> semiring, IWrapEngine<TValue> wrapEngine);

        IRelationOperant<TValue> IRelationOperantFactory.Create<TValue>(IValuationSemiring<TValue> semiring)
            => Create(semiring);
    }

    public interface IRelationEngine<TValue> : IRelationOperant<TValue>
    {
        TResult Accept<TResult>(IRelationEngineVisitor<TValue, TResult> visitor);
    }

    public interface IRelationEngineVisitor<TValue, out TResult>
    {
        TResult Visit<TAtom, TWrap, TShape, TRelation>(IRelationEngine<TValue, TAtom, TWrap, TShape, TRelation> engine);
    }

    public interface IRelationEngine<TValue, TAtom, TWrap, TRelation> : ILeftDistributive<TRelation, TValue>, IRelationOperant<TValue, TAtom, TRelation>, IRelationEngine<TValue>
    {
        IWrapEngine<TValue, TAtom, TWrap> WrapEngine { get; }
        IEnumerable<(IState, IReadOnlyCollection<TRelation>)> Derivatives(TRelation relation, Func<IState, Boolean> filter);
        TWrap Epsilon(TRelation relation);
        TRelation Multiply(IAtomicRelation<TAtom> left, TRelation right);
        TRelation Concat(TRelation left, TRelation right);
        IReadOnlyCollection<TRelation> Factorize(TRelation relation);
    }

    public interface IRelationEngine<TValue, TAtom, TWrap, TShape, TRelation> : IRelationEngine<TValue, TAtom, TWrap, TRelation>
    {
        TRelation Compose(TShape shape, IReadOnlyList<TWrap> content);
        (TShape, IReadOnlyList<TWrap>) Decompose(TRelation relation);
        IEqualityComparer<TShape> ShapeComparer { get; }
    }

    public abstract class RelationEngine<TValue, TAtom, TWrap, TRelation> : LeftDistributive<TRelation, TValue>, IRelationEngine<TValue, TAtom, TWrap, TRelation, TRelation>
    {
        public virtual IAtomEngine<TValue, TAtom> AtomEngine
            => WrapEngine.AtomEngine;
        public virtual TRelation Concat(TRelation left, TRelation right)
            => throw new NotSupportedException();
        public abstract IEnumerable<(IState, IReadOnlyCollection<TRelation>)> Derivatives(TRelation relation, Func<IState, Boolean> filter);
        public abstract TWrap Epsilon(TRelation relation);
        public virtual TValue FlatEpsilon(TRelation relation)
            => WrapEngine.Flatten(Epsilon(relation));
        public virtual IReadOnlyCollection<TRelation> Factorize(TRelation relation)
            => throw new NotSupportedException();
        public abstract TRelation Multiply(IAtomicRelation<TAtom> left, TRelation right);
        public virtual void Perform(IRelationalOperation<TValue, TAtom> operation, ref TRelation relation)
            => relation = operation.Perform(this, relation);
        public abstract IWrapEngine<TValue, TAtom, TWrap> WrapEngine { get; }

        TRelation IRelationEngine<TValue, TAtom, TWrap, TRelation, TRelation>.Compose(TRelation shape, IReadOnlyList<TWrap> content)
            => shape;
        (TRelation, IReadOnlyList<TWrap>) IRelationEngine<TValue, TAtom, TWrap, TRelation, TRelation>.Decompose(TRelation relation)
            => (relation, Array.Empty<TWrap>());
        IEqualityComparer<TRelation> IRelationEngine<TValue, TAtom, TWrap, TRelation, TRelation>.ShapeComparer
            => Comparer;

        TResult IRelationOperant<TValue>.Accept<TResult>(IRelationOperantVisitor<TValue, TResult> visitor)
            => visitor.Visit(this);
        TResult IRelationEngine<TValue>.Accept<TResult>(IRelationEngineVisitor<TValue, TResult> visitor)
            => visitor.Visit(this);
    }

    public abstract class RelationEngine<TValue, TAtom, TWrap, TShape, TRelation> : RelationEngine<TValue, TAtom, TWrap, TRelation>, IRelationEngine<TValue, TAtom, TWrap, TShape, TRelation>
    {
        public abstract IEqualityComparer<TShape> ShapeComparer { get; }
        public abstract TRelation Compose(TShape shape, IReadOnlyList<TWrap> content);
        public abstract (TShape, IReadOnlyList<TWrap>) Decompose(TRelation relation);

        TResult IRelationEngine<TValue>.Accept<TResult>(IRelationEngineVisitor<TValue, TResult> visitor)
            => visitor.Visit<TAtom, TWrap, TShape, TRelation>(this);
    }
}
