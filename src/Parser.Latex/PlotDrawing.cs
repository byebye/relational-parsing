using System;
using System.Collections.Generic;

namespace Parser.Latex
{
    public class PlotDrawing : TikzDrawing
    {
        private String Axis { get; }
        
        const String DefaultAxis = "loglog";
        const String DefaultStyle = "grid=both,log basis x=2,xtick distance=2,log basis y=2,ytick distance=2,";

        public PlotDrawing(LatexDocument document, String axis = null, String style = "")
            : base(document)
        {
            Axis = (axis ?? DefaultAxis) + "axis";
            Document.WriteLine($"\\begin{{{Axis}}}[{DefaultStyle}{style}]");
        }

        public override void Dispose()
        {
            Document.WriteLine($"\\end{{{Axis}}}");
            base.Dispose();
        }

        public void PlotData<T>(IEnumerable<(T, T)> points, String style = "")
        {
            Document.WriteLine($"\\addplot+[{style}] table[header=false] {{");
            foreach (var (x, y) in points)
                Document.WriteLine($"  {x} {y}");
            Document.WriteLine("};");
        }
    }
}