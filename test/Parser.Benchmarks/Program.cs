﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using CommandLine;
using GlobExpressions;

namespace Parser.Benchmarks
{
    using System.Threading;
    using Algebra;
    using Utilities;
    using Valuations;

    public class Program
    {
        private class Options
        {
            [Value(0, Min = 1, HelpText = "Files to parse. May include glob patterns.")]
            public IEnumerable<String> Inputs { get; set; }

            [Option('g', "grammar", Required = true, HelpText = "Grammar file, in ANTLR4 format.")]
            public String Grammar { get; set; }

            [Option('i', "input", HelpText = "Cache file for tokenized input. Will be created if it does not exist.")]
            public String InputCache { get; set; }

            [Option('p', "parser", HelpText = "Parser generators to benchmark.")]
            public IEnumerable<String> Generators { get; set; }

            [Option('m', "mode", Default = "recognition", HelpText = "Parsing mode. Can be one of 'recognition', 'counting', 'length', or 'shape'.")]
            public String Mode { get; set; }

            [Option('d', "dot-every", Default = 10000, HelpText = "Print progress dot every <count> terminals.")]
            public Int32 DotEvery { get; set; }
        }

        private static Int32 Main(Options options)
        {
            var timer = new Stopwatch();
            try
            {
                Console.WriteLine("Instantiating parser generators:");
                var instantiator = new Instantiator()
                {
                    { typeof(Relational.IWrapEngineFactory), "CommutativeWrapEngineFactory(null)" }
                };
                var generators = new List<ParserGenerator>();
                foreach (var description in options.Generators)
                {
                    Console.WriteLine("  " + description);
                    generators.AddRange(instantiator.InstantiateAll<ParserGenerator>(description));
                }
                if (generators.Count == 0)
                    throw new ArgumentException("No parser generators chosen. Aborting.");
                Console.WriteLine($"{generators.Count} generator(s) to benchmark.");

                Console.WriteLine($"Reading grammar {options.Grammar}");
                Antlr4Grammar grammar;
                using (var reader = new StreamReader(options.Grammar))
                    grammar = new Antlr4Grammar(reader);

                timer.Restart();
                List<Char[]> inputs;
                var formatter = new BinaryFormatter();
                try
                {
                    using (var stream = File.OpenRead(options.InputCache))
                        inputs = (List<Char[]>)formatter.Deserialize(stream);
                    Console.WriteLine($"Read cached input from {options.InputCache}");
                }
                catch (Exception)
                {
                    Console.WriteLine($"Scanning input files:");
                    inputs = new List<Char[]>();
                    foreach (var pattern in options.Inputs)
                    {
                        Console.Write($"  {pattern} ");
                        var fragments = pattern.Split(Path.DirectorySeparatorChar);
                        var leading = Math.Min(fragments.TakeWhile(s => !s.Contains('*')).Count(), fragments.Length - 1);
                        var root = (leading > 0) ? String.Join(Path.DirectorySeparatorChar, fragments.Take(leading)) : Directory.GetCurrentDirectory();
                        var lookup = String.Join(Path.DirectorySeparatorChar, fragments.Skip(leading));
                        foreach (var file in new DirectoryInfo(root).GlobFiles(lookup))
                        {
                            Console.Write('.');
                            using (var reader = new StreamReader(file.FullName))
                                inputs.Add(grammar.Input(reader).ToArray());
                        }
                        Console.WriteLine();
                    }
                    if (options.InputCache != null)
                    {
                        Console.WriteLine($"Writing input to {options.InputCache}");
                        using (var stream = File.OpenWrite(options.InputCache))
                            formatter.Serialize(stream, inputs);
                    }
                }
                var inputTime = timer.ElapsedMilliseconds;
                Console.WriteLine($"Inputs: {inputs.Count} file(s), {inputs.Sum(_ => _.Length)} token(s), {inputTime} ms");

                foreach (var generator in generators)
                {
                    Int64 createTime, parseTime = 0;
                    TResult Run<TValue, TResult>(IParser<TValue> parser, TResult initial, Func<TResult, TValue, TResult> accumulate)
                    {
                        timer.Restart();
                        var count = 0;
                        foreach (var input in inputs)
                            foreach (var _ in parser.Run(input, value => initial = accumulate(initial, value)))
                                if (++count % options.DotEvery == 0)
                                    Console.Write('.');
                        parseTime = timer.ElapsedMilliseconds;
                        return initial;
                    };
                    Counter.ResetAll();
                    Console.Write($"Running {generator} ");
                    try
                    {
                        switch (options.Mode)
                        {
                            case "recognition":
                            {
                                timer.Restart();
                                var parser = generator.CreateRecognizer(grammar);
                                createTime = timer.ElapsedMilliseconds;
                                Counter.AssignNames(parser, nameof(parser));
                                var accepted = Run(parser, 0, (x, y) => x + 1);
                                Console.WriteLine($" {accepted} accepted inputs; took {createTime} + {parseTime} ms");
                                break;
                            }
                            case "counting":
                            {
                                timer.Restart();
                                var parser = generator.CreateCounter(grammar);
                                createTime = timer.ElapsedMilliseconds;
                                Counter.AssignNames(parser, nameof(parser));
                                var trees = Run(parser, 0L, (x, y) => x + y);
                                Console.WriteLine($" {trees} total trees; took {createTime} + {parseTime} ms");
                                break;
                            }
                            case "length":
                            {
                                timer.Restart();
                                var parser = generator.CreateParser(grammar, new LengthSemiring());
                                createTime = timer.ElapsedMilliseconds;
                                Counter.AssignNames(parser, nameof(parser));
                                var trees = Run(parser, 0L, (x, y) => x + y.GetValueOrDefault(0));
                                Console.WriteLine($" {trees} maximal accepting sequence length; took {createTime} + {parseTime} ms");
                                break;
                            }
                            case "shape":
                            {
                                timer.Restart();
                                var parser = generator.CreateParser(grammar, new ShapeSemiring());
                                createTime = timer.ElapsedMilliseconds;
                                Counter.AssignNames(parser, nameof(parser));
                                Run(parser, 0, (x, y) => 0);
                                Console.WriteLine($" finished; took {createTime} + {parseTime} ms");
                                break;
                            }
                            default:
                                throw new ArgumentException($"invalid parsing mode '{options.Mode}'");
                        }
                    }
                    catch (OperationCanceledException)
                    {
                        Console.WriteLine("Cancelled.");
                    }
                    Counter.PrintAll();
                }
            }
            catch (Exception error)
            {
                Console.Error.WriteLine(error);
                return 1;
            }
            return 0;
        }

        public static Int32 Main(String[] args) =>
            CommandLine.Parser.Default.ParseArguments<Options>(args).MapResult(Main, _ => 1);
    }
}
