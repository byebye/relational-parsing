﻿using System;
using NUnit.Framework;

namespace Parser.Tests
{
    using Grammars;
    using Utilities;
    using Valuations;

    public class ParsingTests
    {
        public void TestRecognizer(ParserGenerator generator, SampleGrammar grammar)
        {
            try
            {
                var parser = generator.CreateRecognizer(grammar);
                foreach (var (input, expected) in grammar.Inputs.SkipLast())
                    Assert.AreEqual(expected.GetValueOrDefault(1) > 0, parser.Run(input));
            }
            catch (NotImplementedException)
            {
                throw new IgnoreException($"{generator} seems not to support this grammar");
            }
        }

        public void TestCounter(ParserGenerator generator, SampleGrammar grammar)
        {
            try
            {
                var parser = generator.CreateCounter(grammar);
                foreach (var (input, expected) in grammar.Inputs.SkipLast())
                    if (expected.HasValue)
                        Assert.AreEqual(expected.Value, parser.Run(input));
            }
            catch (NotImplementedException)
            {
                throw new IgnoreException($"{generator} seems not to support this grammar");
            }
        }


        public void TestShapeParser(ParserGenerator generator, SampleGrammar grammar)
        {
            try
            {
                var parser = generator.CreateParser(grammar, new ShapeSemiring());
                foreach (var (input, expected) in grammar.Inputs.SkipLast())
                    parser.Run(input);
            }
            catch (NotImplementedException)
            {
                throw new IgnoreException($"{generator} seems not to support this grammar");
            }
        }
    }
}
