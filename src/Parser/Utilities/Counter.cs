using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;

namespace Parser.Utilities
{
    public sealed class Counter
    {
        private String Name;
        private Int64 Value;

        public Counter()
        {
            Value = 0L;
            All.Add(this);
        }

        [Conditional("DEBUG")]
        public void Increment(Int64 change = 1L)
        {
            Cancellation.Check();
            Interlocked.Add(ref Value, change);
        }

        private static readonly HashSet<Counter> All
            = new HashSet<Counter>();

        [Conditional("DEBUG")]
        public static void AssignNames(Object root, String rootName)
        {
            var visited = new HashSet<Object>();
            var queue = new Queue<(Object, String)>();
            if (visited.Add(root))
                queue.Enqueue((root, rootName));
            while (queue.Count > 0)
            {
                var (node, name) = queue.Dequeue();
                if (node is Counter counter)
                    counter.Name = name;
                var fields = new HashSet<String>();
                for (var type = node.GetType(); type != null; type = type.BaseType)
                    foreach (var field in type.GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic))
                        if (field.FieldType.Namespace.StartsWith("Parser") && fields.Add(field.Name))
                        {
                            var fieldName = field.Name;
                            if (fieldName.EndsWith("k__BackingField"))
                                fieldName = fieldName.Substring(1, fieldName.Length - 17);
                            var target = field.GetValue(node);
                            if (target != null && visited.Add(target))
                                queue.Enqueue((target, $"{name}({type.Name}).{fieldName}"));
                        }
            }
        }

        [Conditional("DEBUG")]
        public static void PrintAll(TextWriter writer = null)
            => (writer ?? Console.Out).WriteLine(String.Join("\n", All.Where(_ => _.Name != null && _.Value != 0).Select(_ => $"{_.Name} : {_.Value}")));

        [Conditional("DEBUG")]
        public static void ResetAll()
        {
            foreach (var counter in All)
                counter.Value = 0L;
        }
    }
}
