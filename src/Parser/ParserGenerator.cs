using System;

namespace Parser
{
    public abstract class ParserGenerator
    {
        public abstract ParserFeatures Features { get; }

        public virtual IParser<TValue> CreateParser<TValue>(IGrammar grammar, IValuationSemiring<TValue> semiring) =>
            throw new NotSupportedException($"Parsing with semiring {typeof(TValue).Name} not supported.");

        public virtual IParser<Int64> CreateCounter(IGrammar grammar) =>
            CreateParser(grammar, new Valuations.CountingSemiring());

        public virtual IParser<Boolean> CreateRecognizer(IGrammar grammar) =>
            CreateParser(grammar, new Valuations.BooleanSemiring());
    }
}
