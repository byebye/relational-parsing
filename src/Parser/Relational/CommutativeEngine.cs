using System;
using System.Collections.Generic;

namespace Parser.Relational
{
    using Algebra;

    public class CommutativeEngine<TValue> : Semiring<TValue>, IWrapEngine<TValue, TValue, TValue>, IAtomEngine<TValue, TValue>
    {
        public ISemiring<TValue> Semiring { get; }

        public CommutativeEngine(ISemiring<TValue> semiring)
        {
            if (!semiring.IsCommutative)
                throw new ArgumentException("commutative semiring required", nameof(semiring));
            Semiring = semiring;
        }

        public IAtomEngine<TValue, TValue> AtomEngine
            => this;

        public override IEqualityComparer<TValue> Comparer
            => Semiring.Comparer;

        public override TValue Zero
            => Semiring.Zero;

        public override Boolean IsZero(TValue value)
            => Semiring.IsZero(value);

        public override TValue Add(TValue left, TValue right)
            => Semiring.Add(left, right);

        public override TValue Add(IEnumerable<TValue> values)
            => Semiring.Add(values);

        public override TValue One
            => Semiring.One;

        public override Boolean IsOne(TValue value)
            => Semiring.IsOne(value);

        public override TValue Multiply(TValue left, TValue right)
            => Semiring.Multiply(left, right);

        public override TValue Multiply(IEnumerable<TValue> values)
            => Semiring.Multiply(values);

        public TValue Flatten(TValue wrap)
            => wrap;

        public TValue Fold(TValue wrap)
            => wrap;

        public override Boolean IsCommutative
            => true;

        public override Boolean IsIdempotent
            => Semiring.IsIdempotent;

        public TValue Multiply(TValue left, (TValue, TValue) right)
            => Semiring.Multiply(right.Item1, left, right.Item2);

        public TValue Wrap(TValue value, TValue atom)
            => Semiring.Multiply(value, atom);

        TResult IAtomEngine<TValue>.Accept<TResult>(IAtomEngineVisitor<TValue, TResult> visitor)
            => visitor.Visit(this);

        TResult IWrapEngine<TValue>.Accept<TResult>(IWrapEngineVisitor<TValue, TResult> visitor)
            => visitor.Visit(this);
    }

    public class CommutativeWrapEngineFactory : IWrapEngineFactory
    {
        private readonly IWrapEngineFactory Fallback;

        public CommutativeWrapEngineFactory(IWrapEngineFactory fallback)
        {
            Fallback = fallback;
        }

        public IWrapEngine<TValue> Create<TValue>(ISemiring<TValue> semiring)
            => semiring.IsCommutative ? new CommutativeEngine<TValue>(semiring) : Fallback.Create(semiring);

        public override String ToString()
            => $"{nameof(CommutativeWrapEngineFactory)}({Fallback})";
    }

    public class CommutativeAtomEngineFactory : IAtomEngineFactory
    {
        private readonly IAtomEngineFactory Fallback;

        public CommutativeAtomEngineFactory(IAtomEngineFactory fallback)
        {
            Fallback = fallback;
        }

        public IAtomEngine<TValue> Create<TValue>(ISemiring<TValue> semiring)
            => semiring.IsCommutative ? new CommutativeEngine<TValue>(semiring) : Fallback.Create(semiring);
    }
}
