using System;
using System.Linq.Expressions;
using Antlr4.Runtime;
using Antlr4.Runtime.Tree;

namespace Parser.Benchmarks
{
    public class Antlr4Parser<TParser> : Antlr4ParserBase<TParser>
        where TParser : Antlr4.Runtime.Parser
    {
        private static Func<TParser, ITokenStream, ITree> MakeInvokeParse(TParser antlrParser)
        {
            var setInput = typeof(TParser).GetMethod(nameof(Antlr4.Runtime.Parser.SetInputStream));
            var parse = typeof(TParser).GetMethod(antlrParser.RuleNames[0]);
            var parser = Expression.Parameter(typeof(TParser), "parser");
            var stream = Expression.Parameter(typeof(ITokenStream), "stream");
            var lambda = Expression.Lambda<Func<TParser, ITokenStream, ITree>>(
                Expression.Block(
                    typeof(ITree),
                    Expression.Call(parser, setInput, stream),
                    Expression.Call(parser, parse)
                ),
                parser, stream);
            return lambda.Compile();
        }

        public Antlr4Parser(TParser antlrParser)
            : base(antlrParser, MakeInvokeParse(antlrParser))
        {
            antlrParser.BuildParseTree = false;
        }
    }
}
