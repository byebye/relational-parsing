using System;
using System.Collections.Generic;
using System.Linq;

namespace Parser.Graphs
{
    using Algebra;
    using Utilities;

    public interface IGraphEngineFactory
    {
        /// <param name="edgeEC">
        /// An EqualityComparer classifying parallel edges.
        /// Edges with identical values of <see cref="EqualityComparer<T>.GetHashCode" /> will be merged using <see cref="mergeEdges" />.
        /// Edges with different hash codes will be left parallel.
        /// </param>
        /// <param name="mergeEdges">
        /// The function used to merge parallel edges.
        /// </param>
        /// <param name="mergeIsIdempotent">
        /// A hint. If set to true, the engine may optimize its operations by assuming <see cref="mergeEdges" /> is idempotent with respect to <see cref="edgeEC" />.
        /// </param>
        IGraphEngine<TEdge> Create<TEdge>(IEqualityComparer<TEdge> edgeEC, Func<TEdge, TEdge, TEdge> mergeEdges, Boolean mergeIsIdempotent = false);
    }

    public static class GraphEngineFactory
    {
        private class NoParallelEC<TEdge> : IEqualityComparer<TEdge>
        {
            private readonly Func<TEdge, TEdge, Boolean> EqualEdges;

            public NoParallelEC(Func<TEdge, TEdge, Boolean> equalEdges)
            {
                EqualEdges = equalEdges;
            }

            public Boolean Equals(TEdge x, TEdge y)
                => EqualEdges(x, y);

            public Int32 GetHashCode(TEdge obj)
                => 0;
        }

        public static IGraphEngine<TEdge> CreateNoParallel<TEdge>(this IGraphEngineFactory factory, IEqualityComparer<TEdge> edgeEC, Func<TEdge, TEdge, TEdge> mergeEdges = null, Boolean mergeIsIdempotent = false)
            => factory.Create(new ConstantHashCodeEqualityComparer<TEdge>(edgeEC), mergeEdges ?? MergeError, mergeIsIdempotent);

        private static TEdge MergeError<TEdge>(TEdge x, TEdge y)
            => throw new InvalidOperationException($"unexpected merge of edges {x} and {y}");
    }

    public interface IGraphEngine<TEdge>
    {
        TResult Accept<TResult>(IGraphEngineVisitor<TEdge, TResult> visitor);
    }

    public interface IGraphEngineVisitor<TEdge, out TResult>
    {
        TResult Visit<TNode>(IGraphEngine<TEdge, TNode> engine);
    }

    public interface IGraphEngine<TEdge, TNode> : IAdditive<TNode>, IGraphEngine<TEdge>
    {
        IEnumerable<(TEdge, TNode)> Neighbors(TNode node);
        TNode Create(IEnumerable<(TEdge, TNode)> edges);
        TNode Map(TNode node, Func<TEdge, TEdge> mapping);
        void AddEdge(TNode source, TEdge edge, TNode target);
    }

    public static class GraphEngineExtensions
    {
        public static TNode Create<TEdge, TNode>(this IGraphEngine<TEdge, TNode> graphEngine, params (TEdge, TNode)[] edges)
            => graphEngine.Create(edges as IEnumerable<(TEdge, TNode)>);
    }

    public abstract class GraphEngine<TEdge, TNode> : Additive<TNode>, IGraphEngine<TEdge, TNode>
    {
        public abstract IEnumerable<(TEdge, TNode)> Neighbors(TNode node);
        public abstract TNode Create(IEnumerable<(TEdge, TNode)> edges);

        public override TNode Add(IEnumerable<TNode> nodes)
            => Create(nodes.SelectMany(Neighbors));

        public virtual TNode Map(TNode node, Func<TEdge, TEdge> mapping)
            => Create(Neighbors(node).Select(_ => (mapping(_.Item1), _.Item2)));

        public virtual void AddEdge(TNode source, TEdge edge, TNode target)
            => throw new NotSupportedException();

        TResult IGraphEngine<TEdge>.Accept<TResult>(IGraphEngineVisitor<TEdge, TResult> visitor)
            => visitor.Visit(this);
    }
}
