using System;
using System.Collections.Generic;
using System.Linq;

namespace Parser.Utilities
{
    public static class Combinatorics
    {
        public static IEnumerable<IEnumerable<T>> Repeats<T>(T value, Int32 minCount, Int32 maxCount)
            => Enumerable.Range(minCount, maxCount - minCount + 1)
                .Select(count => Enumerable.Repeat(value, count));

        public static IEnumerable<IEnumerable<T>> Subsets<T>(this IEnumerable<T> values)
            => values.Aggregate(
                Repeats(default(T), 0, 0),
                (vss, v) => vss.SelectMany(_ => Repeats(v, 0, 1), Enumerable.Concat));

        public static IEnumerable<IEnumerable<T>> Product<T>(this IEnumerable<IEnumerable<T>> iterables)
            => iterables.Aggregate(Repeats(default(T), 0, 0), (vss, ws) => vss.SelectMany(_ => ws, (vs, w) => vs.Concat(Enumerable.Repeat(w, 1))));
    }
}
