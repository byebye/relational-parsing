﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Parser.GLR
{
    using Common;
    using Utilities;
    using Valuations;

    public class GLRParser<TConfiguration> : IParser<Boolean>
        where TConfiguration : IEquatable<TConfiguration>
    {
        private readonly IGrammar Grammar;
        private readonly GrammarAnalysis Analysis;
        private readonly IConfigurationEngine<TConfiguration> Engine;
        private readonly TConfiguration StartConfiguration;
        private readonly Dictionary<IState, (IState, Int32)> Distances;
        public IValuationSemiring<Boolean> Semiring { get; }
            = new BooleanSemiring();

        public GLRParser(IConfigurationEngine<TConfiguration> engine, Boolean precompute)
        {
            Engine = engine;
            Analysis = Engine.Analysis;
            Grammar = Analysis.Grammar;
            StartConfiguration = Engine.Atomic(Grammar.StartState);
            var initial = new HashSet<IState>(from call in Analysis.CallTransitions select call.Child);
            initial.Add(Grammar.StartState);
            Distances = new Dictionary<IState, (IState, Int32)>();
            var queue = new Queue<IState>();
            foreach (var state in initial)
            {
                Distances.Add(state, (state, 0));
                queue.Enqueue(state);
            }
            while (queue.Count > 0)
            {
                var state = queue.Dequeue();
                var (origin, distance) = Distances[state];
                foreach (var transition in state.Transitions)
                    try
                    {
                        switch (transition)
                        {
                            case Transition.Call call:
                                Distances.Add(call.Next, (origin, distance + 1));
                                queue.Enqueue(call.Next);
                                break;
                            case Transition.Shift shift:
                                Distances.Add(shift.Next, (origin, distance + 1));
                                queue.Enqueue(shift.Next);
                                break;
                        }
                    }
                    catch (ArgumentException)
                    {
                        throw new NotImplementedException($"{nameof(GLRParser<TConfiguration>)} can currently only handle grammars with tree-shaped right-hand sides");
                    }
            }
            if (precompute)
            {
                var visited = new HashSet<TConfiguration> { StartConfiguration };
                var queue2 = new Queue<TConfiguration>(visited);
                while (queue2.Count > 0)
                    foreach (var successor in Engine.Successors(queue2.Dequeue()))
                        if (visited.Add(successor))
                            queue2.Enqueue(successor);
            }
        }

        private Boolean Reducer(GSSLayer<TConfiguration> layer, Char? lookahead, GSSNode<TConfiguration> rootNode, ref Statistics statistics)
        {
            Boolean accept = false;
            var visited = new Dictionary<GSSNode<TConfiguration>, HashSet<(IState, Int32)>>();
            var queue = new Queue<(GSSNode<TConfiguration>, IState, Int32)>();
            foreach (var node in layer)
                foreach (var (origin, distance) in Engine.Reduce(node.Label, lookahead).Select(s => Distances[s]))
                {
                    statistics.VisitedEdges += 1;
                    if (visited.GetOrCreate(node).Add((origin, distance)))
                        queue.Enqueue((node, origin, distance));
                }
            while (queue.Count > 0)
            {
                var (node, origin, distance) = queue.Dequeue();
                if (distance == 0)
                {
                    if (node == rootNode && origin == Grammar.StartState)
                        accept = true;
                    var target = Engine.Call(node.Label, origin);
                    var parent = layer.AddNode(target, ref statistics);
                    if (parent.Children.Count == 0)
                        foreach (var (origin2, distance2) in Engine.Reduce(target, lookahead).Select(s => Distances[s]))
                            if (distance2 == 0)
                            {
                                statistics.VisitedEdges += 1;
                                if (visited.GetOrCreate(parent).Add((origin2, distance2)))
                                    queue.Enqueue((parent, origin2, distance2));
                            }
                    if (parent.AddChild(node, ref statistics))
                        foreach (var (origin2, distance2) in Engine.Reduce(target, lookahead).Select(s => Distances[s]))
                            if (distance2 > 0)
                            {
                                statistics.VisitedEdges += 1;
                                if (visited.GetOrCreate(node).Add((origin2, distance2 - 1)))
                                    queue.Enqueue((node, origin2, distance2 - 1));
                            }
                }
                else foreach (var child in node.Children)
                {
                    statistics.VisitedEdges += 1;
                    if (visited.GetOrCreate(child).Add((origin, distance - 1)))
                        queue.Enqueue((child, origin, distance - 1));
                }
            }
            return accept;
        }

        private void Shifter(GSSLayer<TConfiguration> currentLayer, GSSLayer<TConfiguration> nextLayer, Char terminal, ref Statistics statistics)
        {
            foreach (var node in currentLayer)
            {
                var target = Engine.Shift(node.Label, terminal);
                if (Engine.IsAlive(target))
                    nextLayer.AddNode(target, ref statistics).AddChild(node, ref statistics);
            }
        }

        public IEnumerable<Statistics> Run(IEnumerable<Char> input, Action<Boolean> withResult)
        {
            var layerIndex = 0;
            var statistics = new Statistics();
            var currentLayer = new GSSLayer<TConfiguration>(layerIndex);
            var rootNode = currentLayer.AddNode(StartConfiguration, ref statistics);
            Boolean accept;
            foreach (var terminal in input)
            {
                Reducer(currentLayer, terminal, rootNode, ref statistics);
                statistics.Graph = currentLayer;
                yield return statistics;
                layerIndex += 1;
                statistics = new Statistics();
                var nextLayer = new GSSLayer<TConfiguration>(layerIndex);
                Shifter(currentLayer, nextLayer, terminal, ref statistics);
                currentLayer = nextLayer;
            }
            accept = Reducer(currentLayer, null, rootNode, ref statistics);
            statistics.Graph = currentLayer;
            yield return statistics;
            withResult(accept);
        }
    }
}
