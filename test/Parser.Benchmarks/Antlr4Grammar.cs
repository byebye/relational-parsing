using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Antlr4;
using Antlr4.Runtime;
using Antlr4.Runtime.Misc;
using Antlr4.Tool.Ast;

namespace Parser.Benchmarks
{
    using Grammars;

    public class Antlr4Grammar : SimpleGrammar
    {
        private static readonly AntlrTool AntlrTool = new AntlrTool();
        public String Source { get; }
        public Func<ICharStream, LexerInterpreter> CreateLexer { get; }
        public Char EOF { get; }
        public Func<ITokenStream, ParserInterpreter> CreateParser { get; }

        public Antlr4Grammar(TextReader reader)
        {
            Source = reader.ReadToEnd();
            var input = new Antlr.Runtime.ANTLRStringStream(Source);
            var ast = AntlrTool.Parse("<string>", input);
            var grammar = AntlrTool.CreateGrammar((GrammarRootAST)ast.DupTree());
            AntlrTool.Process(grammar, false);
            CreateLexer = grammar.GetImplicitLexer().CreateLexerInterpreter;
            EOF = (Char)grammar.GetImplicitLexer().GetTokenType("EOF");
            CreateParser = grammar.CreateParserInterpreter;
            ast.Visit(new GrammarVisitor(this, grammar.GetTokenType));
        }

        private static void Print(GrammarAST node, String indent = "")
        {
            Console.Error.WriteLine(indent + node.GetType().Name + "  " + node.Text);
            if (node.ChildCount > 0)
                foreach (GrammarAST child in node.Children)
                    Print(child, indent + "  ");
        }

        public IEnumerable<Char> Input(TextReader reader)
        {
            var input = new UnbufferedCharStream(reader);
            var lexer = CreateLexer(input);
            return (from token in lexer.GetAllTokens() select (Char)token.Type).Concat(new []{ EOF });
        }

        private class GrammarVisitor : GrammarASTVisitor
        {
            private readonly SimpleGrammar Grammar;
            private readonly Dictionary<String, Int32> RuleToStateMap;
            private readonly Func<String, Int32> TokenToSymbol;
            private Int32 Source;
            private Int32 Target;

            public GrammarVisitor(SimpleGrammar grammar, Func<String, Int32> tokenToSymbol)
            {
                Grammar = grammar;
                RuleToStateMap = new Dictionary<String, Int32>();
                TokenToSymbol = tokenToSymbol;
            }

            private Int32 RuleToState(String name)
            {
                Int32 value;
                if (!RuleToStateMap.TryGetValue(name, out value))
                {
                    value = Grammar.NewState();
                    RuleToStateMap.Add(name, value);
                }
                return value;
            }

            private Object VisitChildren(GrammarAST node)
            {
                if (node.ChildCount > 0)
                    foreach (GrammarAST child in node.Children)
                        child.Visit(this);
                return null;
            }

            public Object Visit(GrammarAST node) =>
                VisitChildren(node);

            public Object Visit(GrammarRootAST node) =>
                VisitChildren(node);

            public Object Visit(RuleAST node)
            {
                if (node.IsLexerRule())
                    return null;
                Source = RuleToState(node.GetRuleName());
                Target = -1;
                return VisitChildren(node);
            }

            public Object Visit(BlockAST node) =>
                VisitChildren(node);

            public Object Visit(OptionalBlockAST node)
            {
                var epsilon = Grammar.NewState();
                Grammar.Add(R(epsilon));
                Grammar.Add(C(Source, epsilon, Target));
                return VisitChildren(node);
            }

            public Object Visit(PlusBlockAST node)
            {
                var (source, target) = (Source, Target);
                var entry = Grammar.NewState();
                var exit = Grammar.NewState();
                var epsilon = Grammar.NewState();
                Grammar.Add(R(epsilon));
                Grammar.Add(C(Source, entry, Target));
                Grammar.Add(R(exit));
                Grammar.Add(C(exit, epsilon, entry));
                (Source, Target) = (entry, exit);
                VisitChildren(node);
                (Source, Target) = (source, target);
                return null;
            }

            public Object Visit(StarBlockAST node)
            {
                var (source, target) = (Source, Target);
                var loop = Grammar.NewState();
                Grammar.Add(C(Source, loop, Target));
                Grammar.Add(R(loop));
                Source = Target = loop;
                VisitChildren(node);
                (Source, Target) = (source, target);
                return null;
            }

            public Object Visit(AltAST node)
            {
                var (source, target) = (Source, Target);
                var current = source;
                var stack = new Stack<GrammarAST>();
                if (node.ChildCount > 0)
                    foreach (GrammarAST child in node.Children.Reverse())
                        stack.Push(child);
                while (stack.Count > 0)
                {
                    var child = stack.Pop();
                    if (child.GetType() == typeof(GrammarAST))
                    {
                        if (child.ChildCount > 0)
                            foreach (GrammarAST grandchild in child.Children.Reverse())
                                stack.Push(grandchild);
                        continue;
                    }
                    Source = current;
                    Target = current = Grammar.NewState();
                    child.Visit(this);
                }
                var epsilon = Grammar.NewState();
                Grammar.Add(R(epsilon));
                (Source, Target) = (source, target);
                if (target == -1)
                {
                    target = Grammar.NewState();
                    Grammar.Add(R(target));
                }
                Grammar.Add(C(current, epsilon, target));
                return null;
            }

            public Object Visit(NotAST node) =>
                throw new NotSupportedException();

            public Object Visit(PredAST node) =>
                throw new NotSupportedException();

            public Object Visit(RangeAST node) =>
                throw new NotSupportedException();

            public Object Visit(SetAST node) =>
                throw new NotSupportedException();

            public Object Visit(RuleRefAST node)
            {
                Grammar.Add(C(Source, RuleToState(node.Text), Target));
                return null;
            }

            public Object Visit(TerminalAST node)
            {
                Grammar.Add(S(Source, (Char)TokenToSymbol(node.Text), Target));
                return null;
            }
        }
    }
}
