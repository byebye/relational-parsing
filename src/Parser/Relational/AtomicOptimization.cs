using System;
using System.Collections.Generic;
using No.Comparers;

namespace Parser.Relational
{
    using static EqualityComparerFactories;

    public abstract class AtomicOptimization
    {
        public abstract IAtomicRelations<TAtom> Perform<TValue, TAtom>(IAtomicRelations<TAtom> relations, IAtomEngine<TValue, TAtom> atomEngine);

        protected class GroupedRelation<TAtom> : List<IAtomicRelation<TAtom>>, IAtomicRelation<TAtom>
        {
            public TAtom Epsilon { get; set; }

            public IEnumerable<(IState, TAtom, IAtomicRelation<TAtom>)> Derivatives { get; set; }

            public override String ToString()
                => String.Join("~", this);
        }

        protected static IEqualityComparer<GroupedRelation<TAtom>> GroupedRelationEC<TAtom>()
            => UniqueHashCodeEC(IdentityEC<GroupedRelation<TAtom>>());
    }
}
